<div class="sticky-top" style="background-color: rgb(0, 8, 61); color: #FFF">
  <nav class="navbar navbar-expand-lg navbar-light">
    <div class="container-fluid">
      <a class="navbar-brand mb-2" href="#">
        <img src="public/images/logo.png" alt="" width="50">
      </a>

      <button class="navbar-toggler bg-white btn" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" style="width: 70px;">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent" style="text-align: right;">
        <ul class="navbar-nav  ms-auto" > 
          <li class="nav-item">
            <a class="nav-link m-2 <?php if ($titulo == 'inicio') { echo ' active'; } ?>" aria-current="page" href="index" style="color: #FFF">Inicio</a>
          </li>
          
          <li class="nav-item">
            <a class="nav-link m-2 <?php if ($titulo == 'metodo') { echo ' active'; } ?>" href="metodo" style="color: #FFF">Método</a>
          </li>

          <li class="nav-item">
            <a class="nav-link m-2 <?php //if ($titulo == 'blog') { echo ' active'; } ?>" href="https://fastenglish.com.mx/english-level-test" style="color: #FFF">Examen de ubicación</a>
          </li>
          
          <?php
          if (isset($_SESSION['login'])) {
          ?>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle m-2" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Mi cuenta
              </a>
              <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                <li>
                  <a class="dropdown-item" href="panel">Ir al panel</a>
                </li>
                <li>
                  <a class="dropdown-item" href="cerrar_sesion">Cerrar sesión</a>
                </li>
              </ul>
            </li>
          <?php
          } else {
          ?>
            <li class="nav-item">
              <a class="nav-link m-2 <?php if ($titulo == 'iniciar sesión') { echo ' active'; } ?>" href="http://lms.fastenglish.com.mx" style="color: #FFF">Iniciar Sesión</a>
            </li>

            <li class="nav-item">
              <a class="nav-link m-2 <?php if ($titulo == 'Pago') { echo ' active'; } ?>" href="pago" style="color: #FFF">Realizar pago</a>
            </li>
          <?php
          }
          ?>
        </ul>
      </div>
    </div>
  </nav>
</div>