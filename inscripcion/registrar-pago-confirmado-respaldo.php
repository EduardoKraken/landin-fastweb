<?php
date_default_timezone_set('America/Monterrey');
require_once 'inc/conexion.php';
?>

<?php
  $id        = $_POST['identificador'];
  $sqlUpdate = "UPDATE inscripciones_web SET pago_confirmado=1 where id=$id";
  

  if ($conn->query($sqlUpdate)) {
      $getEmail = "select tipoAlumno,nombre,apaterno,amaterno,edad,telefono,celular,correo,metodo_pago,cuatro_digitos,sucursal,afiliacion,autorizacion,clave_rastreo,referencia from inscripciones_web where id=$id";
      if (!$resEmail = $conn->query($getEmail)) {
        $errorGetEmail=1;
      }
      $dataEmail      = $resEmail->fetch_assoc();
      $nombreAlumno   = $dataEmail['nombre'];
      $apaternoAlumno = $dataEmail['apaterno'];
      $amaternoAlumno = $dataEmail['amaterno'];
      $edadAlumno     = $dataEmail['edad'];
      $celularAlumno  = $dataEmail['celular'];
      $telefonoAlumno = $dataEmail['telefono'];
      $correoAlumno   = $dataEmail['correo'];
      $metodopagoAlumno    = $dataEmail['metodo_pago'];
      $sucursalAlumno      = $dataEmail['sucursal'];
      $cuatrodigitosAlumno = $dataEmail['cuatro_digitos'];


      $afiliacion    = $dataEmail['afiliacion'];
      $autorizacion  = $dataEmail['autorizacion'];
      $clave_rastreo = $dataEmail['clave_rastreo'];
      $referencia    = $dataEmail['referencia'];

      ////// Obtener Datos de la sucursal
      $sqlSuc = "select nombre,correo from sucursales where id=$sucursalAlumno";
      $resDataSuc = $conn->query($sqlSuc);
      $datosSuc  = $resDataSuc->fetch_assoc();
      $nombreSucursal = $datosSuc['nombre'];
      $correoSucursal = $datosSuc['correo'];
      ///// Se termina consulta datos sucursal

      ####################################### Correo para alumnos  ######################################

      
      $subject = 'Confirmación de pago INBI.';
      
      $correo = "pagos@fastenglish.com.mx";
      $headers = "From: " . $correo. "\r\n";
      $headers .= "MIME-Version: 1.0\r\n";
      $headers .= "Content-Type: text/html; charset=utf8\r\n";
      
      $message = "
      <!doctype html>
      <html lang=\"en\">
        <head>
          <title>Confirmación de aplicación de pago FastEnglish.</title>
      <style>
        body {
          margin: 0;
          padding: 0;
        }
        h1 {
          font-family: Arial, Helvetica, sans-serif;
          font-size: 110%;
          font-weight: bold;
          color: #2E86C1;
        }
      
        h2 {
          font-family: Arial, Helvetica, sans-serif;
          font-size: 100%;
          font-weight: bold;
          color: #2E86C1 ;
        }
      p{
        color: #666;
        font-family: Arial, Helvetica, sans-serif;
      }
        .center {
          position: relative;
          margin: auto;
          padding: auto;
          text-align: center;
        }
        .contenedor {
          width: 80%;
          height: auto;
          padding: 0;
          margin: auto;
          background-color: #fff;
        }
        .centro {
          width: 200px;
          height: 30px;
          vertical-align: middle;
        }
        .asado:active {
          border: none;
        }
        .linea {
          width: 100%;
      height: 1px;
      border: 1px solid #cdcdcd;
        }
        .go {
          background-color:#FF5733;
          color:#fff;
          border:none;
          padding:10px 15px 10px 15px;
          font-family: Arial, Helvetica, sans-serif;
        }
        .dato-title {
          font-size: 100%;
          font-weight: bold;
          font-family: Arial, Helvetica, sans-serif;
        }
        .dato-description {
          font-size: 100%;
          font-weight: normal;
          font-family: Arial, Helvetica, sans-serif;
        }
      
      </style>
      
      
      </head>
      <body>
        <div class=\"contenedor\">
          <h1>INBI</h1>
          
          <h2>¡Confirmación de aplicación de pago FastEnglish!</h2>
      
          <p>Hola ".$nombreAlumno.", Nos da alegría poder confirmarte que tu pago ha sido elaborado de manera correcta, 
          esperamos verte pronto en clase. Te enviamos los datos de tu pago y para continuar con la atención personalizada, 
          favor de dar clic en el link al final de este mail. 
          </p>
          
          <br/>
          <span class=\"dato_tile\">DATOS GENERALES.</span>
          <br/>

          <table>
          <tr><td>Nombre</td><td>".$nombreAlumno."</td></tr>
          <tr><td>Apellido Paterno</td><td>".$apaternoAlumno."</td></tr>
          <tr><td>Apellido Materno</td><td>".$amaternoAlumno."</td></tr>
          <tr><td>Edad</td><td>".$edadAlumno."</td></tr>
          <tr><td>Telefono</td><td>".$telefonoAlumno."</td></tr>
          <tr><td>Celular</td><td>".$celularAlumno."</td></tr>
          <tr><td>Correo</td><td>".$correoAlumno."</td></tr>
          </table>

          <br/>
          <span class=\"dato_tile\">DATOS DE PAGO</span>
          <br/>
          
          <table>
          <tr><td>Terminacion ulitmos 4 digitos:</td><td>".$cuatrodigitosAlumno."</td></tr>
          <tr><td>Sucursal</td><td>".$nombreSucursal."</td></tr>
            ";
            if($metodopagoAlumno=="deposito"){
            
              $message .="  <tr><td>Afiliación</td><td>$afiliacion</td></tr>
              <tr><td>Autorizacion</td><td>$autorizacion</td></tr>";

            }else if($metodopagoAlumno=="transferencia"){
              $message .="  <tr><td>Clave Rastreo</td><td>$clave_rastreo</td></tr>
              <tr><td>Referencia</td><td>$referencia</td></tr>";
            }
            $message .="
          </table>

          <p>Para continuar tu inscripción , clic aquí (link que dirija a la sucursal que eligió).</p>


          <br/><br/><br/>
        </div>
      </body>
      </html>
      ";
      
      mail($correoAlumno, $subject, $message, $headers);
      #### Termina correo para el alumno





# ...................... Inicia correo Sucursal

//Se le notifica la confirmacion de pago del alumno nombre, asignado a su sucursal.
$subject = 'Confirmación de pago FastEnglish, sucursal '.$nombreSucursal.' ';
      
$correo = "pagos@fastenglish.com.mx";
$headers = "From: " . $correo. "\r\n";
$headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

$message = "
<!doctype html>
<html lang=\"en\">
  <head>
    <title>Confirmación de aplicacion de pago FastEnglish, sucursal " . $nombreSucursal. " </title>
<style>
  body {
    margin: 0;
    padding: 0;
  }
  h1 {
    font-family: Arial, Helvetica, sans-serif;
    font-size: 110%;
    font-weight: bold;
    color: #2E86C1;
  }

  h2 {
    font-family: Arial, Helvetica, sans-serif;
    font-size: 100%;
    font-weight: bold;
    color: #2E86C1 ;
  }
p{
  color: #666;
  font-family: Arial, Helvetica, sans-serif;
}
  .center {
    position: relative;
    margin: auto;
    padding: auto;
    text-align: center;
  }
  .contenedor {
    width: 80%;
    height: auto;
    padding: 0;
    margin: auto;
    background-color: #fff;
  }
  .centro {
    width: 200px;
    height: 30px;
    vertical-align: middle;
  }
  .asado:active {
    border: none;
  }
  .linea {
    width: 100%;
height: 1px;
border: 1px solid #cdcdcd;
  }
  .go {
    background-color:#FF5733;
    color:#fff;
    border:none;
    padding:10px 15px 10px 15px;
    font-family: Arial, Helvetica, sans-serif;
  }
  .dato-title {
    font-size: 100%;
    font-weight: bold;
    font-family: Arial, Helvetica, sans-serif;
  }
  .dato-description {
    font-size: 100%;
    font-weight: normal;
    font-family: Arial, Helvetica, sans-serif;
  }

</style>


</head>
<body>
  <div class=\"contenedor\">
    <h1>FastEnglish</h1>
    
    <h2>¡Confirmacion de aplicación de pago FastEnglish, sucursal ".$nombreSucursal."</h2>

    <p>Hola ".$nombreAlumno.", Nos da alegría poder confirmarte que tu pago ha sido elaborado de manera correcta, 
    esperamos verte pronto en clase. Te enviamos los datos de tu pago y para continuar con la atención personalizada, 
    favor de dar clic en el link al final de este mail. 
    </p>
    
    <br/>
    <span class=\"dato_tile\">DATOS GENERALES.</span>
    <br/>

    <table>
    <tr><td>Nombre</td><td>".$nombreAlumno."</td></tr>
    <tr><td>Apellido Paterno</td><td>".$apaternoAlumno."</td></tr>
    <tr><td>Apellido Materno</td><td>".$amaternoAlumno."</td></tr>
    <tr><td>Edad</td><td>".$edadAlumno."</td></tr>
    <tr><td>Telefono</td><td>".$telefonoAlumno."</td></tr>
    <tr><td>Celular</td><td>".$celularAlumno."</td></tr>
    <tr><td>Correo</td><td>".$correoAlumno."</td></tr>
    </table>

    <br/>
    <span class=\"dato_tile\">DATOS DE PAGO</span>
    <br/>
    
    <table>
    <tr><td>Terminacion ulitmos 4 digitos:</td><td>".$cuatrodigitosAlumno."</td></tr>
    <tr><td>Sucursal</td><td>".$nombreSucursal."</td></tr>
      ";
      if($metodopagoAlumno=="deposito"){
      
        $message .="  <tr><td>Afiliación</td><td>$afiliacion</td></tr>
        <tr><td>Autorizacion</td><td>$autorizacion</td></tr>";

      }else if($metodopagoAlumno=="transferencia"){
        $message .="  <tr><td>Clave Rastreo</td><td>$clave_rastreo</td></tr>
        <tr><td>Referencia</td><td>$referencia</td></tr>";
      }
      $message .="
    </table>

    <br/><br/><br/>
  </div>
</body>
</html>
";

mail($correoSucursal, $subject, $message, $headers);
# ...................... Termina correo Sucursal

      echo "exito";
    } else {
      echo "error";
    }
  

 
?>
