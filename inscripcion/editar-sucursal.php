<?php
include('inc/session.php');
require_once 'inc/conexion.php';
date_default_timezone_set('America/Monterrey');
?>
<!doctype html>
<html lang="en">
  <head>
    <meta http-equiv=”Content-Type” content=”text/html; charset=UTF-8″ />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/customer.css">
    <script type="text/script" src="js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="font-awesome/css/font-awesome.css">
    <title>Plataforma de registros de Inscripcion</title>
  </head>
  <body>

  <div class="container"> <!--Inicia Container -->
        <div class="row">    
          <div class="col-md-2"></div>
          <div class="col-md-6 text-center"><img src="logo/cropped-logo-fast-png-1.png" width="155" height="131"></div>
          <div class="col-md-4">
            <div> 
            Hola <?=$_SESSION["fast"][1];?> | <a href="inc/salir.php"> <i class="fa fa-sign-out" aria-hidden="true"></i> </a>   
            <br/>
            Sucursal <?php echo $nomSucSession;?>
            </div>
          </div>
        </div>
  </div>
  <br/><br/><br/>
  <?php
    // Obtener por get el id de la sucursal
    $idUsuario = $_GET['id'];
  ?>

<form action="editar-sucursal.php" method="post">
  <div class="container"> <!--Inicia Container Fluid-->
    <div class="row">
      <div class="col-md-4"></div>
      <div class="col-md-4">
      <?php
          $sql = "SELECT * FROM sucursales";
              if (!$resultado = $conn->query($sql)) {
                echo "Error al buscar las su ursales";
              exit;
          }                    
      ?>                  
                        
        <select class="form-control form-control-sm" name="sucursal" id="sucursal" required>
        <option value="">Seleccione una opción</option>
        <?php
        while($data = $resultado->fetch_assoc()){
        ?>
        <option value="<?php echo $data['id'];?>"><?php echo $data['nombre'];?></option>
        <?php
        }
        ?>
        </select>
      </div>
      <div class="col-md-4"></div>
    </div>
    <br/><br/><br/>
    <div class="row">
    <div class="col-md-4"></div>
      <div class="col-md-4">
          <div class="text-center">
          <input type="hidden" name="idUsuario" id="idUsuario" value="<?php echo $idUsuario;?>">
            <button type="submit" name="btnEditarSucursal" id="btnEditarSucursal" class="btn btn-primary btn-sm"> Guardar. </button>
          </div>
      </div>
    <div class="col-md-4"></div>
    </div>

  </div>
  </form>
  


  <?php
  // si se envio el formulario procesar hacer el update y redireccionar al panel de inscripciones
  if(isset($_POST['btnEditarSucursal'])){
    $idUsuario = $_POST['idUsuario'];
    $sucursal  = $_POST['sucursal'];
    $sqlUpdate = "UPDATE inscripciones_web SET sucursal=$sucursal where id=$idUsuario limit 1";
  

    if ($conn->query($sqlUpdate)) {
      ?>
      <script>
      alert("Se actualizo la sucursal");
      setTimeout(function(){ window.location.href = "consulta.php"; }, 1000);
      </script>
      <?php
    }else{
      ?>
      <script>
      alert("No se pudo actualizar la sucursal por un detalle técnico, consulte con el administrador del sistema.");
      setTimeout(function(){ window.location.href = "consulta.php"; }, 1000);
      </script>
      <?php
    }

  }
  ?>


</body>
</html>