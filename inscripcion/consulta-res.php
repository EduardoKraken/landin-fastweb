<?php
include('inc/session.php');
require_once 'inc/conexion.php';
date_default_timezone_set('America/Monterrey');
?>
<!doctype html>
<html lang="en">
  <head>
    <meta http-equiv=”Content-Type” content=”text/html; charset=UTF-8″ />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/customer.css">
    <script type="text/script" src="js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="font-awesome/css/font-awesome.css">
    <title>Plataforma de registros de Inscripcion</title>

  </head>
  <style>
  #mostrarBaucher{
    position:relative;
    width:50%;
    height:50%;
    display:none;
    background-color:#5D6D7E;
    justify-content: center;
    align-items: center;
    margin:auto;
    padding:0;
  }

 
  .btnCerrarVentana {
    color:#fff;
    text-decoration:none;
  }
 

</style>
  <body>
  <?php
    $roll            = $_SESSION["fast"][2];
    $sessionSucursal = $_SESSION["fast"][3];
    if(!empty($sessionSucursal)){
        $sqlSucSess = "select nombre from sucursales where id=$sessionSucursal";

        if (!$rSu = $conn->query($sqlSucSess)) {
          echo "Error al obtener los registros de sucursales";
          exit;
        }

        $dss = $rSu->fetch_assoc();
        $nomSucSession = $dss['nombre'];
      }else{
        $nomSucSession = "Administrador";
      }
      ?>

  <div class="container"> <!--Inicia Container -->
        <div class="row">    
          <div class="col-md-2"></div>
          <div class="col-md-6 text-center"><img src="logo/cropped-logo-fast-png-1.png" width="155" height="131"></div>
          <div class="col-md-4">
            <div> 
            Hola <?=$_SESSION["fast"][1];?> | <a href="inc/salir.php"> <i class="fa fa-sign-out" aria-hidden="true"></i> </a>   
            <br/>
            Sucursal <?php echo $nomSucSession;?>
            </div>
          </div>
        </div>
  </div>


  <div class="container-fluid"> <!--Inicia Container Fluid-->
        <div class="row">    
          
          <div class="col-md-12">
          <?PHP
          $TAMANO_PAGINA = 10;
          $pagina = $_GET["pagina"];
          if (!$pagina) {
              $inicio = 0;
              $pagina=1;
          }else {
              $inicio = ($pagina - 1) * $TAMANO_PAGINA;
          }

          // vaildar si es direccion o es administrador
          // si es direccion validar que solo traiga los registros de la sucursal
          
          



          if($roll=="direccion"){
            $ssql = "select * from inscripciones_web where sucursal= $sessionSucursal";
          }else{
            $ssql = "select * from inscripciones_web";
          }

          // $ssql = "select * from inscripciones_web where pago_confirmado is null";
          $rs   = $conn->query($ssql);
          $numTotalRegistros = $rs->num_rows;
          $totalPaginas      = ceil($numTotalRegistros / $TAMANO_PAGINA);
          //pongo el número de registros total, el tamaño de página y la página que se muestra
          echo "Número de registros encontrados: <span class='badge badge-success'>" . $numTotalRegistros . "</span> <br>";
          echo "Se muestran páginas de " . $TAMANO_PAGINA . " registros cada una<br>";
          echo "Mostrando la página " . $pagina . " de " . $totalPaginas . "<p>";
          ?>
          <br/><br/>

          <?PHP
          if($roll=="direccion"){
            $sqlRegistros = "select id,tipoAlumno,nombre,apaterno,amaterno,celular,correo,metodo_pago,cuatro_digitos,sucursal,afiliacion,autorizacion,clave_rastreo,referencia,ruta_imagen,fecha_creacion,pago_confirmado 
            from inscripciones_web where sucursal= " . $sessionSucursal . " limit " . $inicio . "," . $TAMANO_PAGINA;
          }else{
            $sqlRegistros = "select id,tipoAlumno,nombre,apaterno,amaterno,celular,correo,metodo_pago,cuatro_digitos,sucursal,afiliacion,autorizacion,clave_rastreo,referencia,ruta_imagen,fecha_creacion,pago_confirmado
             from inscripciones_web limit " . $inicio . "," . $TAMANO_PAGINA;
          }



            if (!$resultado = $conn->query($sqlRegistros)) {
                echo "Error al obtener los registros";
                exit;
            }
            $numReg = $resultado->num_rows;
            ?>
  
            <br/>
            <style>
            table th {
              background-color:#3498DB;
              color:#fff;
              font-style:arial;
              font-size:90%;
            }
            .fecha {
              font-size:80%;
            }
            #registros tr:nth-child(even) {
            background-color: #eee;
            }
            #registros tr:nth-child(odd) {
            background-color: #fff;
            }
            input[type="button"] {
            border: none;
            outline:none;
            }

            </style>
            <table id="registros">
            <tr>
            <th><span class="font-weight-bold"> ID </span></th>
            <th><span class="font-weight-bold"> TipoAlumno </span></th>
            <th><span class="font-weight-bold"> Nombre </span></th>
            <th><span class="font-weight-bold"> Apellido Paterno </span></th>
            <th><span class="font-weight-bold"> Apellido Materno </span></th>
            <th><span class="font-weight-bold"> Celular</span></th>
            <th><span class="font-weight-bold"> Correo</span></th>

            <th><span class="font-weight-bold"> Metodo Pago</span></th>
            <th><span class="font-weight-bold"> Terminación Cuenta</span></th>
            <th><span class="font-weight-bold"> Afiliación</span></th>
            <th><span class="font-weight-bold"> Autorización</span></th>
            <th><span class="font-weight-bold"> Rastreo</span></th>
            <th><span class="font-weight-bold"> Referencia</span></th>
            <th><span class="font-weight-bold"> Fecha Registro </span></th>
            <th><span class="font-weight-bold"> Status Pago </span></th>

            <th><span class="font-weight-bold"> Sucursal</span></th>
            <th><span class="font-weight-bold"> Ver Baucher</span></th>
            <?php
            if($_SESSION["inbi"][2] == "administracion"){
            ?>
            <th><span class="font-weight-bold"> Confirmar pago </span></th>
            <?php
            }else{
              ?>
              <th></th>
              <?php
            }
            ?>
            </tr>

            <?php
            $i=0;
              while($data = $resultado->fetch_assoc()){
                ?>
                <tr>
                <td><?=$data['id'];?></td>
                <td><?php 
                if($data['tipoAlumno']==1){
                  echo "Nuevo";
                } 
                if($data['tipoAlumno']==2){
                  echo "Inscrito";
                }
                ?></td>
                <td><?=$data['nombre'];?></td>
                <td><?=$data['apaterno'];?></td>
                <td><?=$data['amaterno'];?></td>
                <td><?=$data['celular'];?></td>
                <td><?=$data['correo'];?></td>

                <td><?php echo ucwords($data['metodo_pago']);?></td>
                <td><?=$data['cuatro_digitos'];?></td>
                <td><?=$data['afiliacion'];?></td>
                <td><?=$data['autorizacion'];?> </td>
                <td><?=$data['clave_rastreo'];?></td>
                <td><?=$data['referencia'];?> </td>
                <td><?=$data['fecha_creacion'];?> </td>
                <td class="fecha text-center">
                  <?php
                  if($data['pago_confirmado'] == null){
                    echo "Por Confirmar";
                  }else{
                    echo "Aplicado";
                  }
                  ?>
                </td>


                <td>
                <?php
                if(!empty($data['sucursal'])){
                $sqlSucursal = "select nombre from sucursales where id=".$data['sucursal']."";

                if (!$resSuc = $conn->query($sqlSucursal)) {
                  echo "Error al obtener los registros de sucursales";
                  exit;
                }
                $ds = $resSuc->fetch_assoc();
                echo $ds['nombre'];
                }else{
                  echo "N/A";
                }
                ?>
                </td>
                <td class="text-center">
                <a class="text-weight" href="#" onclick="mostrarBaucher('<?php echo $data['ruta_imagen'];?>')"> 
                <i class="fa fa-picture-o" aria-hidden="true"></i>
                </a>
                </td>
                <?php
                if($_SESSION["fast"][2] == "administracion"){
                  if($data['pago_confirmado']!=null){

                  }else{
                  ?>
                  <td class="fecha text-center">
                    <a href="#" onclick="registrado(<?php echo $data['id'];?>)">
                    <i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i></a>
                  </td>
                  <?php
                    }
                  }
                  ?>
                </tr>
                <?php
              
              }
            ?>
            </table>
            <br/><br/><br/>
      
      </div>
    </div>
  </div>


  <div class="container">
    <div class="row">
      <div class="col-md-12">
      <nav aria-label="Page navigation example">
            <ul class="pagination flex-wrap">

              <?PHP
              //muestro los distintos índices de las páginas, si es que hay varias páginas
              if ($totalPaginas > 1){
                for ($i=1;$i<=$totalPaginas;$i++){
                  if ($pagina == $i){
 
                      //si muestro el índice de la página actual, no coloco enlace
                      echo "<li class='page-item active'>";
                      echo "<a class='page-link' href=''> $pagina <span class='sr-only'>(current)</span></a>";
                      echo "</li>";
                  }else{

                      //si el índice no corresponde con la página mostrada actualmente, coloco el enlace para ir a esa página
                      echo "<li class='page-item'> <a class='page-link' href='consulta.php?pagina=" . $i . "'>" . $i . "</a></li>";
                  }
                }
              }
              ?>

            </ul>
            </nav>

      </div>
    </div>
  </div>

<div id="mostrarBaucher">

</div>
  <script>
  function cerrarVentana(){
  let mostrarBaucher = document.getElementById("mostrarBaucher");
  mostrarBaucher.style.display="none";
  mostrarBaucher.innerHTML="";
  }

  function mostrarBaucher(ruta){
  let mostrarBaucher = document.getElementById("mostrarBaucher");

  mostrarBaucher.style.display="block";
  mostrarBaucher.style.marginTop="-250px";
  //document.getElementById("mostrarBaucher").innerHTML=ruta;
  $('#mostrarBaucher').append('<div class="container">\
          <div class="row">\
          <div class="col-md-12">\
          <div class="text-right"><a href="#" onclick="cerrarVentana()"><span class="btnCerrarVentana">Cerrar[x]</span></a></div>\
            <img class="img-fluid" src="' + ruta +'">\
          </div>\
            </div>\
          </div>');
}
  </script>

  <script src="js/jquery/jquery-3.3.1.min.js"></script>
  <script>
function registrado(valor){
  var editar = confirm("Actualizar el estatus a revisado");
  if (editar==true){
      $.ajax({
        type: "POST",
        url: "registrar-pago-confirmado.php",
        data: "identificador=" + valor,
        success : function(text){
            
        limpio = text.trim();
        if(limpio == "exito"){
          alert("Se modifico exitosamente");
        }else{
          alert("No se pudo modificar, consulte con el administrador del sistema");
        }      
      }
      });
  }


  setTimeout(function(){ window.location.href = "consulta.php"; }, 1000);

/*
$.ajax({
    type: "POST",
    url: "actualizar-registro.php",
    data: "identificador=" + valor,
    success : function(text){
      
      limpio = text.trim();
      if(limpio == "exito"){
        alert("OK");
      }else{
        alert("NOK");
      }
      
    }
});*/

}



/*
$("#form-asignar").submit(function(event){
event.preventDefault();
mazinger();
});*/
</script>

</body>
</html>