<?php
include('inc/conexion.php');
?>
<!doctype html>
<html lang="en">
  <head>
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-77502498-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-77502498-1');
</script>
    <meta http-equiv=”Content-Type” content=”text/html; charset=UTF-8″ />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/customer.css">
    <script type="text/script" src="js/bootstrap.min.js"></script>
    <title>Comprobante de pago</title>

  </head>
  <body>
    <div class="container"> <!--Inicia Container -->
    <div class="row">
        <div class="col-md-4 text-center"><img src="logo/cropped-logo-fast-png-1.png" width="155" height="131"></div>
        <div class="col-md-4 text-center"><h4>Inscripciones</h4></div>
        <div class="col-md-4"></div>
    </div>

    <div class="row"> 
      <div class="col-md-4"></div>
      <div class="col-md-4 text-center">
      ¿ Alumno nuevo / Alumno existente ?
      <br/>
      <br/>
        <select class="form-control form-control-sm" name="tipoAlumno" id="tipoAlumno">
          <option value="">Seleccione una opcion</option>
          <option value="1">Soy alumno nuevo</option>
          <option value="2">Ya soy Alumno</option>
        </select>
      </div>
      <div class="col-md-4"></div>

    </div>
<br/><br/>
<div id="alumnoNuevo">
    <div class="row">   
            <div class="col-md-2 text-center"></div>
              <div class="col-md-8">
              
              <form action="procesar.php" method="post" onsubmit="return validacionNuevo()" enctype="multipart/form-data">
                  <p class="titulo"> Datos Alumno </p>
                  
                  <div class="espacioDos"></div>
                  <div class="row">
                        <div class="col-md-6">
                          <label for="nombre">Nombre </label>
                          <input type="text" class="form-control form-control-sm" name="nombre" id="nombre" required>
                        </div>
                        <div class="col-md-6">
                          <label for="correo">Apellido Paterno </label>
                          <input type="text" class="form-control form-control-sm" name="apaterno" id="apaterno" required>
                        </div>
                  </div>

                  <div class="row">
                        <div class="col-md-6">
                          <label for="nombre">Apellido Materno </label>
                          <input type="text" class="form-control form-control-sm" name="amaterno" id="amaterno" required>
                        </div>
                        <div class="col-md-6">
                          <label for="correo">Edad </label>
                          <input type="text" class="form-control form-control-sm" name="edad" maxlength="2"  id="edad" required>
                        </div>
                  </div>


                  <div class="row">
                        <div class="col-md-6">
                          <label for="nombre">Teléfono </label>
                          <input type="text" class="form-control form-control-sm" name="telefonoNuevo" maxlength="10" id="telefonoNuevo" required>
                        </div>
                        <div class="col-md-6">
                          <label for="correo">Celular </label>
                          <input type="text" class="form-control form-control-sm" name="celularNuevo" maxlength="10" id="celularNuevo" required>
                        </div>
                  </div>

                  <div class="row">
                        <div class="col-md-6">
                          <label for="nombre">Correo </label>
                          <input type="text" class="form-control form-control-sm" name="correo" id="correo" required>
                        </div>
                        <div class="col-md-6">

                        </div>
                  </div>
                  <div class="espacioDos"></div>
                  <hr class="linea"/>
                  <div class="espacioDos"></div>
                  <p class="titulo">Datos Padre o Tutor</p>
                  <div class="espacioDos"></div>
                  <div class="row">
                        <div class="col-md-6">
                          <label for="Nombre">Nombre </label>
                          <input type="text" class="form-control form-control-sm" name="nombreTutor" id="nombreTutor" required>
                        </div>
                        <div class="col-md-6">
                        <label for="Apellido Paterno">Apelido Paterno </label>
                          <input type="text" class="form-control form-control-sm" name="apaternoTutor" id="apaternoTutor" required>
                        </div>
                  </div>

               
                  <div class="row">
                        <div class="col-md-6">
                          <label for="Apellido Materno">Apellido Materno </label>
                          <input type="text" class="form-control form-control-sm" name="amaternoTutor" id="amaternoTutor" required>
                        </div>
                        <div class="col-md-6">
                        <label for="parentesco"> Parentesco</label>
                          <input type="text" class="form-control form-control-sm" name="parentesco" id="parentesco" required>
                        </div>
                  </div>


                   <div class="row">
                        <div class="col-md-6">
                          <label for="telefono_casa">Telefono Casa </label>
                          <input type="text" class="form-control form-control-sm" name="telefonoTutor" maxlength="10" id="telefonoTutor" required>
                        </div>
                        <div class="col-md-6">
                        <label for="celular"> Celular</label>
                          <input type="text" class="form-control form-control-sm" name="celularTutor" maxlength="10"  id="celularTutor" required>
                        </div>
                  </div>
                  <div class="espacioDos"></div>
                  <hr class="linea"/>
                  <div class="espacioDos"></div>
                  <p class="titulo">Datos de pago</p>
                  <div class="espacioDos"></div>

                  <div class="form-group">                 
                          <label for="Metodo d pago"> Metódo de pago</label>
                          <div class="form-check">
                          <input class="form-check-input form-control-sm" onclick="metodoPago()" type="radio" name="metodo_pago" value="deposito" id="deposito" required> 
                          <label class="form-check-label" for="exampleRadios1">
                              Deposito
                          </label>
                          </div>

                          <div class="form-check">
                          <input class="form-check-input form-control-sm" onclick="metodoPago()" type="radio" name="metodo_pago" value="transferencia" id="transferencia" required>
                          <label class="form-check-label" for="exampleRadios1">
                              Transferencia
                          </label>
                          </div>
                  </div>

                  <div class="espacioDos"></div>
                
                <div class="row">
                        <div class="col-md-6">
                          <label for="telefono_casa">Terminación ultimo 4 digitos cuenta </label>
                          <input type="text" class="form-control form-control-sm" name="cuatro_dijitos" maxlength="4" id="cuatro_dijitos" required>
                        </div>
                        <div class="col-md-6">
                        <label for="celular"> Sucursal Interes</label>
                        <?php
                          $sql = "SELECT * FROM sucursales";
                          if (!$resultado = $conn->query($sql)) {
                            echo "Error al buscar las su ursales";
                            exit;
                          }
                          
                          ?>

                          
                        
                          <select class="form-control form-control-sm" name="sucursal" id="sucursal" required>
                          <option value="">Seleccione una opción</option>
                          <?php
                          while($data = $resultado->fetch_assoc()){
                          ?>
                            <option value="<?php echo $data['id'];?>"><?php echo $data['nombre'];?></option>
                            <?php
                          }
                          ?>
                          </select>
                        </div>
                </div>
                  
                  
                  <div class="espacioDos"></div>

                <div id="depositoInfo">
                    <div class="row">
                        <div class="col-md-6">
                          <label for="afiliacion">Afiliación </label>
                          <input type="text" class="form-control form-control-sm" name="afiliacion" id="afiliacion">
                        </div>
                        <div class="col-md-6">
                        <label for="autorizacion"> Autorización</label>
                          <input type="text" class="form-control form-control-sm" name="autorizacion" id="autorizacion">
                        </div>
                    </div>
                </div>
                

                <div id="transferenciaInfo">
                  <div class="row">
                    <div class="col-md-6">
                          <label for="afiliacion"> Clave rastreo </label>
                          <input type="text" class="form-control form-control-sm" name="clave_rastreo" id="clave_rastreo">
                    </div>
                    <div class="col-md-6">
                        <label for="autorizacion"> Referencia</label>
                          <input type="text" class="form-control form-control-sm" name="referencia" id="referencia">
                    </div>
                  </div>
                </div>
                <div class="espacioDos"></div>

                <div class="form-group">
                      <p>Cargar imagen o Tomar una foto.</p>
                      <input type="file" name="mypic" id="mypic" accept="image/*" required>
                      </div>
                      
                      
                      <div class="form-group">
                      <canvas id="mostrar-foto"></canvas>
                      </div>

                      <div class="espacioDos"></div>
                  <div class="text-center">
                    <button type="submit" name="btnAlumnoNuevo" id="btnAlumnoNuevo" class="btn btn-primary btn-sm"> Enviar. </button>
                  </div>
                  <div class="espacioDos"></div>
              </form>
              <br/><br/><br/>
              </div>
            <div class="col-md-2"></div>
    </div>
    </div>



<!-- ######################################## Inicia form de alumno ya inscrito-->


<div id="alumno">
    <div class="row">   
            <div class="col-md-2 text-center"></div>
              <div class="col-md-8">
              <form name="formexiste" action="procesar.php" method="post" onsubmit="return validacionExiste()" id="formexiste" enctype="multipart/form-data">
              <p class="titulo">Datos alumno</p>
                      <div class="row">
                        <div class="col-md-6">
                          <label for="nombre">Nombre </label>
                          <input type="text" class="form-control form-control-sm" name="nombre" id="nombre" required>
                        </div>
                        <div class="col-md-6">
                          <label for="ApellidoPaterno">Apellido Paterno </label>
                          <input type="text" class="form-control form-control-sm" name="apaterno" id="apaterno" required>
                        </div>
                      </div>


                      <div class="row">
                        <div class="col-md-6">
                          <label for="nombre">Apellido Materno </label>
                          <input type="text" class="form-control form-control-sm" name="amaterno" id="amaterno" required>
                        </div>
                        <div class="col-md-6">
                          <label for="correo">Correo </label>
                          <input type="email" class="form-control form-control-sm" name="correo" id="correo" required>
                        </div>
                      </div>

                
                      <div class="row">
                        <div class="col-md-6">
                          <label for="nombre">Teléfono </label>
                          <input type="text" class="form-control form-control-sm" name="telefono" maxlength="10" id="telefono" required>
                        </div>
                        <div class="col-md-6">

                        </div>
                      </div>










                      <div class="espacioDos"></div>
                  <hr class="linea"/>
                  <div class="espacioDos"></div>
                  <p class="titulo">Datos de pago</p>
                  <div class="espacioDos"></div>

                  <div class="form-group">                 
                          <label for="Metodo d pago"> Metódo de pago</label>
                          <div class="form-check">
                          <input class="form-check-input form-control-sm" onclick="metodoPagoExiste()" type="radio" name="metodo_pago" value="deposito" id="depositoExiste" required> 
                          <label class="form-check-label" for="exampleRadios1">
                              Deposito
                          </label>
                          </div>

                          <div class="form-check">
                          <input class="form-check-input form-control-sm" onclick="metodoPagoExiste()" type="radio" name="metodo_pago" value="transferencia" id="transferenciaExiste" required>
                          <label class="form-check-label" for="exampleRadios1">
                              Transferencia
                          </label>
                          </div>
                  </div>

                  <div class="espacioDos"></div>
                
                <div class="row">
                        <div class="col-md-6">
                          <label for="telefono_casa">Terminación ultimo 4 digitos cuenta </label>
                          <input type="text" class="form-control form-control-sm" name="cuatro_dijitos" maxlength="4" id="cuatro_dijitos" required>
                        </div>
                        <div class="col-md-6">
                        <label for="celular"> Sucursal Interes</label>
                        <?php
                          $sql = "SELECT * FROM sucursales";
                          if (!$resultado = $conn->query($sql)) {
                            echo "Error al buscar las sucursales";
                            exit;
                          }
                          
                          ?>

                          
                        
                          <select class="form-control form-control-sm" name="sucursal" id="sucursal" required>
                          <option value="">Seleccione una opción</option>
                          <?php
                          while($data = $resultado->fetch_assoc()){
                            ?>
                            <option value="<?php echo $data['id'];?>"><?php echo $data['nombre'];?></option>
                            <?php
                          }
                          ?>
                          </select>
                        </div>
                </div>
                  <div class="espacioDos"></div>

                <div id="depositoInfoExiste">
                    <div class="row">
                        <div class="col-md-6">
                          <label for="afiliacion">Afiliación </label>
                          <input type="text" class="form-control form-control-sm" name="afiliacion" id="afiliacionExiste">
                        </div>
                        <div class="col-md-6">
                        <label for="autorizacion"> Autorización</label>
                          <input type="text" class="form-control form-control-sm" name="autorizacion" id="autorizacionExiste">
                        </div>
                    </div>
                </div>
                
               

                <div id="transferenciaInfoExiste">
                  <div class="row">
                    <div class="col-md-6">
                          <label for="afiliacion"> Clave rastreo </label>
                          <input type="text" class="form-control form-control-sm" name="clave_rastreo" id="clave_rastreoExiste">
                    </div>
                    <div class="col-md-6">
                        <label for="autorizacion"> Referencia</label>
                          <input type="text" class="form-control form-control-sm" name="referencia" id="referenciaExiste">
                    </div>
                  </div>
                </div>
                <div class="espacioDos"></div>


                      <div class="espacioDos"></div>

                      <div class="form-group">
                      <p>Cargar imagen o Tomar una foto.</p>
                      <input type="file" name="baucher" id="baucher" accept="image/*" required>
                      </div>
                      


                      
                      <div class="col">
                      <canvas id="mostrar-baucher"></canvas>
                      </div>

                      <div id="mostrarProgressBar">

                      <div class="espacioDos"></div>
                      <div class="class">
                        <div class="row">
                          <div class="col-md-3"></div>
                          <div class="col-md-6">
                            <div class="progress">
                              <div class="progress-bar progress-bar-striped bg-info progress-bar-animated" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                          </div>
                          <div class="col-md-3"></div>
                        </div>
                      </div>
                      </div>


                      <div class="espacioDos"></div>
                  <div class="text-center">
                    <button type="submit" name="btnAlumnoInscrito" id="btnAlumnoInscrito" class="btn btn-primary btn-sm"> Enviar. </button>
                  </div>
                  <div class="espacioDos"></div><div class="espacioDos"></div>
                  </form>
              </div>
            <div class="col-md-2 text-center"></div>
    </div>
</div>

</div><!--Termima container-->




<script>
const aInscrito = document.querySelector('#btnAlumnoInscrito');
aInscrito.addEventListener('click', (event) => {
  let mostrarProgressBar  = document.getElementById("mostrarProgressBar");
      mostrarProgressBar.style.display="block";
      mostrarBaucher.style.marginTop="-300px";
      /*mostrarBaucher.margin:auto;
      mostrarBaucher.padding:0px;*/
});
</script>



<!--Script para validar el tipo de pago y mostrar informacion correspondiente -->
<script>
function metodoPago(){
  let mpdeposito      = document.getElementById("depositoInfo");
  let mptransferencia = document.getElementById("transferenciaInfo");

  if(document.getElementById("deposito").checked==true){
    mpdeposito.style.display="block";
    mptransferencia.style.display="none";
  }
  
  if(document.getElementById("transferencia").checked==true){
    mptransferencia.style.display="block";
    mpdeposito.style.display="none";
  }

}
</script>

<script>
function metodoPagoExiste(){
  let mpdepositoExiste      = document.getElementById("depositoInfoExiste");
  let mptransferenciaExiste = document.getElementById("transferenciaInfoExiste");

  if(document.getElementById("depositoExiste").checked==true){
    mpdepositoExiste.style.display="block";
    mptransferenciaExiste.style.display="none";
  }
  
  if(document.getElementById("transferenciaExiste").checked==true){
    mptransferenciaExiste.style.display="block";
    mpdepositoExiste.style.display="none";
  }

}
</script>

<!-- Java Script para mostrar contenido Alumno Nuevo y Alemuno exitente  -->
<script>
const selectElement = document.querySelector('#tipoAlumno');
selectElement.addEventListener('change', (event) => {
  let alumnoNuevo = document.getElementById("alumnoNuevo");
  let alumno      = document.getElementById("alumno");
  let parametro = event.target.value;
  if(parametro==1){
    alumnoNuevo.style.display="block";
    alumno.style.display="none";
    
  }else if(parametro==2){
    alumno.style.display="block";
    alumnoNuevo.style.display="none";
    
  }else if(parametro==0){
    alumno.style.display="none";
    alumnoNuevo.style.display="none";
  }
});
</script>

    
    <script>
    var input = document.querySelector('#mypic');

    input.onchange = function () { // relaciono el id con un evento de cambio
    var file = input.files[0];
    //upload(file);
    document.getElementById("mostrar-foto").style.display = "block";
    drawOnCanvas(file);
    };
/*
  function upload(file) {
    var form = new FormData(),
    xhr = new XMLHttpRequest();
 
    form.append('image', file);
    xhr.open('post', 'procesar.php', true);
    xhr.send(form);
  }*/
 
  function drawOnCanvas(file) {
    var reader = new FileReader();
    reader.onload = function (e) {
      var dataURL = e.target.result,
          c = document.querySelector("#mostrar-foto"), // see Example 4
          ctx = c.getContext('2d'),
          img = new Image();
 
      img.onload = function() {
        c.width = img.width;
        c.height = img.height;
        ctx.drawImage(img, 0, 0);
      }; 
      img.src = dataURL;
    };

    reader.readAsDataURL(file); 
  }
</script>





<script>
// segundo upload de baucher
var baucher = document.querySelector('#baucher');

baucher.onchange = function () { // relaciono el id con un evento de cambio
var bau = baucher.files[0];
document.getElementById("mostrar-baucher").style.display = "block";
//uploadBaucher(bau);
drawOnCanvasBaucher(bau);
};
/*
function uploadBaucher(bau) {
    var form = new FormData(),
    xhr = new XMLHttpRequest();
 
    form.append('image', bau);
    xhr.open('post', 'procesar.php', true);
    xhr.send(form);
}
*/

function drawOnCanvasBaucher(bau) {
    var reader = new FileReader();
    reader.onload = function (e) {
      var dataURL = e.target.result,
          c = document.querySelector("#mostrar-baucher"), // see Example 4
          ctx = c.getContext('2d'),
          img = new Image();
 
      img.onload = function() {
        c.width = img.width;
        c.height = img.height;
        ctx.drawImage(img, 0, 0);
      }; 
      img.src = dataURL;
    };

    reader.readAsDataURL(bau); 
  }
</script>


<script>
function validacionNuevo(){
  let telefonoNuevoAlumno = document.getElementById('telefonoNuevo').value;
  let celularNuevoAlumno  = document.getElementById('celularNuevo').value;

  telefonoNuevoAlumno = telefonoNuevoAlumno.trim();
  celularNuevoAlumno  = celularNuevoAlumno.trim();

      if(telefonoNuevoAlumno==celularNuevoAlumno){
        alert('El teléfono de casa del Alumno y el celular deben de ser diferentes');
        return false;
      }
    
  let telefonoTutor = document.getElementById('telefonoTutor').value;
  let celularTutor  = document.getElementById('celularTutor').value;
   if(telefonoTutor==celularTutor){
      alert('El teléfono  y el celular del Tutor deben de ser diferentes');
      return false;
    }
    
  
    if(document.getElementById("deposito").checked==true){
      let afiliacion   = document.getElementById("afiliacion").value;
      if(afiliacion ==""){
        alert("Debe de agregar un número de afiliación");
        document.getElementById("afiliacion").focus();
        return false;
      }

      let autorizacion = document.getElementById("autorizacion").value;
      if(autorizacion ==""){
        alert("Debe de agregar un número de autorización");
        document.getElementById("autorizacion").focus();
        return false;
      }

    }


    if(document.getElementById("transferencia").checked==true){

      let clave_rastreo   = document.getElementById("clave_rastreo").value;
      if(clave_rastreo ==""){
        alert("Debe de agregar un número de clave_rastreo");
        document.getElementById("clave_rastreo").focus();
        return false;
      }

      let referencia = document.getElementById("referencia").value;
      if(referencia ==""){
        alert("Debe de agregar un número de referencia");
        document.getElementById("referencia").focus();
        return false;
      }
    }




  return true;
}
</script>




<script>
function validacionExiste(){
  if(document.getElementById("depositoExiste").checked==true){
      let afiliacionExiste   = document.getElementById("afiliacionExiste").value;
      if(afiliacionExiste ==""){
        alert("Debe de agregar un número de afiliación");
        document.getElementById("afiliacionExiste").focus();
        return false;
      }

      let autorizacionExiste = document.getElementById("autorizacionExiste").value;
      if(autorizacionExiste ==""){
        alert("Debe de agregar un número de autorización");
        document.getElementById("autorizacionExiste").focus();
        return false;
      }

    }


    if(document.getElementById("transferenciaExiste").checked==true){

      let clave_rastreoExiste   = document.getElementById("clave_rastreoExiste").value;
      if(clave_rastreoExiste ==""){
        alert("Debe de agregar un número de clave_rastreo");
        document.getElementById("clave_rastreoExiste").focus();
        return false;
      }

      let referenciaExiste = document.getElementById("referenciaExiste").value;
      if(referenciaExiste ==""){
        alert("Debe de agregar un número de referencia");
        document.getElementById("referenciaExiste").focus();
        return false;
      }
    }
}
</script>
 </body>
</html>