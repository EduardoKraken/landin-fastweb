<?php
include('inc/conexion.php');

date_default_timezone_set('America/Monterrey');
$fecha_registro = date("Y-m-d H:i:s");
function limpiar($parametro){
  $parametro = str_replace('‘','',$parametro);
  $parametro = str_replace('"','',$parametro);
  $parametro = str_replace('–','',$parametro);
  $parametro = str_replace('/*','',$parametro);
  $parametro = str_replace('*/','',$parametro);
  $parametro = str_replace('xp_','',$parametro);
  $parametro = str_replace('<','',$parametro);
  $parametro = str_replace('>','',$parametro);
  $parametro = str_replace('insert','',$parametro);
  $parametro = str_replace('delete','',$parametro);
  $parametro = str_replace('update','',$parametro);
  $parametro = str_replace('select','',$parametro);
  $parametro = str_replace('drop','',$parametro);
  $parametro = str_replace('table','',$parametro);
  $parametro = str_replace("'","\'",$parametro);
  return $parametro;
  }

if(isset($_POST['btnAlumnoNuevo'])){
  $tipoAlumno    = 1;
  $nombre        = limpiar($_POST['nombre']);
  $apaterno      = limpiar($_POST['apaterno']);
  $amaterno      = limpiar($_POST['amaterno']);
  $edad          = limpiar($_POST['edad']);
  $telefono      = limpiar($_POST['telefonoNuevo']);
  $celular       = limpiar($_POST['celularNuevo']);
  $correo        = limpiar($_POST['correo']);

  $nombreTutor   = limpiar($_POST['nombreTutor']);
  $apaternoTutor = limpiar($_POST['apaternoTutor']);
  $amaternoTutor = limpiar($_POST['amaternoTutor']);
  $parentesco    = limpiar($_POST['parentesco']);
  $telcasaTutor  = limpiar($_POST['telefonoTutor']);
  $celularTutor  = limpiar($_POST['celularTutor']);
  $metodo_pago   = limpiar($_POST['metodo_pago']);
  
  $cuatro_dijitos = limpiar($_POST['cuatro_dijitos']);
  $sucursal       = limpiar($_POST['sucursal']);

  if($metodo_pago=="deposito"){
    $afiliacion    = limpiar($_POST['afiliacion']);
    $autorizacion  = limpiar($_POST['autorizacion']);
    $clave_rastreo = "";
    $referencia    = "";
  }
      
  if($metodo_pago=="transferencia"){
    $afiliacion    = "";
    $autorizacion  = "";
    $clave_rastreo = limpiar($_POST['clave_rastreo']);
    $referencia    = limpiar($_POST['referencia']);
  }

  function aleatorio($length=5){
    $source="ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
    if($length>0){			
      $rstr = "";
      $source = str_split($source,1);
      for($i=1; $i<=$length; $i++){
        mt_srand((double)microtime() * 1000000);
        $num = mt_rand(1,count($source));
        $rstr .= $source[$num-1];				
      }
    }
    return $rstr;
  }
  $r = aleatorio();



  $hoy = date("d-m-Y");
  $directorioRemoto       = "img-upload/$hoy/";
  
    if (!file_exists(dirname(__FILE__)."/".$directorioRemoto)) {
      if (!mkdir(dirname(__FILE__)."/".$directorioRemoto, 0777)){
        $errorCrearDirNuevo= 1;
      }
    }

  $nombreComprobante  = $_FILES['mypic']['name'];
  //$extensionComprobante      = pathinfo($nombreComprobante, PATHINFO_EXTENSION);
  //$rutaCompleta  = $directorioRemoto.basename($nombreComprobante);
  $rutaCompleta  = $directorioRemoto.basename($r.'_'.$nombreComprobante);
 


function compressImage($source, $destination, $quality) { 
    $imgInfo = getimagesize($source); 
    $mime = $imgInfo['mime']; 
    switch($mime){ 
        case 'image/jpeg': 
            $image = imagecreatefromjpeg($source); 
            break; 
        case 'image/png': 
            $image = imagecreatefrompng($source); 
            break; 
        case 'image/gif': 
            $image = imagecreatefromgif($source); 
            break; 
        default: 
            $image = imagecreatefromjpeg($source); 
    } 
     
    imagejpeg($image, $destination, $quality); 
    return $destination; 
} 
 

if(!compressImage($_FILES['mypic']['tmp_name'], $rutaCompleta, 50)){
  $errorMoverVaucherNuevo= 1;
}
/*
  if (move_uploaded_file($_FILES['mypic']['tmp_name'], $rutaCompleta)) {

  }else{
    $errorMoverVaucherNuevo= 1;
  }*/



$sql = "INSERT INTO inscripciones_web (
  tipoAlumno,
  nombre,
  apaterno,
  amaterno,
  edad,
  telefono,
  celular,
  correo,
  nombreTutor,
  apellidoPaternoTutor,
  apellidoMaternoTutor,
  parentesco,
  telcasaTutor,
  celularTutor,
  metodo_pago,
  cuatro_digitos,
  sucursal,
  afiliacion,
  autorizacion,
  clave_rastreo,
  referencia,
  ruta_imagen,
  fecha_creacion) VALUES (
    $tipoAlumno,
  '".$nombre."',
  '".$apaterno."',
  '".$amaterno."',
  $edad,
  '".$telefono."',
  '".$celular."',
  '".$correo."',
  '".$nombreTutor."',
  '".$apaternoTutor."',
  '".$amaternoTutor."',
  '".$parentesco."',
  '".$telcasa."',
  '".$celularTutor."',
  '".$metodo_pago."',
  '".$cuatro_dijitos."',
  $sucursal,
  '".$afiliacion."',
  '".$autorizacion."',
  '".$clave_rastreo."',
  '".$referencia."',
  '".$rutaCompleta."',
  '".$fecha_registro."')";

if ($conn->query($sql) === TRUE){

}else{
  $erroQueryNuevo=1;
}

}else if(isset($_POST['btnAlumnoInscrito'])){


  $tipoAlumno    = 2;
  $hoy              = date("d-m-Y");
  $directorioRemoto = "img-upload/$hoy/";

  $nombre      = limpiar($_POST['nombre']);
  $apaterno    = limpiar($_POST['apaterno']);
  $amaterno    = limpiar($_POST['amaterno']);
  $correo      = limpiar($_POST['correo']);
  $celular     = limpiar($_POST['celular']);
  


  $metodo_pago   = limpiar($_POST['metodo_pago']);
  $cuatro_dijitos = limpiar($_POST['cuatro_dijitos']);
  $sucursal       = limpiar($_POST['sucursal']);

  if($metodo_pago=="deposito"){
    $afiliacion    = limpiar($_POST['afiliacion']);
    $autorizacion  = limpiar($_POST['autorizacion']);
    $clave_rastreo = "";
    $referencia    = "";
  }
      
  if($metodo_pago=="transferencia"){
    $afiliacion    = "";
    $autorizacion  = "";
    $clave_rastreo = limpiar($_POST['clave_rastreo']);
    $referencia    = limpiar($_POST['referencia']);
  }

  function aleatorio($length=5){
    $source="ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
    if($length>0){			
      $rstr = "";
      $source = str_split($source,1);
      for($i=1; $i<=$length; $i++){
        mt_srand((double)microtime() * 1000000);
        $num = mt_rand(1,count($source));
        $rstr .= $source[$num-1];				
      }
    }
    return $rstr;
  }
  $r = aleatorio();



  if (!file_exists(dirname(__FILE__)."/".$directorioRemoto)) {
    if (!mkdir(dirname(__FILE__)."/".$directorioRemoto, 0777)){
      $errorCrearDirInscrito= 1;
    }
  }
  
  $nombreComprobante  = $_FILES['baucher']['name'];
  //$extensionComprobante      = pathinfo($nombreComprobante, PATHINFO_EXTENSION);
  //$rutaCompleta  = $directorioRemoto.basename($nombreComprobante);
  $rutaCompleta  = $directorioRemoto.basename($r.'_'.$nombreComprobante);
  //echo "ruta completa de la imagena  cargar: ".$rutaCompleta."<br/>";
  /*if (move_uploaded_file($_FILES['baucher']['tmp_name'], $rutaCompleta)) {

  }else{
    $errorMoverVaucheInscrito= 1;
  }*/
  function compressImage($source, $destination, $quality) { 
    $imgInfo = getimagesize($source); 
    $mime = $imgInfo['mime']; 
    switch($mime){ 
        case 'image/jpeg': 
            $image = imagecreatefromjpeg($source); 
            break; 
        case 'image/png': 
            $image = imagecreatefrompng($source); 
            break; 
        case 'image/gif': 
            $image = imagecreatefromgif($source); 
            break; 
        default: 
            $image = imagecreatefromjpeg($source); 
    } 
     
    imagejpeg($image, $destination, $quality); 
    return $destination; 
} 
 
if(!compressImage($_FILES['baucher']['tmp_name'], $rutaCompleta, 50)){
  $errorMoverVaucheInscrito= 1;
}


  $sql = "INSERT INTO inscripciones_web (
    tipoAlumno,
    nombre,
    apaterno,
    amaterno,
    celular,
    correo,
    metodo_pago,
    cuatro_digitos,
    sucursal,
    afiliacion,
    autorizacion,
    clave_rastreo,
    referencia,
    ruta_imagen,
    fecha_creacion) VALUES (
      $tipoAlumno,
    '".$nombre."',
    '".$apaterno."',
    '".$amaterno."',
    '".$celular."',
    '".$correo."',
    '".$metodo_pago."',
    '".$cuatro_dijitos."',
    '".$sucursal."',
    '".$afiliacion."',
    '".$autorizacion."',
    '".$clave_rastreo."',
    '".$referencia."',
    '".$rutaCompleta."',
    '".$fecha_registro."')";
  




  if ($conn->query($sql) === TRUE){

  }else{
    $erroQueryInscrito=1;

  }


}else{
  // Redireccionar a pagina principal de INBI.
  header("Location: https://fastenglish.com.mx/");
}

?>

<!doctype html>
<html lang="en">
  <head>

    <meta http-equiv=”Content-Type” content=”text/html; charset=UTF-8″ />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/customer.css">
    <script type="text/script" src="js/bootstrap.min.js"></script>
    <title>Comprobante de pago</title>

  </head>
  <body>
    <div class="container"> <!--Inicia Container -->
      <div class="row">
        <div class="col-md-4 text-center"><a href="http://fastenglish.com.mx"><img src="logo/cropped-logo-fast-png-1.png" width="155" height="131"></a></div>
        <div class="col-md-4 text-center"><h4>Inscripciones</h4></div>
        <div class="col-md-4"></div>
      </div>

      <div class="row"> 
        <div class="col-md-4"></div>
        <div class="col-md-4 text-center">
        <?php
        if(isset($errorCrearDirInscrito) || isset($errorMoverVaucheInscrito) || isset($erroQueryInscrito) || isset($errorCrearDirNuevo) || isset($errorMoverVaucherNuevo) || isset($erroQueryNuevo)){
          echo "Se produjo un error al intentar enviar su información, por favor intentelo de nuevo.<br/>";
        }else{
          ?>
          Su datos se enviaron correctamente, el area administrativa revisara y se le confirmara por correo la informacion de su comprobante de pago.
          En unos segundos sera re direccionado a la pagina de inicio de FastEnglish.
          <?php
        }

        ?>
          <script>
        setTimeout(function(){ window.location.href = "https://www.fastenglish.com.mx"; }, 9000);
          </script>
        </div>
        <div class="col-md-4"></div>
      </div>
    </div>
