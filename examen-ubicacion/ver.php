<?php
require 'inc/conexion.php';
date_default_timezone_set('America/Monterrey');
?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script type="text/script" src="js/bootstrap.min.js"></script>    
    <link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-96x96.png">

    <style>
    #respuestaAjax {
      display:none;
    }

    #retornaError {
      display:none;
    }
    </style>
    <title>Plataforma de evaluaciones Fastenglish</title>
  </head>
  <body>

  <?php
 
    if (isset($_GET['a'])) {
      $a  = limpiar($_GET['a']);
    } else {
      $a      ='';
    }

    if(!empty($a)){
      $sql = "SELECT * FROM examen_ubicacion where id='".$a."'";
    }
  
    if (!$resultado = $conn->query($sql)) {
      echo "Lo sentimos, este sitio web está experimentando problemas.";
      echo "Error: La ejecución de la consulta falló debido a: \n";
      echo "Query: " . $sql . "\n";
      echo "Errno: " . $conn->errno . "\n";
      echo "Error: " . $conn->error . "\n";
      exit;
    }

    if ($resultado->num_rows === 0) {
      echo "No se encontraron registros. Inténtelo de nuevo.";
      exit;
    }
    $data = $resultado->fetch_assoc();
  ?>

  <div class="container"> <!--Inicia Container -->
    <div class="row">
      <div class="col-md-4">
        
      </div>
    </div>
  </div><!--Termima container-->

  <header>
     <h1 class="text-center text-light">Examenes</h1>
     <h2 class="text-center text-light"> <span class="badge badge-primary">Examen de ubicacion <?php echo $data['nombre'];?></span></h2> 
  </header>    

  <div style="height:50px"></div>
     
  <!--Ejemplo tabla con DataTables-->
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="table-responsive">        
          <table id="example" class="table table-striped table-bordered" style="width:100%">
            <tbody>
                                                 
              <tr id="arriba"><td><span class="font-weight-bold">Nombre</span></td><td><?php echo $data['nombre'];?></td></tr>
              <tr><td><span class="font-weight-bold">Correo</span></td><td><?php echo $data['correo'];?></td></tr>
              <tr><td><span class="font-weight-bold">Teléfono</span></td><td><?php echo $data['telefono'];?></td></tr>
              <tr><td><span class="font-weight-bold">Motivo de aprender</span></td><td><?php echo $data['motivo_aprender'];?></td></tr>
              <?php
                  $suc = $data['sucursal'];
                  $sqlSucursal = "select * from sucursales where id=$suc";

                  $resSuc   = $conn->query($sqlSucursal);
                  $arraySuc = $resSuc->fetch_assoc();
                  $nombreSucursal = $arraySuc['nombre'];
              ?>

              <tr><td><span class="font-weight-bold">Sucursal</span></td><td><?php echo $nombreSucursal;?></td></tr>
              <tr><td><span class="font-weight-bold">Horario</span></td><td><?php echo $data['horario'];?></td></tr>
              <tr><td><span class="font-weight-bold">Fecha Registro</span></td><td><?php echo $data['fecha_registro'];?></td></tr>
              <tr><td><span class="font-weight-bold">Origen de registro</span></td><td><?php echo $data['origen'];?></td></tr>
              
              <form action="actualizar-nivel.php" method="post">
                <tr><td><span class="font-weight-bold">Nivel</span></td><td>
                  <div style="width:30%">
                  <?php
                  if(!empty($data['nivel'])){
                    $ssN= "select id,nombre from niveles_evaluacion";
                    $rnn   = $conn->query($ssN);
                    ?>
                    <select class="form-control" name="nivel_asignado" id="nivel_asignado" required>
                      <option value="">Seleccione una opción</option>
                      <?php
                      while($nivel = $rnn->fetch_assoc()){
                        if($nivel['id']==$data['nivel']){
                        ?>
                        <option value="<?=$nivel['id']?>" selected><?=$nivel['nombre']?></option>
                        <?php
                        }else{
                          ?>
                          <option value="<?=$nivel['id']?>"><?=$nivel['nombre']?></option>
                          <?php
                        }  
                      }
                      ?>
                    </select>
                    <?php
                  }else{
                    echo "Sin asignar";
                  }
                  ?>
                  </div>
                  </td>
                </tr>

                <tr>
                <td><input type="hidden" name="idAlumno" id="idAlumno" value="<?=$a;?>"></td>
                <td><div style="width:30%"><button type="submit" class="btn btn-primary btn-sm" name="btnEditarNivel">Actualizar Nivel</button></div></td>
                </tr>
              </form>

              <tr><td colspan="2"><h2>Respuesta de las preguntas del examen de ubicación.</h2></td></tr>
              <tr><td colspan="2"><h3>Inicisos del 1 al 17: <?php echo $data['cant'];?></h3></td></tr>

              <tr><td colspan="2"><h3>Parte 3: Descripción de imagenes.</h3></td></tr>
              <tr><td colspan="2"><img class="img-fluid" src="imagenes/Diapositiva15.PNG" width="444" height="283" alt="examen de ingles"/></td></tr>
              <tr><td>Respuesta: </td><td><?php echo $data['respuesta_imagen_1'];?></td></tr>
              
              <tr><td colspan="2"><h3>Parte 4: Expresar vivencias.</h3></td></tr>
              <tr><td colspan="2"><img class="img-fluid" src="imagenes/Diapositiva18.PNG" width="444" height="283" alt="examen de ingles"/></td></tr>
              <tr><td>Respuesta: </td><td><?php echo $data['respuesta_imagen_3'];?></td></tr>
            </tbody>        
          </table>                  
        </div>
      </div>
    </div>  
  </div>
  <br/>  

<!-- jQuery first, then Popper.js, then Bootstrap JS -->
    
<script src="js/jquery/jquery-3.3.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>   

</body>
</html>