
<?php
use  PHPMailer\PHPMailer;
use  PHPMailer\Exception;

require 'inc/conexion.php';
date_default_timezone_set('America/Monterrey');
require 'vendor/autoload.php';
require 'vendor/phpmailer/src/Exception.php';
require 'vendor/phpmailer/src/PHPMailer.php';
require 'vendor/phpmailer/src/SMTP.php';
?>
<?php
  if(isset($_POST['btnRegistrar'])){
    $nivel    = $_POST['asig-nivel'];
    $id       = $_POST['id'];
    $unico    = $_POST['unico'];
    $correo   = $_POST['correo'];
    $sucursal = $_POST['sucursal'];

    $respuestaOnce = $_POST['respuestaOnce'];
    $opcionRespuestaOnce = $respuestaOnce=="nok" ? $_POST['opcionRespuestaOnce'] : 'null';

  }
  // Asignacion de nivel
  $sqlUpdate = "UPDATE examen_ubicacion SET nivel='".$nivel."' WHERE id=".$id." AND uniq='".$unico."' and correo = '".$correo."' and sucursal ='".$sucursal."'";
  $conn->query($sqlUpdate);

  $sqlInsert = "INSERT INTO clasificacion_respuestas (id_examen_ubicacion,uniq,correo,sucursal,valor_11,opcion_11) VALUES (
    $id,
    '".$unico."',
    '".$correo."',
    $sucursal,
    '".$respuestaOnce."',
    $opcionRespuestaOnce)";
  
  if ($conn->query($sqlInsert) === TRUE) {
    //echo "Se creo el registro de manera exitosa.<br/>";
    // Re-direccionar al panel
    ?>
    <script>
      window.open("https://fastenglish.com.mx/english-level-test/diploma.php?id=<?php echo $id ?>");
    </script>
    <?php

    $sql = "SELECT * FROM examen_ubicacion where id=$id";
    if (!$resultado = $conn->query($sql)) {
      echo $sql;
    }
    $data = $resultado->fetch_assoc();

  //   $to      = 'fastenglishventas@gmail.com,fastenglishventas1@gmail.com,armando.molina@inbi.mx,fastenglishventas2@gmail.com';
  //   $subject = 'PDF Resultado del examen de ubicacion fast';
  //   $headers = "From: revision.ceaa@fastenglish.com.mx\r\n";
  //   $headers .= "MIME-Version: 1.0\r\n";
  //   $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

  //   $message = "
	 // <b>nombre: ".$data['nombre']."</b>
  //     <b><h3>Diploma: https://fastenglish.com.mx/english-level-test/diploma.php?id=".$id."</h3></b>
  //   ";


  //   mail($to, $subject, $message, $headers);

    $nombre = $data['nombre'];
    $mail = new PHPMailer(true);

    // $mail = \Config\Services::email();
    // SMTP Parametros.
    $mail->isSMTP();
    // SMTP Server Host. 
    $mail->Host = 'smtp.ionos.mx';
    // Use SMTP autentificacion. 
    $mail->SMTPAuth = true;
    // Introduce la incriptacion del sistema. 
    $mail->SMTPSecure = 'tls';
    // SMTP usuario. 
    $mail->Username = 'web-master@inbi.mx';
    // SMTP contraseña. 
    $mail->Password = '3s6!4zgJ';
    // Introduce el SMTP Puerto.
    $mail->Port = 587;

    // Activo condificacción utf-8
    $mail->CharSet = 'UTF-8';


    // Añade remitente.
    $mail->setFrom('web-master@inbi.mx', 'Fast English');
    $mail->addAddress('fastenglishventas@gmail.com', 'Fast English');
    $mail->addAddress('armando.molina@inbi.mx', 'Fast English');
    $mail->addAddress('fastenglishventas2@gmail.com', 'Fast English');
    $mail->addAddress('fastenglishventas1@gmail.com', 'Fast English');
    $mail->addAddress('eduardo.zav.dev@gmail.com', 'Fast English');
    $mail->Subject = 'PDF Resultado del examen de ubicacion fast';
    $mail->isHTML(TRUE);
    $mail->Body =  "
      <!doctype html>
      <html lang=\"en\">
        <head>
          <title>Registro de examen de ubicación.</title>
      <style>
        body {
          margin: 0;
          padding: 0;
        }
        h1 {
          font-family: Arial, Helvetica, sans-serif;
          font-size: 110%;
          font-weight: bold;
          color: #2E86C1;
        }

        h2 {
          font-family: Arial, Helvetica, sans-serif;
          font-size: 100%;
          font-weight: bold;
          color: #2E86C1 ;
        }
      p{
        color: #666;
        font-family: Arial, Helvetica, sans-serif;
      }
        .center {
          position: relative;
          margin: auto;
          padding: auto;
          text-align: center;
        }
        .contenedor {
          width: 80%;
          height: auto;
          padding: 0;
          margin: auto;
          background-color: #fff;
        }
        .centro {
          width: 200px;
          height: 30px;
          vertical-align: middle;
        }
        .asado:active {
          border: none;
        }
        .linea {
          width: 100%;
      height: 1px;
      border: 1px solid #cdcdcd;
        }
        .go {
          background-color:#FF5733;
          color:#fff;
          border:none;
          padding:10px 15px 10px 15px;
          font-family: Arial, Helvetica, sans-serif;
        }
        .dato-title {
          font-size: 100%;
          font-weight: bold;
          font-family: Arial, Helvetica, sans-serif;
        }
        .dato-description {
          font-size: 100%;
          font-weight: normal;
          font-family: Arial, Helvetica, sans-serif;
        }

      </style>


      </head>
      <body>
        <div class=\"contenedor\">
          <h1>PDF</h1>

          <p><h1><b>".$nombre."</h1></b></p>
          <br/>

          <a href='https://fastenglish.com.mx/english-level-test/diploma.php?id=$id'>
            <button class='go'>Descargar</button>
          </a>

        </div>
      </body>
      </html>
    ";
    $mail->send();
  }else{
    echo "No se puedo insertar el registro. <br/><br/><br/>";
    echo $sqlInsert."<br/><br/>";
  }
 
?>
