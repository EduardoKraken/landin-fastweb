<?php
  require 'inc/conexion.php';
  if(isset($_GET['id'])){
    $id=$_GET['id'];
    $sql = "SELECT * FROM examen_ubicacion where id=$id";

    if (!$resultado = $conn->query($sql)) {
    echo $sql;
    }
    $data = $resultado->fetch_assoc();


    $sqlAnalizado = "select * from clasificacion_respuestas where id_examen_ubicacion = $id";
    $resA = $conn->query($sqlAnalizado);
    $arreglo= $resA->fetch_assoc();

    $sqlSucursal = "select nombre from sucursales where id=".$data['sucursal'];
    $resSucursal = $conn->query($sqlSucursal);
    $ds = $resSucursal->fetch_assoc();
    $nombreSucursal = $ds['nombre'];

    $valor_11  = $arreglo['valor_11'];
    $opcion_11 = $arreglo['opcion_11'];
    if($valor_11=="nok"){
      $sqlMensaje = "SELECT descripcion FROM opciones_respuesta_preguntas_incorrectas where id=$opcion_11";
      $resM = $conn->query($sqlMensaje);
      $m= $resM->fetch_assoc();
      $mensajeOnce  = $m['descripcion'];
    }
  }

  // Obtener el nombre del nivel(solo impirmir Nivel No, no poner los incisos) y la descripcion.
  $ssN= "select nombre,descripcion from niveles_evaluacion where id=".$data['nivel'];
  $rnn   = $conn->query($ssN);
  $nivel = $rnn->fetch_assoc();
  $nombreNivel =$nivel['nombre'];
  $descripcion =$nivel['descripcion'];

  $nNivel = explode(" ", $nombreNivel);
  $nombre_nivel =$nNivel[0]." ".$nNivel[1];  
  // Optionally define the filesystem path to your system fonts
  // otherwise tFPDF will use [path to tFPDF]/font/unifont/ directory
  define("_SYSTEM_TTFONTS", "C:/Windows/Fonts/");
  require('tfpdf/tfpdf.php');
  $pdf = new tFPDF('P','mm','A4');
  $pdf->AddPage();
  // Add a Unicode font (uses UTF-8)
  $pdf->Image('diploma_fast.jpg' , 0 ,0, 210 , 297,'JPG');

  // Load a UTF-8 string from a file and print it
  $pdf->AddFont('arial','','arial.ttf',true);
  $pdf->AddFont('arialbd','','arialbd.ttf',true);

  /*$pdf->AddFont('minionpromed','','MinionProMedium.ttf',true);
  $pdf->SetFont('minionpromed','',36);
  $pdf->Ln(20);
  $pdf->SetXY(40,40);
  */

  /*Este es el contenido del DIPLOMA */ 

  $pdf->SetFont('arial','',36);
  $pdf->SetXY(15,70);
  $pdf->Write(8, "Fast English hace constar que:");
  $pdf->Ln(20);
  $pdf->SetFont('arial','',26);
  $pdf->SetXY(20,100);
  //$pdf->Write(8, );
  $pdf->MultiCell(0,8, $data['nombre'] , $border=0, $align="C", $fill=false);

  $pdf->Ln(20);
  $pdf->SetFont('arial','',20);
  $pdf->SetXY(10,130);
  $textoFijo="Ha realizado nuestro examen para comprobar sus habilidades en el idioma inglés “Placement test” concluyendo lo que se describirá a continuación.";
  $pdf->MultiCell(0,8, $textoFijo , $border=0, $align="C", $fill=false);

  $pdf->SetFont('arial','',12);
  $pdf->Ln(10);
  $pdf->SetXY(10,170);
  $pdf->MultiCell(0,8, $descripcion , $border=0, $align="C", $fill=false);
  $pdf->Ln(15);



  $pdf->AddPage();
  // Add a Unicode font (uses UTF-8)
  $pdf->Image('diploma_fast.jpg' , 0 ,0, 210 , 297,'JPG');

/***** Termina pregunta de audio ***************************/
/***********************************************************/
/****** Imagenes *******/


  $pdf->SetXY(10,80);
  $pdf->SetFont('arialbd','',30);
  $textoFijo="Etapa 4: Escritura";
  $pdf->MultiCell(0,8, $textoFijo , $border=0, $align="C", $fill=false);


  $pdf->SetXY(10,100);
  $pdf->SetFont('arialbd','',12);
  $pdf->Write(7,"Usted escribió");
  $pdf->SetXY(10,105);
  $pdf->SetFont('arial','',10);
  $pdf->Write(8," Respuesta: " .$data['respuesta_imagen_1']);
  if($valor_11=="nok"){
    $pdf->SetXY(10,150);
    $pdf->Write(6,"$mensajeOnce");
    $pdf->Ln(15);
  }else{
    $pdf->SetXY(10,150);
    $pdf->Write(6,"Correcta");
    $pdf->Ln(15);  
  }

  $pdf->SetXY(10,190);
  $pdf->SetFont('arialbd','',30);
  $textoFijo="Datos personales";
  $pdf->MultiCell(0,8, $textoFijo , $border=0, $align="C", $fill=false);
  $pdf->Ln(15);

  $pdf->SetXY(10,200);
  $pdf->SetFont('arialbd','',11);
  $pdf->Write(7,"Teléfono: ".$data['telefono']);
  $pdf->Ln(15);

  $pdf->SetXY(10,206);
  $pdf->SetFont('arialbd','',11);
  $pdf->Write(7,"Motivo: ".$data['motivo_aprender']);
  $pdf->Ln(15);

  $pdf->SetXY(10,212);
  $pdf->SetFont('arialbd','',11);
  $pdf->Write(7,"Horario: ".$data['horario']);
  $pdf->Ln(15);

  $pdf->SetXY(10,218);
  $pdf->SetFont('arialbd','',11);
  $pdf->Write(7,"sucursal: ".$nombreSucursal);
  $pdf->Ln(15);

  


  $pdf->Output();

  $to      = 'fastenglishventas@gmail.com,fastenglishventas1@gmail.com,armando.molina@inbi.mx,eduardo.zav.dev@gmail.com';
  $subject = 'Registro de examen de ubicación Fastenglish';


  $headers = "From: " . $correo. "\r\n";
  $headers .= "MIME-Version: 1.0\r\n";
  $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

  $message = "
    <b><h2>Nombre: ".$data['nombre']."</h2></b>
    <b><h3>Nivel: ".$nombreNivel."</h3></b>
    <b><h3>Teléfono: ".$data['telefono']."</h3></b>
    <b><h3>Motivo: ".$data['motivo_aprender']."</h3></b>
    <b><h3>Horario: ".$data['horario']."</h3></b>
    <b><h3>Diploma: https://fastenglish.com.mx/english-level-test/diploma.php?id=".$id."</h3></b>
  ";

  mail($to, $subject, $message, $headers);

?>