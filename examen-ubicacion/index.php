<!-- Black List: Buscar ip en una tabla de Black List -->
<!-- Generación de token y ID de session -->
<!-- Insertar la id y la session en la base de datos-->

<!-- Checar como esta el examen de ubicación de Grop Up -->
<!--Poner Captcha para evitar Span -->
<!--Ingresar estos datos en la base de datos -->

<!--Poder calcular el tiempo de el examen -->
<?php
  date_default_timezone_set('America/Monterrey');
  require 'inc/conexion.php';
?>
<!doctype html>
<html lang="en">
  <head>
    <!--  meta tags -->
    <meta http-equiv=”Content-Type” content=”text/html; charset=UTF-8″ />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script type="text/script" src="js/bootstrap.min.js"></script>
    <!-- <link  rel="icon"   href="favicon/favicon-32x32.png" type="image/png" />-->
    <link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-96x96.png">

    <title>Examen de ubicación Fastenglish.</title>
    <style>
      body {
        border-top:4px solid #074D80;
        padding-top:20px;
        background: #EDEDED;
      }

      .asado {
        width: 200px;
        height: 30px;
        vertical-align: middle;
      }
      .asado:active {
        border: none;
      }
    </style>
  </head>
  <body>
    <?php
    if(!isset($_POST['entrar'])){
    ?>
    <div class="container"> <!--Inicia Container -->
      <br/><br/>
      <div class="text-center"><h1 style="font-size: 52px;"><b>English level test</b></h1> </div>
      <br/><br/>
      <div class="row">
        <div class="col-md-12">
          <div class="text-center">
            <img class="img-fluid" src="img/cropped-logo-fast-png-1.png" width="155" height="161" Alt="">
          </div>
        </div>
      </div>

      <!--Inicia el formulario -->
      <div class="row">
        <div class="col-md-2">
        </div>

        <div class="col-md-8"><br/><br/><br/>
          <span class="font-weight-bold" style="font-size: 24px;">
            Instrucciones: 
          </span> / 
          <audio class="asado" src="audios/instrucciones_generales.mp3" controls>
            <p>Tu navegador no implementa el elemento audio</p>
          </audio>
          <br/>
          <br/>
          <img class="img-fluid" src="imagenes/1.JPG" width="730" height="490" Alt="">
            <form action="" method="POST" onsubmit="return evitarDuplicidadEnvio();" name="forma" id="forma">
              <br/>
              <br/>
              <div class="form-group">
                <label for="nombre"><b>Nombre</b></label>
                <input type="text" class="form-control" name="nombre" id="nombre" required>
              </div>

              <div class="form-group">
                <label for="correo"><b>Correo</b></label>
                <input type="email" class="form-control" name="correo" id="correo" required>
              </div>

              <div class="form-group">
                <label for="telefono"><b>Teléfono</b></label>
                <input type="text" class="form-control" name="telefono" id="telefono" required>
              </div>

              <div class="form-group">
                <label for="exampleFormControlSelect1"><b>¿Por qué quieres aprender Inglés?</b></label>
                <select class="form-control" name="motivo_aprender" id="motivo_aprender" >
                  <option value="">Seleccione una opción</option>
                  <option value="academico">Académico</option>
                  <option value="trabajo">Trabajo</option>
                  <option value="ocio">Ocio</option>
                </select>
              </div>
              <?php
                if ($conn->connect_errno) { 
                  // No se debe revelar información delicada
                  echo "Lo sentimos, este sitio web está experimentando problemas.";
                  echo "Error: Fallo al conectarse a MySQL debido a: \n";
                  echo "Errno: " . $conn->connect_errno . "\n";
                  echo "Error: " . $conn->connect_error . "\n";    
                  exit;
                }

                  $sql = "SELECT * FROM sucursales";
                  if (!$resultado = $conn->query($sql)) {
                    echo "Error al buscar las su ursales";
                    exit;
                  }
                  $conn->close();  
              ?>

              <div class="form-group">
                <label for="exampleFormControlSelect1"><b>Elige la sucursal más cercana a ti (en caso de no estar en el area metropolitana de Nuevo León, selecciona online)</b></label>
                <select class="form-control" name="sucursal" id="sucursal" >
                  <option value="">Seleccione una opción</option>
                  <?php
                    while($data = $resultado->fetch_assoc()){
                  ?>
                  <option value="<?php echo $data['id'];?>" required><?php echo $data['nombre'];?></option>
                  <?php
                    }
                  ?>
                </select>
              </div>

              <div class="form-group">
                <label for="exampleFormControlSelect1"><b>¿En qué horario te gustaría aprender inglés?</b></label>
                <select class="form-control" name="horario" id="horario" required>
                  <option value="">Seleccione una opción</option>
                  <option value="manana">Mañana</option>
                  <option value="tarde">Tarde</option>
                  <option value="sabatino">Sabatino</option>
                  <option value="dominical">Dominical</option>
                </select>
              </div>

              <div class="form-group">
                <label for="exampleFormControlSelect1"><b>¿Cómo consideras tu nivel de inglés?</b></label>
                <select class="form-control" name="nivel_ingles" id="nivel_ingles" required>
                  <option value="">Seleccione una opción</option>
                  <option value="bajo">Bajo</option>
                  <option value="medio">Medio</option>
                  <option value="alto">Alto</option>
                </select>
              </div>

              <div class="form-group">
                <label for="exampleFormControlSelect1"><b>¿Has estudiado inglés en una escuela anteriormente?</b></label>
                <select class="form-control" name="estudios" id="estudios" required>
                  <option value="">Seleccione una opción</option>
                  <option value="si">Si</option>
                  <option value="no">No</option>
                </select>
              </div>

              <br/>


              <h3>Etapa 1</h3> 
              <span class="font-weight-bold">
                Instrucciones Parte 1: 
              </span> /  
              <audio class="asado" src="audios/etapa_1_instrucciones.mp3" controls>
                <p>Tu navegador no implementa el elemento audio</p>
              </audio>
              <div class="form-group"></div>
              <br/>

              <div class="form-group">
                <label for="respuesta-ingles-1">
                  <span class="font-weight-bold">
                    QUESTION 1.- 
                  </span>
                    <audio class="asado" src="audios/pregunta_de_audio_1.mp3" controls>
                      <p>Tú navegador no implementa el elemento de audio</p>
                    </audio>
                </label>
                <label for="respuesta-ingles-imagen1">
                  <img class="img-fluid" src="imagenes/2.JPG" width="644" height="483" alt="examen de ingles"/>
                </label>
                <br/>
                <input type= "radio" name="parte1p1" id="memoria1" value="0" required><b> a) I am fine</b><br/>
                <input type= "radio" name="parte1p1" id="memoria2" value="1" required><b> b) I am 17 years old</b><br/>
                <input type= "radio" name="parte1p1" id="memoria3" value="2" required><b> c) I have 17 years</b><br/>
                <input type= "radio" name="parte1p1" id="memoria4" value="3" required><b> d) No se cómo se debe contestar</b>
              </div>
              <br/><br/>

              <div class="form-group">
                <label for="respuesta-ingles-1">
                  <span class="font-weight-bold">
                    QUESTION 2.- 
                  </span>
                    <audio class="asado" src="audios/pregunta_de_audio_2.mp3" controls>
                      <p>Tú navegador no implementa el elemento de audio</p>
                    </audio>
                </label>
                <label for="respuesta-ingles-imagen1">
                  <img class="img-fluid" src="imagenes/3.JPG" width="644" height="483" alt="examen de ingles"/>
                </label>
                <br/>
                <input type= "radio" name="parte1p2" id="memoria1" value="0" required><b> a) She is from Georgia</b><br/>
                <input type= "radio" name="parte1p2" id="memoria2" value="1" required><b> b) He is from Georgia</b><br/>
                <input type= "radio" name="parte1p2" id="memoria3" value="2" required><b> c) I am from Georgia</b><br/>
                <input type= "radio" name="parte1p2" id="memoria4" value="3" required><b> d) No se cómo se debe contestar</b>
              </div>
              <br/><br/>

              <div class="form-group">
                <label for="respuesta-ingles-1">
                  <span class="font-weight-bold">
                    QUESTION 3.- 
                  </span>
                    <audio class="asado" src="audios/pregunta_de_audio_3.mp3" controls>
                      <p>Tú navegador no implementa el elemento de audio</p>
                    </audio>
                </label>
                <label for="respuesta-ingles-imagen1">
                  <img class="img-fluid" src="imagenes/4.JPG" width="644" height="483" alt="examen de ingles"/>
                </label>
                <br/>
                <input type= "radio" name="parte1p3" id="memoria1" value="0" required><b> a) She works in a company</b><br/>
                <input type= "radio" name="parte1p3" id="memoria2" value="1" required><b> b) She work everyday</b><br/>
                <input type= "radio" name="parte1p3" id="memoria3" value="2" required><b> c) She working in a factory</b><br/>
                <input type= "radio" name="parte1p3" id="memoria4" value="3" required><b> d) No se cómo se debe contestar</b>
              </div>
              <br/><br/>

              <div class="form-group">
                <label for="respuesta-ingles-1">
                  <span class="font-weight-bold">
                    QUESTION 4.- 
                  </span>
                    <audio class="asado" src="audios/pregunta_de_audio_4.mp3" controls>
                      <p>Tú navegador no implementa el elemento de audio</p>
                    </audio>
                </label>
                <label for="respuesta-ingles-imagen1">
                  <img class="img-fluid" src="imagenes/5.JPG" width="644" height="483" alt="examen de ingles"/>
                </label>
                <br/>
                <input type= "radio" name="parte1p4" id="memoria1" value="0" required><b> a) I ate some rice and chicken</b><br/>
                <input type= "radio" name="parte1p4" id="memoria2" value="1" required><b> b) I eat some rice and chicken</b><br/>
                <input type= "radio" name="parte1p4" id="memoria3" value="2" required><b> c) I was eat some rice and chicken</b><br/>
                <input type= "radio" name="parte1p4" id="memoria4" value="3" required><b> d) No se cómo se debe contestar</b>
              </div>
              <br/><br/>

              <div class="form-group">
                <label for="respuesta-ingles-1">
                  <span class="font-weight-bold">
                    QUESTION 5.- 
                  </span>
                    <audio class="asado" src="audios/pregunta_de_audio_5.mp3" controls>
                      <p>Tú navegador no implementa el elemento de audio</p>
                    </audio>
                </label>
                <label for="respuesta-ingles-imagen1">
                  <img class="img-fluid" src="imagenes/6.JPG" width="644" height="483" alt="examen de ingles"/>
                </label>
                <br/>
                <input type= "radio" name="parte1p5" id="memoria1" value="0" required><b> a) Yes, we are very nice friends</b><br/>
                <input type= "radio" name="parte1p5" id="memoria2" value="1" required><b> b) Yes, we have a lot of money</b><br/>
                <input type= "radio" name="parte1p5" id="memoria3" value="2" required><b> c) Yes, we study English</b><br/>
                <input type= "radio" name="parte1p5" id="memoria4" value="3" required><b> d) No se cómo se debe contestar</b>
              </div>
              <br/><br/>


              <!-- Etapa 2 -->
              <h3>Etapa 2</h3> 
              <span class="font-weight-bold">
                Instrucciones Parte 2: 
              </span> /  
              <audio class="asado" src="audios/etapa_2_instrucciones.mp3" controls>
                <p>Tu navegador no implementa el elemento audio</p>
              </audio>
              <div class="form-group"></div>
              <br/>

              <div class="form-group">
                <label for="respuesta-ingles-1">
                  <span class="font-weight-bold">
                    QUESTION 6.- 
                  </span>
                </label>
                <label for="respuesta-ingles-imagen1">
                  <img class="img-fluid" src="imagenes/7.JPG" width="644" height="483" alt="examen de ingles"/>
                </label>
                <br/>
                <input type= "radio" name="parte2p1" id="memoria1" value="0" required><b> a) spend</b><br/>
                <input type= "radio" name="parte2p1" id="memoria2" value="1" required><b> b) spent</b><br/>
                <input type= "radio" name="parte2p1" id="memoria3" value="2" required><b> c) spending</b><br/>
                <input type= "radio" name="parte2p1" id="memoria4" value="3" required><b> d) No se cómo se debe contestar</b>
              </div>
              <br/><br/>

              <div class="form-group">
                <label for="respuesta-ingles-1">
                  <span class="font-weight-bold">
                    QUESTION 7.- 
                  </span>
                </label>
                <label for="respuesta-ingles-imagen1">
                  <img class="img-fluid" src="imagenes/8.JPG" width="644" height="483" alt="examen de ingles"/>
                </label>
                <br/>
                <input type= "radio" name="parte2p2" id="memoria1" value="0" required><b> a) I</b><br/>
                <input type= "radio" name="parte2p2" id="memoria2" value="1" required><b> b) my</b><br/>
                <input type= "radio" name="parte2p2" id="memoria3" value="2" required><b> c) me</b><br/>
                <input type= "radio" name="parte2p2" id="memoria4" value="3" required><b> d) No se cómo se debe contestar</b>
              </div>
              <br/><br/>

              <div class="form-group">
                <label for="respuesta-ingles-1">
                  <span class="font-weight-bold">
                    QUESTION 8.- 
                  </span>
                </label>
                <label for="respuesta-ingles-imagen1">
                  <img class="img-fluid" src="imagenes/9.JPG" width="644" height="483" alt="examen de ingles"/>
                </label>
                <br/>
                <input type= "radio" name="parte2p3" id="memoria1" value="0" required><b> a) her</b><br/>
                <input type= "radio" name="parte2p3" id="memoria2" value="1" required><b> b) she</b><br/>
                <input type= "radio" name="parte2p3" id="memoria3" value="2" required><b> c) hers</b><br/>
                <input type= "radio" name="parte2p3" id="memoria4" value="3" required><b> d) No se cómo se debe contestar</b>
              </div>
              <br/><br/>

              <div class="form-group">
                <label for="respuesta-ingles-1">
                  <span class="font-weight-bold">
                    QUESTION 9.- 
                  </span>
                </label>
                <label for="respuesta-ingles-imagen1">
                  <img class="img-fluid" src="imagenes/10.JPG" width="644" height="483" alt="examen de ingles"/>
                </label>
                <br/>
                <input type= "radio" name="parte2p4" id="memoria1" value="0" required><b> a) do</b><br/>
                <input type= "radio" name="parte2p4" id="memoria2" value="1" required><b> b) does</b><br/>
                <input type= "radio" name="parte2p4" id="memoria3" value="2" required><b> c) is</b><br/>
                <input type= "radio" name="parte2p4" id="memoria4" value="3" required><b> d) No se cómo se debe contestar</b>
              </div>
              <br/><br/>

              <div class="form-group">
                <label for="respuesta-ingles-1">
                  <span class="font-weight-bold">
                    QUESTION 10.- 
                  </span>
                </label>
                <label for="respuesta-ingles-imagen1">
                  <img class="img-fluid" src="imagenes/11.JPG" width="644" height="483" alt="examen de ingles"/>
                </label>
                <br/>
                <input type= "radio" name="parte2p5" id="memoria1" value="0" required><b> a) The taller</b><br/>
                <input type= "radio" name="parte2p5" id="memoria2" value="1" required><b> b) The tallest</b><br/>
                <input type= "radio" name="parte2p5" id="memoria3" value="2" required><b> c) The most tall</b><br/>
                <input type= "radio" name="parte2p5" id="memoria4" value="3" required><b> d) No se cómo se debe contestar</b>
              </div>
              <br/><br/>

              <div class="form-group">
                <label for="respuesta-ingles-1">
                  <span class="font-weight-bold">
                    QUESTION 11.- 
                  </span>
                </label>
                <label for="respuesta-ingles-imagen1">
                  <img class="img-fluid" src="imagenes/12.JPG" width="644" height="483" alt="examen de ingles"/>
                </label>
                <br/>
                <input type= "radio" name="parte2p6" id="memoria1" value="0" required><b> a) go</b><br/>
                <input type= "radio" name="parte2p6" id="memoria2" value="1" required><b> b) going</b><br/>
                <input type= "radio" name="parte2p6" id="memoria3" value="2" required><b> c) goes</b><br/>
                <input type= "radio" name="parte2p6" id="memoria4" value="3" required><b> d) No se cómo se debe contestar</b>
              </div>
              <br/><br/>

              <div class="form-group">
                <label for="respuesta-ingles-1">
                  <span class="font-weight-bold">
                    QUESTION 12.- 
                  </span>
                </label>
                <label for="respuesta-ingles-imagen1">
                  <img class="img-fluid" src="imagenes/13.JPG" width="644" height="483" alt="examen de ingles"/>
                </label>
                <br/>
                <input type= "radio" name="parte2p7" id="memoria1" value="0" required><b> a) because</b><br/>
                <input type= "radio" name="parte2p7" id="memoria2" value="1" required><b> b) due to</b><br/>
                <input type= "radio" name="parte2p7" id="memoria3" value="2" required><b> c) despite</b><br/>
                <input type= "radio" name="parte2p7" id="memoria4" value="3" required><b> d) No se cómo se debe contestar</b>
              </div>
              <br/><br/>

              <div class="form-group">
                <label for="respuesta-ingles-1">
                  <span class="font-weight-bold">
                    QUESTION 13.- 
                  </span>
                </label>
                <label for="respuesta-ingles-imagen1">
                  <img class="img-fluid" src="imagenes/14.JPG" width="644" height="483" alt="examen de ingles"/>
                </label>
                <br/>
                <input type= "radio" name="parte2p8" id="memoria1" value="0" required><b> a) so</b><br/>
                <input type= "radio" name="parte2p8" id="memoria2" value="1" required><b> b) very</b><br/>
                <input type= "radio" name="parte2p8" id="memoria3" value="2" required><b> c) too</b><br/>
                <input type= "radio" name="parte2p8" id="memoria4" value="3" required><b> d) No se cómo se debe contestar</b>
              </div>
              <br/><br/>


              <!-- Etapa 2 -->
              <h3>Etapa 3</h3> 
              <span class="font-weight-bold">
                Instrucciones Parte 3: 
              </span> /  
              <audio class="asado" src="audios/etapa_3_instrucciones.mp3" controls>
                <p>Tu navegador no implementa el elemento audio</p>
              </audio>
              <div class="form-group"></div>
              <br/>

              <div class="form-group">
                <label for="respuesta-ingles-1">
                  <span class="font-weight-bold">
                    QUESTION 14.- 
                  </span>
                </label>
                <label for="respuesta-ingles-imagen1">
                  <img class="img-fluid" src="imagenes/15.JPG" width="644" height="483" alt="examen de ingles"/>
                </label>
                <br/>
                <input type= "radio" name="parte3p1" id="memoria1" value="0" required><b> a) kinda</b><br/>
                <input type= "radio" name="parte3p1" id="memoria2" value="1" required><b> b) coz</b>y<br/>
                <input type= "radio" name="parte3p1" id="memoria3" value="2" required><b> c) to</b><br/>
                <input type= "radio" name="parte3p1" id="memoria4" value="3" required><b> d) No se cómo se debe contestar</b>
              </div>
              <br/><br/>

              <div class="form-group">
                <label for="respuesta-ingles-1">
                  <span class="font-weight-bold">
                    QUESTION 15.- 
                  </span>
                </label>
                <label for="respuesta-ingles-imagen1">
                  <img class="img-fluid" src="imagenes/16.JPG" width="644" height="483" alt="examen de ingles"/>
                </label>
                <br/>
                <input type= "radio" name="parte3p2" id="memoria1" value="0" required><b> a) Neither</b><br/>
                <input type= "radio" name="parte3p2" id="memoria2" value="1" required><b> b) or</b><br/>
                <input type= "radio" name="parte3p2" id="memoria3" value="2" required><b> c) in</b><br/>
                <input type= "radio" name="parte3p2" id="memoria4" value="3" required><b> d) No se cómo se debe contestar</b>
              </div>
              <br/><br/>

              <div class="form-group">
                <label for="respuesta-ingles-1">
                  <span class="font-weight-bold">
                    QUESTION 16.- 
                  </span>
                </label>
                <label for="respuesta-ingles-imagen1">
                  <img class="img-fluid" src="imagenes/17.JPG" width="644" height="483" alt="examen de ingles"/>
                </label>
                <br/>
                <input type= "radio" name="parte3p3" id="memoria1" value="0" required><b> a) Much</b><br/>
                <input type= "radio" name="parte3p3" id="memoria2" value="1" required><b> b) enjoy</b><br/>
                <input type= "radio" name="parte3p3" id="memoria3" value="2" required><b> c) riding</b><br/>
                <input type= "radio" name="parte3p3" id="memoria4" value="3" required><b> d) No se cómo se debe contestar</b>
              </div>
              <br/><br/>

              <div class="form-group">
                <label for="respuesta-ingles-1">
                  <span class="font-weight-bold">
                    QUESTION 17.- 
                  </span>
                </label>
                <label for="respuesta-ingles-imagen1">
                  <img class="img-fluid" src="imagenes/18.JPG" width="644" height="483" alt="examen de ingles"/>
                </label>
                <br/>
                <input type= "radio" name="parte3p4" id="memoria1" value="0" required><b> a) will</b><br/>
                <input type= "radio" name="parte3p4" id="memoria2" value="1" required><b> b) in</b><br/>
                <input type= "radio" name="parte3p4" id="memoria3" value="2" required><b> c) Wilson´s</b><br/>
                <input type= "radio" name="parte3p4" id="memoria4" value="3" required><b> d) No se cómo se debe contestar</b>
              </div>

              <div class="form-group">
                <label for="respuesta-ingles-1">
                  <span class="font-weight-bold">
                    QUESTION 18.- 
                  </span>
                </label>
                <label for="respuesta-ingles-imagen1">
                  <img class="img-fluid" src="imagenes/20.PNG" width="644" height="483" alt="examen de ingles"/>
                </label>
                <br/>
                <input type= "radio" name="parte3p5" id="memoria1" value="0" required><b> a) Invited</b><br/>
                <input type= "radio" name="parte3p5" id="memoria2" value="1" required><b> b) Would</b><br/>
                <input type= "radio" name="parte3p5" id="memoria3" value="2" required><b> c) Had</b><br/>
                <input type= "radio" name="parte3p5" id="memoria4" value="3" required><b> d) No se cómo se debe contestar</b>
              </div>

              <div class="form-group">
                <label for="respuesta-ingles-1">
                  <span class="font-weight-bold">
                    QUESTION 19.- 
                  </span>
                </label>
                <label for="respuesta-ingles-imagen1">
                  <img class="img-fluid" src="imagenes/21.PNG" width="644" height="483" alt="examen de ingles"/>
                </label>
                <br/>
                <input type= "radio" name="parte3p6" id="memoria1" value="0" required><b> a) more</b><br/>
                <input type= "radio" name="parte3p6" id="memoria2" value="1" required><b> b) earn</b><br/>
                <input type= "radio" name="parte3p6" id="memoria3" value="2" required><b> c) be</b><br/>
                <input type= "radio" name="parte3p6" id="memoria4" value="3" required><b> d) No se cómo se debe contestar</b>
              </div>

              <div class="form-group">
                <label for="respuesta-ingles-1">
                  <span class="font-weight-bold">
                    QUESTION 20.- 
                  </span>
                </label>
                <label for="respuesta-ingles-imagen1">
                  <img class="img-fluid" src="imagenes/22.PNG" width="644" height="483" alt="examen de ingles"/>
                </label>
                <br/>
                <input type= "radio" name="parte3p7" id="memoria1" value="0" required><b> a) have</b><br/>
                <input type= "radio" name="parte3p7" id="memoria2" value="1" required><b> b) bring</b><br/>
                <input type= "radio" name="parte3p7" id="memoria3" value="2" required><b> c) knew</b><br/>
                <input type= "radio" name="parte3p7" id="memoria4" value="3" required><b> d) No se cómo se debe contestar</b>
              </div>
              <br/><br/>


              <h3>Etapa 4</h3> 
              <span class="font-weight-bold">
                Instrucciones Parte 4: 
              </span> /  
              <audio class="asado" src="audios/audioparte3.mp3" controls>
                <p>Tu navegador no implementa el elemento audio</p>
              </audio>
              <div class="form-group"></div>
              <br/>

                      
              <!-- TERMINA INSTRUCCIONES TRES -->

              <div class="form-group">
                <label for="respuesta-ingles-imagen1">
                  <img class="img-fluid" src="imagenes/Diapositiva15.PNG" width="644" height="483" alt="examen de ingles"/>
                </label>
                <textarea class="form-control" placeholder="Escribe tu respuesta aquí" name="respuesta_imagen_1" id="respuesta_imagen_1" rows="3" required></textarea>
              </div>
              <br/>

              <!-- INSTRUCCIONES TRES -->
              <br/><br/>
              <h4>Audio de despedida</h4>
              <audio class="asado" src="audios/Despedida.mp3" controls>
                <p>Tu navegador no implementa el elemento audio</p>
              </audio>
              <br/>
                <label for="respuesta-ingles-imagen1">
                  <img class="img-fluid" src="imagenes/19.jpg" width="644" height="483" alt="examen de ingles"/>
                </label>
              <br/><br/>
              <br/><br/>

              <!-- TERMINA INSTRUCCIONES TRES -->
              <div class="text-center">
                <button type="submit" id="entrar" name="entrar" class="btn btn-primary">Finalizar</button>
              </div>

              <!-- onclick="evitarDuplicidadEnvio();" -->
              <style>
                #enviando {
                  display:none;
                  text-align:center;
                }
              </style>
              <div class="form-group">
                <div class="text-center">
                <table width="100%">
                <tr>
                <td width="33%"></td>
                <td width="33%"><button id="enviando" type="button" class="btn btn-info"> Enviando formulario </button></td>
                <td width="33%"></td>
                </tr>
                </table>
                </div>
              </div>
            </form>
            <br/><br/>

          </div>
          <div class="col-md-2"></div>
        </div><!-- Termina el Row -->
        
      </div><!--Termima container-->
    <script>
      function evitarDuplicidadEnvio(){
        /*
        var nombre = document.getElementById("nombre").value;
        if( nombre == null || nombre.length == 0 || /^\s+$/.test(nombre) ) {
          alert("Debe de ingresar su nombre");
          return false;
        }

        var correo = document.getElementById("correo").value;
        if( correo == null || correo.length == 0 || /^\s+$/.test(correo) ) {
          alert("Debe de ingresar su correo");
          return false;
        }

        if( !(/\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)/.test(correo)) ) {
          alert("Debe de ingresar un correo valido");
          return false;
        }

        
        
        if( telefono == null || telefono.length == 0 || /^\s+$/.test(telefono) ) {
          alert("Debe de ingresar su teléfono");
          return false;
        }

        if( telefono == null || telefono.length == 0 || /^\s+$/.test(telefono) ) {
          alert("Debe de ingresar su teléfono");
          return false;
        }
        motivo_aprender
        sucursal
        horario
        respuesta1
        respuesta2
        respuesta3
        respuesta4
        respuesta5
        respuesta_ingles_1
        respuesta_ingles_2
        respuesta_ingles_3
        respuesta_ingles_4
        respuesta_ingles_5
        respuesta_imagen_1
        respuesta_imagen_2
        respuesta_imagen_3
        */
          var entrar   = document.getElementById("entrar");
          var enviando = document.getElementById("enviando");
          entrar.style.display = "none";
          enviando.style.display = "block";
          this.form.submit();   
      }

      function verRespuesta(){
        var sum = 0;
        let parte1p1 = document.getElementsByName('parte1p1');
        let parte1p2 = document.getElementsByName('parte1p2');
        let parte1p3 = document.getElementsByName('parte1p3');
        let parte1p4 = document.getElementsByName('parte1p4');
        let parte1p5 = document.getElementsByName('parte1p5');

        let parte2p1 = document.getElementsByName('parte2p1');
        let parte2p2 = document.getElementsByName('parte2p2');
        let parte2p3 = document.getElementsByName('parte2p3');
        let parte2p4 = document.getElementsByName('parte2p4');
        let parte2p5 = document.getElementsByName('parte2p5');
        let parte2p6 = document.getElementsByName('parte2p6');
        let parte2p7 = document.getElementsByName('parte2p7');
        let parte2p8 = document.getElementsByName('parte2p8');
        
        let parte3p1 = document.getElementsByName('parte3p1');
        let parte3p2 = document.getElementsByName('parte3p2');
        let parte3p3 = document.getElementsByName('parte3p3');
        let parte3p4 = document.getElementsByName('parte3p4');
        let parte3p5 = document.getElementsByName('parte3p4');
        let parte3p6 = document.getElementsByName('parte3p4');
        let parte3p7 = document.getElementsByName('parte3p4');
        
        if(parte1p1[1].checked){ sum += 1; }
        if(parte1p2[0].checked){ sum += 1; }
        if(parte1p3[0].checked){ sum += 1; }
        if(parte1p4[0].checked){ sum += 1; }
        if(parte1p5[0].checked){ sum += 1; }

        if(parte2p1[1].checked){ sum += 1; }
        if(parte2p2[0].checked){ sum += 1; }
        if(parte2p3[0].checked){ sum += 1; }
        if(parte2p4[1].checked){ sum += 1; }
        if(parte2p5[1].checked){ sum += 1; }
        if(parte2p6[2].checked){ sum += 1; }
        if(parte2p7[1].checked){ sum += 1; }
        if(parte2p8[1].checked){ sum += 1; }

        if(parte3p1[0].checked){ sum += 1; }
        if(parte3p2[1].checked){ sum += 1; }
        if(parte3p3[0].checked){ sum += 1; }
        if(parte3p4[1].checked){ sum += 1; }
        if(parte3p5[2].checked){ sum += 1; }
        if(parte3p6[2].checked){ sum += 1; }
        if(parte3p7[1].checked){ sum += 1; }

        console.log(sum)
      }
    </script>

  <?php  
  }
  else{
    $sum = 0;

    $parte1p1 = limpiar($_POST['parte1p1']);
    $parte1p2 = limpiar($_POST['parte1p2']);
    $parte1p3 = limpiar($_POST['parte1p3']);
    $parte1p4 = limpiar($_POST['parte1p4']);
    $parte1p5 = limpiar($_POST['parte1p5']);

    $parte2p1 = limpiar($_POST['parte2p1']);
    $parte2p2 = limpiar($_POST['parte2p2']);
    $parte2p3 = limpiar($_POST['parte2p3']);
    $parte2p4 = limpiar($_POST['parte2p4']);
    $parte2p5 = limpiar($_POST['parte2p5']);
    $parte2p6 = limpiar($_POST['parte2p6']);
    $parte2p7 = limpiar($_POST['parte2p7']);
    $parte2p8 = limpiar($_POST['parte2p8']);

    $parte3p1 = limpiar($_POST['parte3p1']);
    $parte3p2 = limpiar($_POST['parte3p2']);
    $parte3p3 = limpiar($_POST['parte3p3']);
    $parte3p4 = limpiar($_POST['parte3p4']);
    $parte3p5 = limpiar($_POST['parte3p4']);
    $parte3p6 = limpiar($_POST['parte3p4']);
    $parte3p7 = limpiar($_POST['parte3p4']);
    
    if($parte1p1 == 1){ $sum += 1; }
    if($parte1p2 == 0){ $sum += 1; }
    if($parte1p3 == 0){ $sum += 1; }
    if($parte1p4 == 0){ $sum += 1; }
    if($parte1p5 == 0){ $sum += 1; }

    if($parte2p1 == 1){ $sum += 1; }
    if($parte2p2 == 0){ $sum += 1; }
    if($parte2p3 == 0){ $sum += 1; }
    if($parte2p4 == 1){ $sum += 1; }
    if($parte2p5 == 1){ $sum += 1; }
    if($parte2p6 == 2){ $sum += 1; }
    if($parte2p7 == 1){ $sum += 1; }
    if($parte2p8 == 2){ $sum += 1; }

    if($parte3p1 == 2){ $sum += 1; }
    if($parte3p2 == 1){ $sum += 1; }
    if($parte3p3 == 0){ $sum += 1; }
    if($parte3p4 == 1){ $sum += 1; }
    if($parte3p5 == 2){ $sum += 1; }
    if($parte3p6 == 2){ $sum += 1; }
    if($parte3p7 == 1){ $sum += 1; }

    $nombre          = limpiar($_POST['nombre']);
    $correo          = limpiar($_POST['correo']);
    $telefono        = limpiar($_POST['telefono']);
    $motivo_aprender = limpiar($_POST['motivo_aprender']);
    $sucursal        = limpiar($_POST['sucursal']);
    $nivel_ingles    = limpiar($_POST['nivel_ingles']);
    $estudios        = limpiar($_POST['estudios']);
    $sqlNombreSucursal="SELECT nombre FROM sucursales WHERE id=$sucursal";

    if (!$resultado = $conn->query($sqlNombreSucursal)) {
      $nombreSucursal ="SIN SUCURSAL";
    }

    $data = $resultado->fetch_assoc();
    $nombreSucursal = $data['nombre'];

    $horario         = limpiar($_POST['horario']);
    switch($horario){
      case "manana":
        $nombreHorario="Mañana";
        break;
      case "tarde":
        $nombreHorario="Tarde";
        break;
      case "sabatino":
        $nombreHorario="Sabatino";
        break;
      case "dominical":
        $nombreHorario="Dominical";
      break;
      default:
      $nombreHorario="SIN HORARIO";
      break;
    }

    $fecha              = date("Y-m-d H:i:s");
    $uniq               = uniqid();
    $origen             = "WEB";
    $respuesta_imagen_1 = limpiar($_POST['respuesta_imagen_1']);
    $respuesta_imagen_3 = "";

    $parte1p1 = limpiar($_POST['parte1p1']);

    if (mysqli_connect_errno()) {
        echo "error al conectar la base de datos";
        exit();
    }

    $sql = "INSERT INTO examen_ubicacion (
    uniq,
    nombre,
    correo,telefono,motivo_aprender,sucursal,horario,nivel_ingles,estudios,fecha_registro,origen,respuesta_imagen_1,respuesta_imagen_3,cant) VALUES (
    '".$uniq."',
    '".$nombre."',
    '".$correo."',
    '".$telefono."',
    '".$motivo_aprender."',
    ".$sucursal.",
    '".$horario."',
    '".$nivel_ingles."',
    '".$estudios."',
    '".$fecha."',
    '".$origen."',
    '".$respuesta_imagen_1."',
    '".$respuesta_imagen_3."',
    '".$sum."')";

    if ($conn->query($sql) === TRUE) {

      $to      = 'fastenglishventas@gmail.com,fastenglishventas1@gmail.com,armando.molina@inbi.mx';
      // $to      = 'eduardo.zav.dev@gmail.com';
      $subject = 'Registro de examen de ubicación Fastenglish.';


      $headers = "From: " . $correo. "\r\n";
      $headers .= "MIME-Version: 1.0\r\n";
      $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

      $message = "
        <!doctype html>
        <html lang=\"en\">
          <head>
            <title>Registro de examen de ubicación.</title>
        <style>
          body {
            margin: 0;
            padding: 0;
          }
          h1 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 110%;
            font-weight: bold;
            color: #2E86C1;
          }

          h2 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 100%;
            font-weight: bold;
            color: #2E86C1 ;
          }
        p{
          color: #666;
          font-family: Arial, Helvetica, sans-serif;
        }
          .center {
            position: relative;
            margin: auto;
            padding: auto;
            text-align: center;
          }
          .contenedor {
            width: 80%;
            height: auto;
            padding: 0;
            margin: auto;
            background-color: #fff;
          }
          .centro {
            width: 200px;
            height: 30px;
            vertical-align: middle;
          }
          .asado:active {
            border: none;
          }
          .linea {
            width: 100%;
        height: 1px;
        border: 1px solid #cdcdcd;
          }
          .go {
            background-color:#FF5733;
            color:#fff;
            border:none;
            padding:10px 15px 10px 15px;
            font-family: Arial, Helvetica, sans-serif;
          }
          .dato-title {
            font-size: 100%;
            font-weight: bold;
            font-family: Arial, Helvetica, sans-serif;
          }
          .dato-description {
            font-size: 100%;
            font-weight: normal;
            font-family: Arial, Helvetica, sans-serif;
          }

        </style>


        </head>
        <body>
          <div class=\"contenedor\">
            <h1>Fastenglish</h1>
            
            <h2>¡Informacion de la persona que contesto el examen de ubicación de Fastenglish!</h2>

            <p>".$nombre." a contestao el examen de evaluación, este correo es una notificación con la información basica, 
            si quiere consultar el registro completo haga click en el boton Ir al registro. </p>
            <br/><br/><br/>

              <table cellpadding=\"8\">
              <tr style=\'background: #eee;\'><td><span class=\"dato-title\">Name:</span> </td><td><span class=\"dato-description\">" . $nombre . "</span></td></tr>
              <tr><td><span class=\"dato-title\">Correo:</span></td><td><span class=\"dato-description\">" . $correo . "</span></td></tr>
              <tr><td><span class=\"dato-title\">Teléfono:</span> </td><td><span class=\"dato-description\">" . $telefono . "</span></td></tr>
              <tr><td><span class=\"dato-title\">Motivos de aprendizaje:</span> </td><td><span class=\"dato-description\">" . $motivo_aprender . "</span></td></tr>
              <tr><td><span class=\"dato-title\">Horarios que solicita:</span> </td><td><span class=\"dato-description\">" . $nombreHorario . "</span></td></tr>
              <tr><td><span class=\"dato-title\">Horarios que solicita:</span> </td><td><span class=\"dato-description\">" . $nombreSucursal . "</span></td></tr>
              <tr><td><span class=\"dato-title\">Fecha:</span> </td><td><span class=\"dato-description\">" . $fecha . "</span></td></tr>
              </table>
              <br/><br/>
              <div class=\"center\">
                
              </div>

          </div>
        </body>
        </html>
        ";

      mail($to, $subject, $message, $headers);
    ?>

    <!-- <span class=\"go\"><a href=\"https://www.fastenglish.com.mx/prueba-url.php?v=".$uniq."\">Ir al registro</a></span> -->
    <br/>
    <br/>
    <br/><br/>
    <div class="container">
      <div class="row">
        <div class="col-md-3">
        </div>
        <div class="col-md-6">
        <div class="text-center">
          <img src="img/cropped-logo-fast-png-1.png">
        </div>
        <p class="text-center">
                      Has contestado el examen con éxito, un representante de nuestra escuela se comunicará contigo a la brevedad al número que nos proporcionaste. 
                      De igual manera debes estar al pendiente de tú correo para los avisos referentes a futuros exámenes. 
                      Gracias por tú participación.</p>
        </div>
        <div class="col-md-3">
        </div>
      </div>
    </div>
    <script>
      // setTimeout(function(){ window.location.href = "https://www.fastenglish.com.mx"; }, 10000);
    </script>

  <?php
    } else {
      echo "Error al registrar, los datos del examen.";
     
    }

    $conn->close();
    }
  ?>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>