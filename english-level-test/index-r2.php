<?php
  header("Cache-Control: no-store, no-cache, must-revalidate");
  header("Cache-Control: post-check=0, pre-check=0", false);
  header("Pragma: no-cache");
?>
<!-- Black List: Buscar ip en una tabla de Black List -->
<!-- Generación de token y ID de session -->
<!-- Insertar la id y la session en la base de datos-->

<!-- Checar como esta el examen de ubicación de Grop Up -->
<!--Poner Captcha para evitar Span -->
<!--Ingresar estos datos en la base de datos -->

<!--Poder calcular el tiempo de el examen -->
<?php
date_default_timezone_set('America/Monterrey');
require 'inc/conexion.php';
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta http-equiv=”Content-Type” content=”text/html; charset=UTF-8″ />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script type="text/script" src="js/bootstrap.min.js"></script>
    <!-- <link  rel="icon"   href="favicon/favicon-32x32.png" type="image/png" />-->
    <link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-96x96.png">

    <title>Examen de ubicación Fastenglish.</title>
<style>
body {
  border-top:4px solid #074D80;
  padding-top:20px;
}

  .asado {
    width: 200px;
    height: 30px;
    vertical-align: middle;
  }
  .asado:active {
    border: none;
  }
</style>
  </head>
  <body>
    <?php
    if(!isset($_POST['entrar'])){
    ?>
      <div class="container"> <!--Inicia Container -->
        <br/><br/>
        <div class="text-center"><h1>Examen de ubicación idioma Ingles</h1> </div>
        <br/><br/>
        <div class="row">
                <div class="col-md-2"><img src="img/cropped-logo-fast-png-1.png" width="155" height="131"></div>
                <div class="col-md-8">
                </div>
                <div class="col-md-2"></div>
        </div>

        <!--Inicia el formulario -->
        <div class="row">
            
            <div class="col-md-2"></div>

            <div class="col-md-8"><br/><br/><br/>
                <span class="font-weight-bold">Instrucciones: </span> / <audio class="asado" src="audios/1.mp3" controls>
                  <p>Tu navegador no implementa el elemento audio</p>
                </audio>
                <br/>
                <!--    
                1.- Realizar la evaluación sin ayuda.<br/>
                2.- Escribir solo en INGLÉS.<br/>
                3.- Escribir lo máximo posible.
                <br/><br/><br/>-->
                <img class="img-fluid" src="imagenes/Diapositiva1.PNG" width="730" height="490" Alt="">
                <form action="" method="POST" name="forma" id="forma">
                <div class="form-group">
                        <label for="nombre">Nombre</label>
                        <input type="text" class="form-control" name="nombre" id="nombre" required>
                      </div>

                      <div class="form-group">
                        <label for="correo">Correo</label>
                        <input type="email" class="form-control" name="correo" id="correo" required>
                      </div>

                      <div class="form-group">
                        <label for="telefono">Teléfono</label>
                        <input type="text" class="form-control" name="telefono" id="telefono" required>
                      </div>

                      <div class="form-group">
                        <label for="exampleFormControlSelect1">¿Por qué quiere aprender Inglés?</label>
                        <select class="form-control" name="motivo_aprender" id="motivo_aprender" required>
                          <option>Seleccione una opción</option>
                          <option value="academico">Académico</option>
                          <option value="trabajo">Trabajo</option>
                          <option value="ocio">Ocio</option>
                        </select>
                        </div>
                        <?php
                          if ($conn->connect_errno) { 
                            // No se debe revelar información delicada
                            echo "Lo sentimos, este sitio web está experimentando problemas.";
                            echo "Error: Fallo al conectarse a MySQL debido a: \n";
                            echo "Errno: " . $conn->connect_errno . "\n";
                            echo "Error: " . $conn->connect_error . "\n";    
                            exit;
                          }

                            $sql = "SELECT * FROM sucursales";
                            if (!$resultado = $conn->query($sql)) {
                              echo "Error al buscar las su ursales";
                              exit;
                            }
                            $conn->close();  
                        ?>
                        <div class="form-group">
                        <label for="exampleFormControlSelect1">Sucursal</label>
                        <select class="form-control" name="sucursal" id="sucursal" required>
                          <option>Seleccione una opción</option>
                          <?php
                          while($data = $resultado->fetch_assoc()){
                            ?>
                            <option value="<?php echo $data['id'];?>"><?php echo $data['nombre'];?></option>
                            <?php
                          }
                          ?>
                          
                        </select>
                        </div>

                        <div class="form-group">
                        <label for="exampleFormControlSelect1">Horario</label>
                        <select class="form-control" name="horario" id="horario" required>
                          <option>Seleccione una opción</option>
                          <option value="manana">Mañana</option>
                          <option value="tarde">Tarde</option>
                          <option value="sabatino">Sabatino</option>
                          <option value="dominical">Dominical</option>
                        </select>
                        </div>

                
                <h3>Parte 1: Preguntas Cotidianas.</h3> 
                <span class="font-weight-bold">Instrucciones Parte 1: </span> /  <audio class="asado" src="audios/2.mp3" controls>
                  <p>Tu navegador no implementa el elemento audio</p>
                </audio>
                <div class="form-group">
                    
                </div>
                <br/>
                


                    <div class="form-group">
                    <img class="img-fluid" src="imagenes/Diapositiva3.PNG" width="730" height="490" Alt=""><br/><br/>
                      <textarea class="form-control" name="respuesta1" id="respuesta1" rows="3" required></textarea>
                    </div>
                    <br/><br/>

                    <div class="form-group">
                    <img class="img-fluid" src="imagenes/Diapositiva4.PNG" width="730" height="490" Alt=""><br/><br/>
                      <textarea class="form-control" name="respuesta2" id="respuesta2" rows="3" required></textarea>
                    </div>
                    <br/><br/>

                    <div class="form-group">
                    <img class="img-fluid" src="imagenes/Diapositiva5.PNG" width="730" height="490" Alt=""><br/><br/>
                      <textarea class="form-control" name="respuesta3" id="respuesta3" rows="3" required></textarea>
                    </div>
                    <br/><br/>

                    <div class="form-group">
                    <img class="img-fluid" src="imagenes/Diapositiva6.PNG" width="730" height="490" Alt=""><br/><br/>
                        <textarea class="form-control" name="respuesta4" id="respuesta4" rows="3" required></textarea>
                      </div>
                      <br/><br/>
                      
                      <div class="form-group">
                      <img class="img-fluid" src="imagenes/Diapositiva7.PNG" width="730" height="490" Alt=""><br/><br/>
                        <textarea class="form-control" name="respuesta5" id="respuesta5" rows="3" required></textarea>
                      </div>
                      <br/><br/>







                      <!-- INSTRUCCIONES DOS -->
                      <h3>Parte 2: Preguntas en Inglés.</h3>
                        <span class="font-weight-bold">Instrucciones Parte 2: </span> / <audio class="asado" src="audios/3.mp3" controls>
                        <p>Tu navegador no implementa el elemento audio</p>
                      </audio>
                      <br/><br/>
                      
                      <!-- TERMINA INSTRUCCIONES DOS -->
                      
                      <div class="form-group">
                        <label for="respuesta-ingles-1">
                            <span class="font-weight-bold">
                              QUESTION 1.- </span><audio class="asado" src="audios/4.mp3" controls>
                                <p>Tú navegador no implementa el elemento de audio</p></audio>
                        </label>
                        <textarea class="form-control" name="respuesta_ingles_1" id="respuesta_ingles_1" rows="3" required></textarea>
                      </div>
                      <br/><br/>

                      <div class="form-group">
                        <label for="respuesta-ingles-2">
                            <span class="font-weight-bold">
                              QUESTION 2.- </span><audio class="asado" src="audios/5.mp3" controls>
                                <p>Tú navegador no implementa el elemento de audio</p></audio>
                        </label>
                        <textarea class="form-control" name="respuesta_ingles_2" id="respuesta_ingles_2" rows="3" required></textarea>
                      </div>
                      <br/><br/>


                      <div class="form-group">
                        <label for="respuesta-ingles-3">
                            <span class="font-weight-bold">
                              QUESTION 3.- </span><audio class="asado" src="audios/6.mp3" controls>
                                <p>Tú navegador no implementa el elemento de audio</p></audio>
                        </label>
                        <textarea class="form-control" name="respuesta_ingles_3" id="respuesta_ingles_3" rows="3" required></textarea>
                      </div>
                      <br/><br/>



                      <div class="form-group">
                        <label for="respuesta-ingles-4">
                            <span class="font-weight-bold">
                              QUESTION 4.- </span><audio class="asado" src="audios/7.mp3" controls>
                                <p>Tú navegador no implementa el elemento de audio</p></audio>
                        </label>
                        <textarea class="form-control" name="respuesta_ingles_4" id="respuesta_ingles_4" rows="3" required></textarea>
                      </div>
                      <br/><br/>




                      <div class="form-group">
                        <label for="respuesta-ingles-5">
                            <span class="font-weight-bold">
                              QUESTION 5.- </span><audio class="asado" src="audios/8.mp3" controls>
                                <p>Tú navegador no implementa el elemento de audio</p></audio>
                        </label>
                        <textarea class="form-control" name="respuesta_ingles_5" id="respuesta_ingles_5" rows="3" required></textarea>
                      </div>
                      <br/><br/>





                      <!-- INSTRUCCIONES TRES -->
                      <h3>Parte 3: Descripción de imagenes.</h3>
                        <span class="font-weight-bold">Instrucciones Parte 3: </span> / <audio class="asado" src="audios/9.mp3" controls>
                        <p>Tu navegador no implementa el elemento audio</p>
                      </audio>
                      <br/><br/>
                      
                      <!-- TERMINA INSTRUCCIONES TRES -->


                      <div class="form-group">
                        <label for="respuesta-ingles-imagen1">
                            
                            <img class="img-fluid" src="imagenes/Diapositiva15.PNG" width="644" height="483" alt="examen de ingles"/>
                        </label>
                        <textarea class="form-control" name="respuesta_imagen_1" id="respuesta_imagen_1" rows="3" required></textarea>
                      </div>
                      <br/><br/>
                      
                      

                      <div class="form-group">
                        <label for="respuesta-ingles-imagen2">
                            
                            <img class="img-fluid" src="imagenes/Diapositiva16.PNG" width="644" height="483" alt="examen de ingles"/>
                        </label>
                        <textarea class="form-control" name="respuesta_imagen_2" id="respuesta_imagen_2" rows="3" required></textarea>
                      </div>
                      <br/><br/>
                      


                      <h3>Parte 4: Expresar vivencias.</h3> 
                <span class="font-weight-bold">Instrucciones Parte 4: </span> /  <audio class="asado" src="audios/10.mp3" controls>
                  <p>Tu navegador no implementa el elemento audio</p>
                </audio>
                <div class="form-group">
                <label for="respuesta-ingles-imagen1">
                            
                            
                  </label>
                </div>
                <br/>



                <div class="form-group">
                    <label for="respuesta-ingles-imagen2">          
                      <img class="img-fluid" src="imagenes/Diapositiva18.PNG" width="644" height="483" alt="examen de ingles"/>
                    </label>
                    <textarea class="form-control" name="respuesta_imagen_3" id="respuesta_imagen_3" rows="3" required></textarea>
                  </div>
                <br/><br/>



                      <!-- INSTRUCCIONES TRES -->
                      

                      <br/><br/>
                      <img class="img-fluid" src="imagenes/findelexamen.png" width="644" height="483" alt="examen de ingles"/> 
                      <br/><br/>
                      <audio class="asado" src="audios/11.mp3" controls>
                        <p>Tu navegador no implementa el elemento audio</p>
                      </audio>
                      <br/><br/>
                      <!-- TERMINA INSTRUCCIONES TRES -->


                  <div class="text-center">
                    <button type="submit" name="entrar" class="btn btn-primary">Finalizar</button>
                  </div>
                  </form>
                  <br/><br/>

            </div>
            <div class="col-md-2"></div>
          </div><!-- Termina el Row -->
        
        </div><!--Termima container-->
  <?php  
  }
  else
  {
          function limpiar($parametro){

          $parametro = str_replace('‘','',$parametro);
          $parametro = str_replace('"','',$parametro);
          $parametro = str_replace('–','',$parametro);
          $parametro = str_replace('/*','',$parametro);
          $parametro = str_replace('*/','',$parametro);
          $parametro = str_replace('xp_','',$parametro);
          $parametro = str_replace('<','',$parametro);
          $parametro = str_replace('>','',$parametro);
          $parametro = str_replace('insert','',$parametro);
          $parametro = str_replace('delete','',$parametro);
          $parametro = str_replace('update','',$parametro);
          $parametro = str_replace('select','',$parametro);
          $parametro = str_replace('drop','',$parametro);
          $parametro = str_replace('table','',$parametro);
          return $parametro;
          }

        $nombre          = limpiar($_POST['nombre']);
        $correo          = limpiar($_POST['correo']);
        $telefono        = limpiar($_POST['telefono']);
        $motivo_aprender = limpiar($_POST['motivo_aprender']);
        $sucursal        = limpiar($_POST['sucursal']);
        $horario         = limpiar($_POST['horario']);
        $fecha           = date("Y-m-d H:i:s");
        $uniq      = uniqid();
          $origen="WEB";
        $respuesta1      = limpiar($_POST['respuesta1']);
        $respuesta2      = limpiar($_POST['respuesta2']);
        $respuesta3      = limpiar($_POST['respuesta3']);
        $respuesta4      = limpiar($_POST['respuesta4']);
        $respuesta5      = limpiar($_POST['respuesta5']);
        $respuesta_ingles_1 = limpiar($_POST['respuesta_ingles_1']);
        $respuesta_ingles_2 = limpiar($_POST['respuesta_ingles_2']);
        $respuesta_ingles_3 = limpiar($_POST['respuesta_ingles_3']);
        $respuesta_ingles_4 = limpiar($_POST['respuesta_ingles_4']);
        $respuesta_ingles_5 = limpiar($_POST['respuesta_ingles_5']);
        $respuesta_imagen_1 = limpiar($_POST['respuesta_imagen_1']);
        $respuesta_imagen_2 = limpiar($_POST['respuesta_imagen_2']);
        $respuesta_imagen_3 = limpiar($_POST['respuesta_imagen_3']);
        

        


if (mysqli_connect_errno()) {
    echo "error al conectar la base de datos";
    exit();
}

 
$sql = "INSERT INTO examen_ubicacion (
  uniq,
  nombre,
  correo,telefono,motivo_aprender,sucursal,horario,fecha_registro,origen,respuesta1,respuesta2,respuesta3,respuesta4,respuesta5,respuesta_ingles_1,respuesta_ingles_2,
  respuesta_ingles_3,respuesta_ingles_4,respuesta_ingles_5,respuesta_imagen_1,respuesta_imagen_2,respuesta_imagen_3) VALUES (
  '".$uniq."',
  '".$nombre."',
  '".$correo."',
  '".$telefono."',
  '".$motivo_aprender."',
  ".$sucursal.",
  '".$horario."',
  '".$fecha."',
  '".$origen."',
  '".$respuesta1."',
  '".$respuesta2."',
  '".$respuesta3."',
  '".$respuesta4."',
  '".$respuesta5."',
  '".$respuesta_ingles_1."',
  '".$respuesta_ingles_2."',
  '".$respuesta_ingles_3."',
  '".$respuesta_ingles_4."',
  '".$respuesta_ingles_5."',
  '".$respuesta_imagen_1."',
  '".$respuesta_imagen_2."',
  '".$respuesta_imagen_3."')";

if ($conn->query($sql) === TRUE) {

$to      = 'gerardo.flores@inbi.mx,armando.molina@inbi.mx';
$subject = 'Registro de examen de ubicación Fastenglish.';


$headers = "From: " . $correo. "\r\n";
$headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

$message = "
<!doctype html>
<html lang=\"en\">
  <head>
    <title>Registro de examen de ubicación.</title>
<style>
  body {
    margin: 0;
    padding: 0;
  }
  h1 {
    font-family: Arial, Helvetica, sans-serif;
    font-size: 110%;
    font-weight: bold;
    color: #2E86C1;
  }

  h2 {
    font-family: Arial, Helvetica, sans-serif;
    font-size: 100%;
    font-weight: bold;
    color: #2E86C1 ;
  }
p{
  color: #666;
  font-family: Arial, Helvetica, sans-serif;
}
  .center {
    position: relative;
    margin: auto;
    padding: auto;
    text-align: center;
  }
  .contenedor {
    width: 80%;
    height: auto;
    padding: 0;
    margin: auto;
    background-color: #fff;
  }
  .centro {
    width: 200px;
    height: 30px;
    vertical-align: middle;
  }
  .asado:active {
    border: none;
  }
  .linea {
    width: 100%;
height: 1px;
border: 1px solid #cdcdcd;
  }
  .go {
    background-color:#FF5733;
    color:#fff;
    border:none;
    padding:10px 15px 10px 15px;
    font-family: Arial, Helvetica, sans-serif;
  }
  .dato-title {
    font-size: 100%;
    font-weight: bold;
    font-family: Arial, Helvetica, sans-serif;
  }
  .dato-description {
    font-size: 100%;
    font-weight: normal;
    font-family: Arial, Helvetica, sans-serif;
  }

</style>


</head>
<body>
  <div class=\"contenedor\">
    <h1>Fastenglish</h1>
    
    <h2>¡Informacion de la persona que contesto el examen de ubicación de Fastenglish!</h2>

    <p>".$nombre." a contestao el examen de evaluación, este correo es una notificación con la información basica, 
    si quiere consultar el registro completo haga click en el boton Ir al registro. </p>
    <br/><br/><br/>

      <table cellpadding=\"8\">
      <tr style=\'background: #eee;\'><td><span class=\"dato-title\">Name:</span> </td><td><span class=\"dato-description\">" . $nombre . "</span></td></tr>
      <tr><td><span class=\"dato-title\">Correo:</span></td><td><span class=\"dato-description\">" . $correo . "</span></td></tr>
      <tr><td><span class=\"dato-title\">Teléfono:</span> </td><td><span class=\"dato-description\">" . $telefono . "</span></td></tr>
      <tr><td><span class=\"dato-title\">Motivos de aprendizaje:</span> </td><td><span class=\"dato-description\">" . $motivo_aprender . "</span></td></tr>
      <tr><td><span class=\"dato-title\">Fecha:</span> </td><td><span class=\"dato-description\">" . $fecha . "</span></td></tr>
      </table>
      <br/><br/>
      <div class=\"center\">
      <span class=\"go\"><a href=\"https://www.fastenglish.com.mx/examen-ubicacion/examenes.php?v=".$uniq."\">Ir al registro</a></span>
      </div>

  </div>
</body>
</html>
";

mail($to, $subject, $message, $headers);
?>

<!-- <span class=\"go\"><a href=\"https://www.fastenglish.com.mx/examenes.php?v=".$uniq."\">Ir al registro</a></span> -->
  <br/>
  <br/>
  <br/><br/>
  <div class="container">
    <div class="row">
      <div class="col-md-3">
      </div>
      <div class="col-md-6">
      <div class="text-center">
        <img src="img/cropped-logo-fast-png-1.png">
      </div>
      <p class="text-center">
                    Has contestado el examen con éxito, un representante de nuestra escuela se comunicará contigo a la brevedad al número que nos proporcionaste. 
                    De igual manera debes estar al pendiente de tú correo para los avisos referentes a futuros exámenes. 
                    Gracias por tú participación.</p>
      </div>
      <div class="col-md-3">
      </div>
    </div>
  </div>
  <script>
        setTimeout(function(){ window.location.href = "https://www.fastenglish.com.mx"; }, 9999);
  </script>

  <?php
} else {
  echo "error al intantar hacer el insert";
 
}

    $conn->close();
      }
      ?>



    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>