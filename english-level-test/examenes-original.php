<?php
require 'inc/conexion.php';
date_default_timezone_set('America/Monterrey');
?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="css/bootstrap.min.css">



    <script type="text/script" src="js/bootstrap.min.js"></script>
  
    
    <link rel="stylesheet" href="dataTables/datatables.min.css">
    <link rel="stylesheet" href="dataTables/DataTables-1.10.18/css/dataTables.bootstrap4.min.css">
    <link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-96x96.png">



    <title>Plataforma de evaluaciones Fastenglish</title>
<style>

</style>
  </head>
  <body>

<?php


  
  if (isset($_GET['v'])) {
    $v  = limpiar($_GET['v']);
  } else {
    $v      ='';
    $limite = 10;
  }



  if(!empty($v)){
    $sql = "SELECT * FROM examen_ubicacion where uniq='".$v."'";
  }else{
    $sql = "SELECT * FROM examen_ubicacion";
  }
  
  if (!$resultado = $conn->query($sql)) {
    echo "Lo sentimos, este sitio web está experimentando problemas.";
    echo "Error: La ejecución de la consulta falló debido a: \n";
    echo "Query: " . $sql . "\n";
    echo "Errno: " . $conn->errno . "\n";
    echo "Error: " . $conn->error . "\n";
    exit;
  }

  if ($resultado->num_rows === 0) {
    echo "No se encontraron registros. Inténtelo de nuevo.";
    exit;
  }


  

  
?>

<div class="container"> <!--Inicia Container -->
    <div class="row">
        <div class="col-md-4">
        <img src="https://www.fastenglish.com.mx/examen-ubicacion/img/cropped-logo-fast-png-1.png" >
        </div>
    </div>
  </div>
</div><!--Termima container-->

<header>
         <h1 class="text-center text-light">Examenes</h1>
         <h2 class="text-center text-light"> <span class="badge badge-danger">Registros</span></h2> 
     </header>    
    <div style="height:50px"></div>
     
    <!--Ejemplo tabla con DataTables-->
    <div class="container-fluid">
        <div class="row">
                <div class="col-lg-12">
                    <div class="table-responsive">        
                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Correo</th>
                                <th>Teléfono</th>
                                <th>Motivo de aprender</th>
                                <th>Sucursal</th>
                                <th>Horario</th>
                                <th>Fecha Registro</th>
                                <th>Nivel</th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php 
                            while ($data = $resultado->fetch_assoc()) {
                            ?>
                            <tr>
                                <td><a href='detalles.php?a=<?php echo $data['id'];?>'><?php echo $data['nombre'];?></a></td>
                                <td><?php echo $data['correo'];?></td>
                                <td><?php echo $data['telefono'];?></td>
                                <td><?php echo $data['motivo_aprender'];?></td>
                                <?php
                                $suc = $data['sucursal'];
                                $sqlSucursal = "select * from sucursales where id=$suc";

                                $resSuc   = $conn->query($sqlSucursal);
                                $arraySuc = $resSuc->fetch_assoc();
                                $nombreSucursal = $arraySuc['nombre'];
                                ?>
                                <td><?php echo $nombreSucursal;?></td>
                                <td><?php echo $data['horario'];?></td>
                                <td><?php echo $data['fecha_registro'];?></td>
                                <td><?php echo $data['nivel']==0 ? 'Sin Asignar' : $data['nivel'];?></td>
                            </tr>
                          <?php
                          }            
                          ?>
                        </tbody>        
                       </table>                  
                    </div>
                </div>
        </div>  
    </div>    
      
<br/><br/><br/><br/>
   


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    
    <script src="js/jquery/jquery-3.3.1.min.js"></script>
    <script src="js/popper/popper.min"></script>
    <script src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="dataTables/datatables.min.js"></script>
    
    <!-- datatables JS -->
    <script type="text/javascript" src="datatables/datatables.min.js"></script> 

    
    
    <script>
$(document).ready(function() {    
    $('#example').DataTable({
    //para cambiar el lenguaje a español
        "language": {
                "lengthMenu": "Mostrar _MENU_ registros",
                "zeroRecords": "No se encontraron resultados",
                "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "infoFiltered": "(filtrado de un total de _MAX_ registros)",
                "sSearch": "Buscar:",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast":"Último",
                    "sNext":"Siguiente",
                    "sPrevious": "Anterior"
			     },
			     "sProcessing":"Procesando...",
            }
    });     
});
</script>
</body>
</html>