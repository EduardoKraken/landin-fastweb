<?php
require 'inc/conexion.php';
date_default_timezone_set('America/Monterrey');
?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    
    <script type="text/script" src="js/bootstrap.min.js"></script>
   
    <link rel="stylesheet" href="dataTables/DataTables-1.10.18/css/dataTables.bootstrap4.min.css">
    <link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-96x96.png">
    <title>Plataforma de evaluaciones Fastenglish</title>

  </head>
  <body>

<?php




  if(!empty($v)){
    $sql = "SELECT * FROM examen_ubicacion where uniq='".$v."'";
  }else{
    //$sql = "SELECT * FROM examen_ubicacion";
    $sql = "SELECT eu.id,eu.nombre,eu.correo,eu.telefono,eu.motivo_aprender,eu.sucursal,eu.horario,eu.fecha_registro,eu.nivel,cr.id_examen_ubicacion FROM examen_ubicacion as eu left join clasificacion_respuestas as cr on eu.id=cr.id_examen_ubicacion ";
  }
  
  if (!$resultado = $conn->query($sql)) {
    echo "Lo sentimos, este sitio web está experimentando problemas.";
    exit;
  }

  if ($resultado->num_rows === 0) {
    echo "No se encontraron registros. Inténtelo de nuevo.";
    exit;
  }  
?>

  

     
    <!--Ejemplo tabla con DataTables-->
    <div class="container-fluid">
        <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">        
                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Correo</th>
                                <th>Teléfono</th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php 
                            while ($data = $resultado->fetch_assoc()) {
                            ?>
                            <tr>
                                <td><?php echo $data['correo'];?></td>
                                <td><?php echo $data['telefono'];?></td>
                                <td><?php echo $data['motivo_aprender'];?></td>

                            </tr>
                          <?php
                          }            
                          ?>
                        </tbody>        
                       </table>                  
                    </div>
                </div>
        </div>  
    </div>    
      

   


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    
    <script src="js/jquery/jquery-3.3.1.min.js"></script>
    <script src="js/popper/popper.min"></script>
    <script src="js/bootstrap.min.js"></script>    
    <!-- datatables JS -->
    <script type="text/javascript" src="datatables/datatables.min.js"></script> 

    
    
    <script>
$(document).ready(function() {    
    $('#example').DataTable({
    //para cambiar el lenguaje a español
        "language": {
                "lengthMenu": "Mostrar _MENU_ registros",
                "zeroRecords": "No se encontraron resultados",
                "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "infoFiltered": "(filtrado de un total de _MAX_ registros)",
                "sSearch": "Buscar:",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast":"Último",
                    "sNext":"Siguiente",
                    "sPrevious": "Anterior"
			     },
			     "sProcessing":"Procesando...",
            }
    });     
});
</script>
</body>
</html>