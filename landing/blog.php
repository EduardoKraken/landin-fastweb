<?php
    session_start();
    date_default_timezone_set('America/Monterrey');
    $titulo = "blog";
    include 'includes/headers/header.php';
    include 'includes/menus/menu-superior.php';
    include 'includes/home/blog.php';
    include 'includes/footers/footer.php';
?>