<footer>
  
</footer>
</body>
<!-- JavaScript Bundle with Popper -->
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous">
</script>
<script src="public/js/all.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script type="text/javascript">
  var top = $('.scroll_fixed').offset().top - parseFloat($('.scroll_fixed').css('margin-top').replace(/auto/, 0));

$(window).scroll(function (event) {
    // what the y position of the scroll is
    var y = $(this).scrollTop();

    // whether that's below the form
    if (y >= top) {
        // if so, ad the fixed class
        $('.scroll_fixed').addClass('fixed');
    } else {
        // otherwise remove it
        $('.scroll_fixed').removeClass('fixed');
    }
});
</script>

<!-- Archivos JS por pagina -->
<?php
switch ($titulo) {
    case 'titulo_ejemplo':
        echo "<script src='public/js/nombre_archivo.js'></script>";
        break;

    case 'iniciar sesión':
        echo "<script src='public/js/iniciar_sesion.js'></script>";
        break;

    default:

        break;
}

?>



</html>