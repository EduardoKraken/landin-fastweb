<div class="container-fluid pl-0 pr-0">

	<div id="carouselExampleDark" class="carousel carousel-dark slide mb-6" data-bs-ride="carousel">
	  <div class="carousel-indicators">
	    <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
	  </div>
	  <div class="carousel-inner">
	    <div class="carousel-item active" data-bs-interval="10000">
	      <img src="public/images/banner_arriba.jpg" class="d-block w-100" alt="...">
	    </div>
	  </div>
	  <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleDark" data-bs-slide="prev">
	    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
	    <span class="visually-hidden">Previous</span>
	  </button>
	  <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleDark" data-bs-slide="next">
	    <span class="carousel-control-next-icon" aria-hidden="true"></span>
	    <span class="visually-hidden">Next</span>
	  </button>
	</div>


	<div class="row text-center p-2" style="margin-top: 25px;">
		<h2>Cursos <i class="fa fa-graduation-cap text-danger" aria-hidden="true"></i></h2>
	</div>

	<div id="containerCards" class="row pl-0 pr-0 ma-0" style="padding: 0px;--bs-gutter-x: 0rem;">

    <div class="card">
      <img src="public/images/intensivo.jpg">
      <h4 style="margin-top: 6px;">Intensivo diario</h4>
      <!-- <a href="#" class="text-danger">Leer más</a> -->
    </div>
    
    <div class="card">
      <img src="public/images/sabatino.jpg">
      <h4 style="margin-top: 6px;">Intensivo sabatino</h4>
      <!-- <a href="#" class="text-danger">Leer más</a> -->
    </div>
    
    <div class="card">
      <img src="public/images/dominical.jpg">
      <h4 style="margin-top: 6px;">Intensivo dominical</h4>
      <!-- <a href="#" class="text-danger">Leer más</a> -->
    </div>

  </div>
	
	<div class="row text-center p-2" style="margin-top: 25px; margin-bottom: 25px;">
		<h2>¿Por qué somos la mejor escuela? <i class="fa fa-university text-danger" aria-hidden="true"></i></h2>
	</div>

	<div class="container">
	  <div class="row pl-0 pr-0 ma-0 d-flex flex-wrap align-items-center">
			<div class="col-xl-6 col-md-6 mb-4 align-items-center">
				<ol id="lista4">
			    <li class="card">Método exclusivo DAS Way (Diviértete, Aprender y Socializa)</li>
			    <li class="card">Mayor contenido en menor tiempo</li>
			    <li class="card">Clases 100% en inglés</li>
			    <li class="card">Plataforma Virtual</li> 
			    <li class="card">Aulas Inteligentes</li> 
			    <li class="card">Certificación en base al Marco Común Europeo de Referencia. (MCER)</li>
			    <li class="card">Costo Único y sin cargos fantasmas</li> 
				</ol>
			</div>
			<div class="col-xl-6 col-md-6 mb-4 align-items-center text-center" >
				<img src="public/images/mejor_escuela.jpg" alt="" width="90%" height="auto" class="thumbnails">
			</div>
		</div>
	</div>


	<div class="container">
	  <div class="row pl-0 pr-0 ma-0 d-flex flex-wrap align-items-center">
			<div class="col-xl-6 col-md-6 mb-4 align-items-center text-center" >
				<img src="public/images/sucursales.jpg" alt="" width="90%" height="auto" class="thumbnails">
			</div>
			<div class="col-xl-6 col-md-6 mb-4 align-items-center">
				<div class="row text-center p-2" style="margin-top: 25px; margin-bottom: 25px;">
					<h2>Sucursales <i class="fa fa-map-marker-alt text-danger" aria-hidden="true"></i></h2>
				</div>

				<div class="row pl-0 pr-0 ma-0 d-flex flex-wrap align-items-center">

					<div class="col-xl-6 col-md-6 mb-4 align-items-center" onclick="abrirModalOne()">
						<ol id="lista5">
			    		<li class="card">Linda Vista</li>
			    	</ol>
					</div>

					<div class="col-xl-6 col-md-6 mb-4 align-items-center" onclick="abrirModalTwo()">
						<ol id="lista5">
			    		<li class="card">Mitras</li>
			    	</ol>
					</div>

					<div class="col-xl-6 col-md-6 mb-4 align-items-center" onclick="abrirModalTre()">
						<ol id="lista5">
			    		<li class="card">Universidad</li>
			    	</ol>
					</div>

					<div class="col-xl-6 col-md-6 mb-4 align-items-center" onclick="abrirModalFour()">
						<ol id="lista5">
			    		<li class="card">Apodaca</li>
			    	</ol>
					</div>

					<div class="col-xl-6 col-md-6 mb-4 align-items-center" onclick="abrirModalFive()">
						<ol id="lista5">
			    		<li class="card">Noria</li>
			    	</ol>
					</div>

					<div class="col-xl-6 col-md-6 mb-4 align-items-center" onclick="abrirModalSix()">
						<ol id="lista5">
			    		<li class="card">Eloy cavazos</li>
			    	</ol>
					</div>

					<div class="col-xl-6 col-md-6 mb-4 align-items-center" onclick="abrirModalSeven()">
						<ol id="lista5">
			    		<li class="card">Rómulo garza</li>
			    	</ol>
					</div>
				</div>

			</div>
		</div>
	</div>

	


	<div id="modal1" class="modal fade " id="exampleModalToggle" aria-hidden="true" aria-labelledby="exampleModalToggleLabel" tabindex="-1">
	  <div class="modal-dialog modal-dialog-centered modal-xl">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalToggleLabel">Linda vista</h5>
	        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      </div>
	      <div class="modal-body">
	      	<div class="row pl-0 pr-0 ma-0 d-flex flex-wrap align-items-center">

					<div class="col-xl-6 col-md-6 mb-4 align-items-center" onclick="abrirModalOne()">
	        	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3595.304241905932!2d-100.25665878461255!3d25.69434891765511!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8662eab3d61198b7%3A0x7a067fb45d8f9e1a!2sAv.%20Linda%20Vista%20209%2C%20Linda%20Vista%2C%2067130%20Guadalupe%2C%20N.L.!5e0!3m2!1ses!2smx!4v1620411341146!5m2!1ses!2smx" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
					</div>

					<div class="col-xl-6 col-md-6 mb-4 align-items-center">
						<div class="row text-center p-2" style="margin-top: 25px; margin-bottom: 25px;">
							<h2>Información <i class="fa fa-map-marker-alt text-danger" aria-hidden="true"></i></h2>
						</div>
						<ol id="lista6">
			    		<li class="card"><b>Teléfono:</b> (81) 2164 2470</li>
			    		<li class="card"><b>Dirección:</b> Av. Linda Vista 209, Linda Vista, 67130 Guadalupe, N.L.</li>
			    		<li class="card"><b>Horaios:</b>
			    			<ul>Lunes a viernes: 09:00 a 21:00</ul>
			    			<ul>Sábados:         09:00 a 16:00</ul>
			    			<ul>Domingos:        09:00 a 15:00</ul>
			    		</li>
			    	</ol>
					</div>
					
				</div>

	      </div>
	      <div class="modal-footer">
	        <button class="btn btn-primary" data-bs-target="#exampleModalToggle2" data-bs-toggle="modal" data-bs-dismiss="modal">Open second modal</button>
	      </div>
	    </div>
	  </div>
	</div>


	<div id="modal2" class="modal fade " id="exampleModalToggle" aria-hidden="true" aria-labelledby="exampleModalToggleLabel" tabindex="-1">
	  <div class="modal-dialog modal-dialog-centered modal-xl">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalToggleLabel">Mitras</h5>
	        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      </div>
	      <div class="modal-body">
	      	<div class="row pl-0 pr-0 ma-0 d-flex flex-wrap align-items-center">

					<div class="col-xl-6 col-md-6 mb-4 align-items-center" onclick="abrirModalOne()">
	        	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3594.6560240233903!2d-100.35476318461235!3d25.715810516773853!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8662967f02043c6d%3A0xae6279564f13b157!2sAv%20Abraham%20Lincoln%203644%2C%20Jard%C3%ADn%20de%20Las%20Mitras%2C%2064300%20Monterrey%2C%20N.L.!5e0!3m2!1ses!2smx!4v1620413929465!5m2!1ses!2smx" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
					</div>

					<div class="col-xl-6 col-md-6 mb-4 align-items-center">
						<div class="row text-center p-2" style="margin-top: 25px; margin-bottom: 25px;">
							<h2>Información <i class="fa fa-map-marker-alt text-danger" aria-hidden="true"></i></h2>
						</div>
						<ol id="lista6">
			    		<li class="card"><b>Teléfono:</b> (81) 2261 4706</li>
			    		<li class="card"><b>Dirección:</b> Av Abraham Lincoln 3644, Jardín de Las Mitras, 64300 Monterrey, N.L.</li>
			    		<li class="card"><b>Horaios:</b>
			    			<ul>Lunes a viernes: 09:00 a 21:00</ul>
			    			<ul>Sábados:         09:00 a 16:00</ul>
			    			<ul>Domingos:        09:00 a 16:00</ul>
			    		</li>
			    	</ol>
					</div>
					
				</div>

	      </div>
	      <div class="modal-footer">
	        <button class="btn btn-primary" data-bs-target="#exampleModalToggle2" data-bs-toggle="modal" data-bs-dismiss="modal">Open second modal</button>
	      </div>
	    </div>
	  </div>
	</div>


	<div id="modal3" class="modal fade " id="exampleModalToggle" aria-hidden="true" aria-labelledby="exampleModalToggleLabel" tabindex="-1">
	  <div class="modal-dialog modal-dialog-centered modal-xl">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalToggleLabel">Universidad</h5>
	        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      </div>
	      <div class="modal-body">
	      	<div class="row pl-0 pr-0 ma-0 d-flex flex-wrap align-items-center">

					<div class="col-xl-6 col-md-6 mb-4 align-items-center" onclick="abrirModalOne()">
	        	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3593.4470691171296!2d-100.29943868461176!3d25.755792815130196!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8662948f1adf70bb%3A0xbd5f187daaf44d6a!2sC.%20Gral.%20Bernardo%20Reyes%20700%2C%20Iturbide%2C%2066420%20San%20Nicol%C3%A1s%20de%20los%20Garza%2C%20N.L.!5e0!3m2!1ses!2smx!4v1620414131756!5m2!1ses!2smx" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
					</div>

					<div class="col-xl-6 col-md-6 mb-4 align-items-center">
						<div class="row text-center p-2" style="margin-top: 25px; margin-bottom: 25px;">
							<h2>Información <i class="fa fa-map-marker-alt text-danger" aria-hidden="true"></i></h2>
						</div>
						<ol id="lista6">
			    		<li class="card"><b>Teléfono:</b> (81) 2261 4358</li>
			    		<li class="card"><b>Dirección:</b> C. Gral. Bernardo Reyes 700, Iturbide, 66420 San Nicolás de los Garza, N.L.</li>
			    		<li class="card"><b>Horaios:</b>
			    			<ul>Lunes a viernes: 09:00 a 21:00</ul>
			    			<ul>Sábados:         09:00 a 16:00</ul>
			    			<ul>Domingos:        09:00 a 16:00</ul>
			    		</li>
			    	</ol>
					</div>
					
				</div>

	      </div>
	      <div class="modal-footer">
	        <button class="btn btn-primary" data-bs-target="#exampleModalToggle2" data-bs-toggle="modal" data-bs-dismiss="modal">Open second modal</button>
	      </div>
	    </div>
	  </div>
	</div>

	<div id="modal4" class="modal fade " id="exampleModalToggle" aria-hidden="true" aria-labelledby="exampleModalToggleLabel" tabindex="-1">
	  <div class="modal-dialog modal-dialog-centered modal-xl">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalToggleLabel">Apodaca</h5>
	        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      </div>
	      <div class="modal-body">
	      	<div class="row pl-0 pr-0 ma-0 d-flex flex-wrap align-items-center">

					<div class="col-xl-6 col-md-6 mb-4 align-items-center" onclick="abrirModalOne()">
	        	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3592.6675849270578!2d-100.18376188461141!3d25.781541114070485!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8662ec088391bb71%3A0xa210501648aaf9de!2sGarza%20Garc%C3%ADa%20820%2C%20Moderno%20Apodaca%2C%2066600%20Cd%20Apodaca%2C%20N.L.!5e0!3m2!1ses!2smx!4v1620414194865!5m2!1ses!2smx" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
					</div>

					<div class="col-xl-6 col-md-6 mb-4 align-items-center">
						<div class="row text-center p-2" style="margin-top: 25px; margin-bottom: 25px;">
							<h2>Información <i class="fa fa-map-marker-alt text-danger" aria-hidden="true"></i></h2>
						</div>
						<ol id="lista6">
			    		<li class="card"><b>Teléfono:</b> (81) 2168 2526</li>
			    		<li class="card"><b>Dirección:</b> Garza García 820, Moderno Apodaca, 66600 Cd Apodaca, N.L.</li>
			    		<li class="card"><b>Horaios:</b>
			    			<ul>Lunes a viernes: 09:00 a 21:00</ul>
			    			<ul>Sábados:         09:00 a 16:00</ul>
			    			<ul>Domingos:        09:00 a 16:00</ul>
			    		</li>
			    	</ol>
					</div>
					
				</div>

	      </div>
	      <div class="modal-footer">
	        <button class="btn btn-primary" data-bs-target="#exampleModalToggle2" data-bs-toggle="modal" data-bs-dismiss="modal">Open second modal</button>
	      </div>
	    </div>
	  </div>
	</div>

	<div id="modal5" class="modal fade " id="exampleModalToggle" aria-hidden="true" aria-labelledby="exampleModalToggleLabel" tabindex="-1">
	  <div class="modal-dialog modal-dialog-centered modal-xl">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalToggleLabel">Noria</h5>
	        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      </div>
	      <div class="modal-body">
	      	<div class="row pl-0 pr-0 ma-0 d-flex flex-wrap align-items-center">

					<div class="col-xl-6 col-md-6 mb-4 align-items-center" onclick="abrirModalOne()">
	        	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3594.1720057017587!2d-100.19652568461207!3d25.731824816115708!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8662ebb5418b1bab%3A0x9626c76835553264!2sEl%20Arenal%20301%2C%20Noria%20Sur%2C%2066633%20Cd%20Apodaca%2C%20N.L.!5e0!3m2!1ses!2smx!4v1620414475102!5m2!1ses!2smx" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
					</div>

					<div class="col-xl-6 col-md-6 mb-4 align-items-center">
						<div class="row text-center p-2" style="margin-top: 25px; margin-bottom: 25px;">
							<h2>Información <i class="fa fa-map-marker-alt text-danger" aria-hidden="true"></i></h2>
						</div>
						<ol id="lista6">
			    		<li class="card"><b>Teléfono:</b> (81) 1351 5613</li>
			    		<li class="card"><b>Dirección:</b> El Arenal 301, Noria Sur, 66633 Cd Apodaca, N.L.</li>
			    		<li class="card"><b>Horaios:</b>
			    			<ul>Lunes a viernes: 09:00 a 21:00</ul>
			    			<ul>Sábados:         09:00 a 16:00</ul>
			    			<ul>Domingos:        09:00 a 16:00</ul>
			    		</li>
			    	</ol>
					</div>
					
				</div>

	      </div>
	      <div class="modal-footer">
	        <button class="btn btn-primary" data-bs-target="#exampleModalToggle2" data-bs-toggle="modal" data-bs-dismiss="modal">Open second modal</button>
	      </div>
	    </div>
	  </div>
	</div>

	<div id="modal6" class="modal fade " id="exampleModalToggle" aria-hidden="true" aria-labelledby="exampleModalToggleLabel" tabindex="-1">
	  <div class="modal-dialog modal-dialog-centered modal-xl">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalToggleLabel">Eloy cavazos</h5>
	        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      </div>
	      <div class="modal-body">
	      	<div class="row pl-0 pr-0 ma-0 d-flex flex-wrap align-items-center">

					<div class="col-xl-6 col-md-6 mb-4 align-items-center" onclick="abrirModalOne()">
	        	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3596.5352426508534!2d-100.21248568461314!3d25.653546119328887!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8662c1b40f5d0673%3A0x59cbd557f3d003b5!2sAv%20Eloy%20Cavazos%205100%2C%20Residencial%20San%20Eduardo%2C%2067183%20Guadalupe%2C%20N.L.!5e0!3m2!1ses!2smx!4v1620414585399!5m2!1ses!2smx" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
					</div>

					<div class="col-xl-6 col-md-6 mb-4 align-items-center">
						<div class="row text-center p-2" style="margin-top: 25px; margin-bottom: 25px;">
							<h2>Información <i class="fa fa-map-marker-alt text-danger" aria-hidden="true"></i></h2>
						</div>
						<ol id="lista6">
			    		<li class="card"><b>Teléfono:</b> (81) 2556 9221</li>
			    		<li class="card"><b>Dirección:</b> Av Eloy Cavazos 5100, Residencial San Eduardo, 67183 Guadalupe, N.L.</li>
			    		<li class="card"><b>Horaios:</b>
			    			<ul>Lunes a viernes: 08:30 a 21:00</ul>
			    			<ul>Sábados:         08:30 a 16:00</ul>
			    			<ul>Domingos:        08:30 a 16:00</ul>
			    		</li>
			    	</ol>
					</div>
					
				</div>

	      </div>
	      <div class="modal-footer">
	        <button class="btn btn-primary" data-bs-target="#exampleModalToggle2" data-bs-toggle="modal" data-bs-dismiss="modal">Open second modal</button>
	      </div>
	    </div>
	  </div>
	</div>

	<div id="modal7" class="modal fade " id="exampleModalToggle" aria-hidden="true" aria-labelledby="exampleModalToggleLabel" tabindex="-1">
	  <div class="modal-dialog modal-dialog-centered modal-xl">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalToggleLabel">Rómulo Garza</h5>
	        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      </div>
	      <div class="modal-body">
	      	<div class="row pl-0 pr-0 ma-0 d-flex flex-wrap align-items-center">

					<div class="col-xl-6 col-md-6 mb-4 align-items-center" onclick="abrirModalOne()">
	        	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3594.500678406604!2d-100.23471188461221!3d25.720951316562694!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8662eae6af7995ff%3A0x46c3692656c9ec39!2sAv%20Los%20%C3%81rboles%20325%2C%20Industrias%20del%20Vidrio%20Oriente%2C%2066470%20San%20Nicol%C3%A1s%20de%20los%20Garza%2C%20N.L.!5e0!3m2!1ses!2smx!4v1620414297010!5m2!1ses!2smx" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
					</div>

					<div class="col-xl-6 col-md-6 mb-4 align-items-center">
						<div class="row text-center p-2" style="margin-top: 25px; margin-bottom: 25px;">
							<h2>Información <i class="fa fa-map-marker-alt text-danger" aria-hidden="true"></i></h2>
						</div>
						<ol id="lista6">
			    		<li class="card"><b>Teléfono:</b> (81) 8883 5200</li>
			    		<li class="card"><b>Dirección:</b> Av Los Árboles 325, Industrias del Vidrio Oriente, 66470 San Nicolás de los Garza, N.L.</li>
			    		<li class="card"><b>Horaios:</b>
			    			<ul>Lunes a viernes: 09:00 a 21:00</ul>
			    			<ul>Sábados:         09:00 a 16:00</ul>
			    			<ul>Domingos:        09:00 a 16:00</ul>
			    		</li>
			    	</ol>
					</div>
					
				</div>

	      </div>
	      <div class="modal-footer">
	        <button class="btn btn-primary" data-bs-target="#exampleModalToggle2" data-bs-toggle="modal" data-bs-dismiss="modal">Open second modal</button>
	      </div>
	    </div>
	  </div>
	</div>

	<div class="container-fluid">
	  <div class="row">
	    <div class="container-sm d-flex flex-wrap align-items-center">
	      <div class="col-xl-6 col-md-6 p-2" style="text-align: justify;">
	        <h1 class="text-center text-primary" style="font-size: 32px;"><b>Metodología</b></h1>
	        <h5>La metodología de enseñanza es muy diferente a las clases que se acostumbran a ver, debido a su método único llamado <b>DAS Way</b> que significa <b>Diviertete, Aprende y Socializa</b></h5>

	        <br/>

	        <h5>El objetivo del curso es que las clases sean dinámicas haciendo que los alumnos participen al menos 18 veces por clase y que aprendan primero a hablar y después a leer y escribir, tal cual aprendieron español</h5>
	        <br/>

	        <h5>La metodlogía trabaja directamente sobre el miedo que tiene el alumno a hablar inglés</h5>
	      </div>
	    
	      <div class="col-xl-6 col-md-6 p-2">
	        <img src="public/images/metodo.jpg" width="100%" alt="">
	      </div>
	    </div>

	  </div>
	</div>



	<!-- <div id="modalPromo" class="modal fade " id="exampleModalToggle" aria-hidden="true" aria-labelledby="exampleModalToggleLabel" tabindex="-1"> -->
	  <!-- <div class="modal-dialog modal-dialog-centered modal-md"> -->
	  <div class="container">
	  	<div class="row justify-content-center d-flex flex-wrap align-items-center">
		    <div class="container-sm d-flex flex-wrap align-items-center">
		    	<div class="col-xl-6 col-md-6 p-2">
		        <img src="public/images/derecha_contacto.jpg" width="100%" alt="">
		      </div>

		      <div class="col-xl-6 col-md-6 p-2 justify-content-center d-flex flex-wrap align-items-center" style="text-align: justify;">
		      	<div class="row justify-content-center d-flex flex-wrap align-items-center">
			        <div class="modal-header">
				        <h1 class="modal-title justify-content-center d-flex flex-wrap align-items-center" id="exampleModalToggleLabel">Formulario de contacto</h1>
						    <!-- <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button> -->
				      </div>

					    <div class="formulario justify-content-center d-flex flex-wrap align-items-center">      
					    	<h3>Escríbenos y en breve los pondremos en contacto contigo</h3>	        
			          <form action="submeter-formulario.php" method="post">       
			            <p>
			              <label for="nombre" class="colocar_nombre">Nombre
			                <span class="obligatorio">*</span>
			              </label>
			              <input type="text" name="introducir_nombre" id="nombre" required="obligatorio" placeholder="Escribe tu nombre">
			            </p>
			          
			            <p>
			              <label for="email" class="colocar_email">Email
			                <span class="obligatorio">*</span>
			              </label>
			              <input type="email" name="introducir_email" id="email" required="obligatorio" placeholder="Escribe tu Email">
			            </p>
			        
			            <p>
			              <label for="telefone" class="colocar_telefono">Teléfono
			              </label>
			              <input type="tel" name="introducir_telefono" id="telefono" placeholder="Escribe tu teléfono">
			            </p>    
			          
			            <p>
			              <label for="mensaje" class="colocar_mensaje">Mensaje
			                <span class="obligatorio">*</span>
			              </label>                     
			                <textarea name="introducir_mensaje" class="texto_mensaje" id="mensaje" required="obligatorio" placeholder="Deja aquí tu comentario..."></textarea> 
			            </p>                    
			          
			            <button type="submit" name="enviar_formulario" id="enviar"><p>Enviar</p></button>

			            <p class="aviso">
			              <span class="obligatorio"> * </span>los campos son obligatorios.
			            </p>          
			            
			        	</form>
			      	</div>
			      </div>
		      </div>
		    
		      
		    </div>

		  </div>
	    <!-- <div class="modal-content"> -->
	    	
	    <!-- </div> -->
	  </div>
	  <!-- </div> -->
	<!-- </div> -->

</div>

<script>

	window.onload = function(){
		$('#modalPromo').modal('show');
	}

	abrirModalOne = function(){
		$('#modal1').modal('show');
	}

	abrirModalTwo = function(){
		$('#modal2').modal('show');
	}

	abrirModalTre = function(){
		$('#modal3').modal('show');
	}

	abrirModalFour = function(){
		$('#modal4').modal('show');
	}

	abrirModalFive = function(){
		$('#modal5').modal('show');
	}

	abrirModalSix = function(){
		$('#modal6').modal('show');
	}

	abrirModalSeven = function(){
		$('#modal7').modal('show');
	}

</script>