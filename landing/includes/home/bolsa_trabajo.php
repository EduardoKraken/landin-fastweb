<div class="container-fluid">
    <div class="row justify-content-center d-flex flex-wrap align-items-center">
        <div class="col-xl-4 col-md-6 mb-4 text-center">
            
            <img src="public/images/contactanos.jpg" alt="" width="100%">
        </div>
        <div class="col-xl-5 col-md-6 mb-4">
            <div class="card o-hidden border-0 shadow my-5">
                <div class="card-body p-5">
                    <form action="">
                        <h5 class="text-center">Envianos tus datos para comunicarnos contigo</h5>
                        <hr>
                        <div class="form-floating mb-4">
                            <input type="text" class="form-control" id="floatingNombre" placeholder="name@example.com">
                            <label for="floatingNombre">Nombre:</label>
                        </div>
                        <div class="form-floating mb-4">
                            <input type="text" class="form-control" id="floatingCelular" placeholder="Celular">
                            <label for="floatingCelular">Celular:</label>
                        </div>
                        <div class="form-floating mb-4">
                            <input type="email" class="form-control" id="floatingEmail" placeholder="Correo">
                            <label for="floatingEmail">Email:</label>
                        </div>
                        <div class="form-floating mb-4">
                            <select class="form-select" id="floatingVacante" aria-label="Vacante">
                                <option selected>-Selecciona-</option>
                                <option value="1">Recepción</option>
                                <option value="2">Teacher</option>
                                <option value="3">Ventas</option>
                            </select>
                            <label for="floatingVacante">Vacante</label>
                        </div>
                        <div class="form-floating mb-4">
                            <select class="form-select" id="floatingSucursal" aria-label="Sucursal">
                                <option selected>-Selecciona-</option>
                                <option value="1">Fresnos</option>
                                <option value="2">Anáhuac</option>
                                <option value="3">Pablo Livas</option>
                                <option value="4">Miguel Alemán</option>
                                <option value="5">Casa Blanca</option>
                                <option value="6">San Miguel</option>
                                <option value="7">Online</option>
                            </select>
                            <label for="floatingSucursal">Sucursal</label>
                        </div>
                        <div class="form-group mb-4">
                            <label for="floatingCV">Adjuntar CV:</label>
                            <input type="file" class="form-control" id="CV">
                        </div>
                        <div class="form-group mb-4">
                            <button class="btn btn-primary btn-block float-end">Enviar datos</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>