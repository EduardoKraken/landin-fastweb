<div class="container-fluid">
  <div class="row">
    <div class="container-sm d-flex flex-wrap align-items-center">
      <div class="col-xl-6 col-md-6 p-2" style="text-align: justify;">
        <h1 class="text-center text-primary" style="font-size: 32px;"><b>Metodología</b></h1>
        <h5>La metodología de enseñanza es muy diferente a las clases que se acostumbran a ver, debido a su método único llamado <b>DAS Way</b> que significa <b>Diviertete, Aprende y Socializa</b></h5>

        <br/>

        <h5>El objetivo del curso es que las clases sean dinámicas haciendo que los alumnos participen al menos 18 veces por clase y que aprendan primero a hablar y después a leer y escribir, tal cual aprendieron español</h5>
        <br/>

        <h5>La metodlogía trabaja directamente sobre el miedo que tiene el alumno a hablar inglés</h5>
      </div>
    
      <div class="col-xl-6 col-md-6 p-2">
        <img src="public/images/metodo.jpg" width="100%" alt="">
      </div>
    </div>

  </div>
</div>


<div class="container p-3">
  <div class="row d-flex flex-wrap align-items-center">
    <div class="col-xl-6 col-md-6 mb-4 text-center">
      <img src="public/images/socializa.jpg" width="100%" alt="">
    </div>

    <div class="col-xl-6 col-md-6 mb-4" >
      <h1 class="text-center text-primary" style="font-size: 32px;"><b>Socializa</b></h1>
      <h5 style="text-align: justify;">Una de las mejores formas de aprender es practicando y que mejor que hacerlos con tus compañeros de clase, aprende a entablar conversaciones con otras personas y crea enlaces de amistad con tus compañeros</h5>
    </div>
  </div>


  <div class="row d-flex flex-wrap align-items-center">
    <div class="col-xl-6 col-md-6 mb-4">
      <h1 class="text-center text-primary" style="font-size: 32px;" style="text-align: left;"><b>Aprende</b></h1>
      <h5  style="text-align: justify;">Aprende a hablar, escribir y leer el inglés de maneras divertidas y dinámicas, cada curso esta desarrollado con las mejores técnicas de aprendizaje</h5>
    </div>

    <div class="col-xl-6 col-md-6 mb-4 text-center">
      <img src="public/images/aprende.jpg" width="100%" alt="">
    </div>
  </div>
</div>