<div class="container-fluid">
    <div class="row justify-content-center d-flex flex-wrap align-items-center">
        <div class="col-xl-4 col-md-6 mb-4 text-center d-none d-sm-none d-md-block">
            <img src="public/images/contactanos.jpg" alt="" width="100%">
        </div>
        <div class="col-xl-5 col-md-6 mb-4">
            <div class="card o-hidden border-0 shadow my-5">
                <div class="card-body p-5">
                    <form action="">
                        <h5 class="text-center">Registro de referenciados</h5>
                        <hr>
                        <div class="form-floating mb-4">
                            <input type="text" class="form-control" id="floatingNombre" placeholder="Nombre">
                            <label for="floatingNombre">Nombre quien recomienda:</label>
                        </div>
                        <div class="form-floating mb-4">
                            <input type="email" class="form-control" id="floatingEmail" placeholder="Correo">
                            <label for="floatingEmail">Correo de quien recomienda:</label>
                        </div>
                        <div class="form-floating mb-4">
                            <select class="form-select" id="floatingSucursal" aria-label="Sucursal">
                                <option selected>-Selecciona-</option>
                                <option value="1">Fresnos</option>
                                <option value="2">Anáhuac</option>
                                <option value="3">Pablo Livas</option>
                                <option value="4">Miguel Alemán</option>
                                <option value="5">Casa Blanca</option>
                                <option value="6">San Miguel</option>
                                <option value="7">Online</option>
                                <option value="8">No, soy alumno</option>
                            </select>
                            <label for="floatingSucursal">Sucursal</label>
                        </div>
                        <hr>
                        <h6>Recomendado</h6>
                        <div class="form-floating mb-4">
                            <input type="text" class="form-control" id="floatingNombreRecomendado1" placeholder="Nombre Recomendado 1">
                            <label for="floatingNombreRecomendado1">Nombre Recomendado:</label>
                        </div>
                        <div class="form-floating mb-4">
                            <input type="email" class="form-control" id="floatingEmailRecomendado1" placeholder="Correo Recomendado 1">
                            <label for="floatingEmailRecomendado1">Correo Recomendado:</label>
                        </div>
                        <div class="form-floating mb-4">
                            <input type="text" class="form-control" id="floatingWhatsAppRecomendado1" placeholder="WhatsApp Recomendado 1">
                            <label for="floatingWhatsAppRecomendado1">WhatsApp Recomendado:</label>
                        </div>
                        <div class="form-group mb-4">
                            <button class="btn btn-primary btn-block float-end">Enviar datos</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>