<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require '../../vendor/autoload.php';

require '../../vendor/phpmailer/phpmailer/src/Exception.php';
require '../../vendor/phpmailer/phpmailer/src/PHPMailer.php';
require '../../vendor/phpmailer/phpmailer/src/SMTP.php';

$nombre     = $_POST['nombre'];
$celular    = $_POST['celular'];
$email      = $_POST['email'];
$vacante    = $_POST['vacante'];
$sucursal   = $_POST['sucursal'];

$date = date('d-m-Y');
$mes = "";
list($day, $month, $year) = explode('-', $date);

if($month == '01'){
  $mes = "Enero";
}

if($month == '02'){
  $mes = "Febrero";
}

if($month == '03'){
  $mes = "Marzo";
}

if($month == '04'){
  $mes = "Abril";
}

if($month == '05'){
  $mes = "Mayo";
}

if($month == '06'){
  $mes = "Junio";
}

if($month == '07'){
  $mes = "Julio";
}

if($month == '08'){
  $mes = "Agosto";
}

if($month == '09'){
  $mes = "Septiembre";
}

if($month == '10'){
  $mes = "Octubre";
}

if($month == '11'){
  $mes = "Noviembre";
}

if($month == '12'){
  $mes = "Diciembre";
}

switch ($vacante) {
  case 1:
    $vacante = "Recepción";
    break;
  case 2:
    $vacante = "Teacher";
    break;
  case 3:
    $vacante = "Ventas";
    break;  
}

switch ($sucursal) {
  case 1:
    $sucursal = "Sucursal Fresnos";
    break;
  case 2:
    $sucursal = "Sucursal Anáhuac";
    break;
  case 3:
    $sucursal = "Sucursal Pablo Livas";
    break;
  case 4:
    $sucursal = "Sucursal Miguel Alemán";
    break;
  case 5:
    $sucursal = "Sucursal Casa Blanca";
    break;
  case 6:
    $sucursal = "Sucursal San Miguel";
    break;
  case 7:
    $sucursal = "Online";
    break;
}

$fecha_final = $day . " de " . $mes . " del " . $year;


$mail = new PHPMailer(true);
//Inicializa variable a devolver
$data = array();
try {
  // SMTP Parametros.
  $mail->isSMTP();
  // SMTP Server Host. 
  $mail->Host = 'smtp.ionos.mx';
  // Use SMTP autentificacion. 
  $mail->SMTPAuth = true;
  // Introduce la incriptacion del sistema. 
  $mail->SMTPSecure = 'tls';
  // SMTP usuario. 
  $mail->Username = 'web-master@inbi.mx';
  // SMTP contraseña. 
  $mail->Password = '3s6!4zgJ';
  // Introduce el SMTP Puerto.
  $mail->Port = 587;

  // Activo condificacción utf-8
  $mail->CharSet = 'UTF-8';

  // Añade remitente.
  $mail->setFrom('web-master@inbi.mx', 'INBI - Sitio Web');

  // Añade destinatario.
  $mail->addAddress('informes@inbi.mx', 'Informes INBI-MX');
  // Asunto del correo.
  $mail->Subject = 'Postulante en Bolsa de Trabajo en inbi.mx';
  
  // Cuerpo del correo.
  $mail->isHTML(TRUE);
  $mail->Body = 
  '<html>
      <h2>Se ha recibido un nuevo postulante</h2>
      <p>Nombre: '. $nombre .'</p>
      <p>Celular: '. $celular .'</p>
      <p>Email: '. $email .'</p>
      <p>Vacante: '. $vacante  .'</p>
      <p>Sucursal: '. $sucursal .'</p>
      <p>Fecha: '. $fecha_final .' </p>
      <p><b>CV Adjuntado</b></p>
  </html>';

  // En caso de incluir un archivo
  $mail->addAttachment($_FILES['CV']['tmp_name'], $_FILES['CV']['name']);

  // Enviar correo
  $mail->send();

  $data[] = array('error' => false, 'mensaje' => "Correo enviado");
  //retornar datos
  echo json_encode($data);
} catch (Exception $e) {
  $data[] = array('error' => true, 'mensaje' => $e->errorMessage());
  echo json_encode($data);
} catch (\Exception $e) {
  $data[] = array('error' => true, 'mensaje' => $e->getMessage());
  echo json_encode($data);
}