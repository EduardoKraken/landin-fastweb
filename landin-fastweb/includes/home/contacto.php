<div class="container-fluid">
    <div class="row justify-content-center d-flex flex-wrap align-items-center">
        <div class="col-xl-4 col-md-6 mb-4 text-center">
            <h5 class="">Envianos tus datos para comunicarnos contigo</h5>
            <img src="public/images/contactanos.jpg" alt="" width="100%">
        </div>
        <div class="col-xl-5 col-md-6 mb-4">
            <div class="card o-hidden border-0 shadow my-5">
                <div class="card-body p-5">
                    <form action="">
                        <div class="form-floating mb-4">
                            <input type="text" class="form-control" id="floatingInput" placeholder="name@example.com">
                            <label for="floatingInput">Nombre:</label>
                        </div>
                        <div class="form-floating mb-4">
                            <input type="text" class="form-control" id="floatingPassword" placeholder="Celular">
                            <label for="floatingPassword">Celular:</label>
                        </div>
                        <div class="form-floating mb-4">
                            <input type="email" class="form-control" id="floatingPassword" placeholder="Correo">
                            <label for="floatingPassword">Email:</label>
                        </div>
                        <div class="form-floating mb-4">
                            <input type="text" class="form-control" id="floatingPassword" placeholder="Asunto">
                            <label for="floatingPassword">Asunto:</label>
                        </div>
                        <div class="form-floating mb-4">
                            <textarea class="form-control" placeholder="Deja tu mensaje aquí" id="floatingTextarea2" style="height: 100px"></textarea>
                            <label for="floatingTextarea2">Mensaje (opcional)</label>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary btn-block float-end">Enviar datos</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
</div>