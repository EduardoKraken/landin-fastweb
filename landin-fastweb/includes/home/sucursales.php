<div class="container-fluid">
    <div class="container">
        <div class="row">
            <div class="col-xl-12 col-md-6 mb-4 text-center">
                <h1>SUCURSALES</h1>
            </div>
        </div>
        <div class="row">
            <hr class="mb-4">
            <div class="col-xl-4 col-md-6 mb-4 text-center">
                <div class="row">
                    <div class="col-xl-12 col-md-6">
                        <h6>Sucursal Fresnos</h6>
                        <p>
                            Calle Guernica 127
                            Tel:2526-6434
                        </p>
                    </div>
                    <div class="col-xl-12 col-md-6 mb-4">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3592.967154741201!2d-100.26281218466565!3d25.771648414477955!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x866294aab03fe63f%3A0xd3dda08f56d2a7d9!2sINBI%20Apodaca%20-%20English%20School!5e0!3m2!1ses!2smx!4v1620167457768!5m2!1ses!2smx" width="100%" height="250" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6 mb-4 text-center">
                <div class="row">
                    <div class="col-xl-12 col-md-6">
                        <h6>Sucursal Anahuac</h6>
                        <p>
                            José Santos Chocano 111
                            Tel: 2473-5131
                        </p>
                    </div>
                    <div class="col-xl-12 col-md-6 mb-4">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d534.250414086413!2d-100.30384593598686!3d25.737709872294374!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x866294f633dd3dfb%3A0x87a6e7307b53000f!2sINBI%20An%C3%A1huac%20-%20English%20School!5e0!3m2!1ses!2smx!4v1620167579770!5m2!1ses!2smx" width="100%" height="250" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6 mb-4 text-center">
                <div class="row">
                    <div class="col-xl-12 col-md-6">
                        <h6>Sucursal Linda Vista</h6>
                        <p>
                            Miguel Alemán 4319
                            Tel:2233-1381
                        </p>
                    </div>
                    <div class="col-xl-12 col-md-6 mb-4">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3595.482324646743!2d-100.26172098466678!3d25.688449917897476!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8662eaad3ba4e8d7%3A0x1b5eb724c965eb32!2sINBI%20Linda%20Vista%20-%20English%20School!5e0!3m2!1ses!2smx!4v1620167610297!5m2!1ses!2smx" width="100%" height="250" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <hr class="mb-4">
            <div class="col-xl-4 col-md-6 mb-4 text-center">
                <div class="row">
                    <div class="col-xl-12 col-md-6">
                        <h6>Sucursal Pablo Livas</h6>
                        <p>
                            Av. Pablo Livas #17
                            Tel:2473-5496
                        </p>
                    </div>
                    <div class="col-xl-12 col-md-6 mb-4">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3596.233234235719!2d-100.20549908255617!3d25.66356209999999!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8662c18e34834413%3A0x5f8c98c4f56c7729!2sINBI%20Pablo%20Livas%20-%20English%20School!5e0!3m2!1ses!2smx!4v1620167830119!5m2!1ses!2smx" width="100%" height="250" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6 mb-4 text-center">
                <div class="row">
                    <div class="col-xl-12 col-md-6">
                        <h6>Sucursal Casa Blanca</h6>
                        <p>
                            Av. Casa Blanca #505
                            Tel:2473-6728
                        </p>
                    </div>
                    <div class="col-xl-12 col-md-6 mb-4">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3594.1407836028516!2d-100.24186168466619!3d25.732857516073523!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8662eb21887ef021%3A0xa8c28ee7745909cf!2sAv.%20Casa%20Blanca%20505%2C%20Jardines%20de%20Casa%20Blanca%201er%20Sector%2C%2066475%20San%20Nicol%C3%A1s%20de%20los%20Garza%2C%20N.L.!5e0!3m2!1ses!2smx!4v1620168242211!5m2!1ses!2smx" width="100%" height="250" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6 mb-4 text-center">
                <div class="row">
                    <div class="col-xl-12 col-md-6">
                        <h6>Sucursal San Miguel</h6>
                        <p>
                            Av. Acapulco #205
                            Tel:2673-9961
                        </p>
                    </div>
                    <div class="col-xl-12 col-md-6 mb-4">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3594.551949768998!2d-100.18198528466638!3d25.71925471663248!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8662eb580672bd3f%3A0x596fd7fa2a6639eb!2sINBI%20San%20Miguel%20-%20English%20School!5e0!3m2!1ses!2smx!4v1620168476600!5m2!1ses!2smx" width="100%" height="250" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>