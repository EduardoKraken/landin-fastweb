$('input[type="file"]').on('change', function () {
  var ext = $(this).val().split('.').pop();
  if ($(this).val() != '') {
    if (ext == "pdf") {
      if ($(this)[0].files[0].size > 1048576) {
        $(this).val('');
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Se solicita que el archivo no sea mayor a 1MB. Por favor verifica!',
        });
      }
    } else {
      $(this).val('');
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Solo se permiten archivos PDF',
      });
    }
  }
});


$('#btn-enviar-datos').click(function () {

  if ($('#floatingNombre').val() == "") {
    $('#floatingNombre').addClass('is-invalid');
  } else {
    $('#floatingNombre').removeClass('is-invalid');
  }

  if ($('#floatingCelular').val() == "") {
    $('#floatingCelular').addClass('is-invalid');
  } else {
    $('#floatingCelular').removeClass('is-invalid');
  }

  if ($('#floatingEmail').val() == "") {
    $('#floatingEmail').addClass('is-invalid');
  } else {
    $('#floatingEmail').removeClass('is-invalid');
  }

  if ($('#floatingVacante').val() == 0) {
    $('#floatingVacante').addClass('is-invalid');
  } else {
    $('#floatingVacante').removeClass('is-invalid');
  }

  if ($('#floatingSucursal').val() == 0) {
    $('#floatingSucursal').addClass('is-invalid');
  } else {
    $('#floatingSucursal').removeClass('is-invalid');
  }

  if ($('#CV').val() == "") {
    $('#CV').addClass('is-invalid');
  } else {
    $('#CV').removeClass('is-invalid');
  }

  if ($('#floatingNombre').val() == "" || $('#floatingCelular').val() == "" || $('#floatingEmail').val() == "" || $('#floatingVacante').val() == 0 || $('#floatingSucursal').val() == 0 || $('#CV').val() == "") {
    Swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: 'Lo sentimos no pueden ir campos vacios',
    });
    return;
  }

  var paqueteDeDatos = new FormData();
  paqueteDeDatos.append('CV', $('#CV')[0].files[0]);
  paqueteDeDatos.append('nombre', $('#floatingNombre').prop('value'));
  paqueteDeDatos.append('celular', $('#floatingCelular').prop('value'));
  paqueteDeDatos.append('email', $('#floatingEmail').prop('value'));
  paqueteDeDatos.append('vacante', $('#floatingVacante').prop('value'));
  paqueteDeDatos.append('sucursal', $('#floatingSucursal').prop('value'));

  $.ajax({
    type: 'POST',
    url: "app/operaciones/bolsa_trabajo.php",
    contentType: false,
    data: paqueteDeDatos,
    processData: false,
    cache: false,
    dataType: 'JSON',
    beforeSend: function () {
      Swal.fire({
        icon: 'info',
        title: 'Un momento',
        text: 'Estamos enviando tus datos!',
        showCancelButton: false,
        showConfirmButton: false
      });
    },
  }).done(function (response) {
    if (response[0].error) {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: response[0].mensaje,
      });
    } else {
      Swal.fire({
        icon: 'success',
        title: 'Excelente!',
        text: 'Tus datos han sido enviados con éxito, nos comunicaremos contigo lo mas pronto posible.',
      });
      $('#CV').val('');
      $('#floatingNombre').val('');
      $('#floatingCelular').val('');
      $('#floatingEmail').val('');
      $('#floatingVacante').val(0);
      $('#floatingSucursal').val(0);
    }
  });
});