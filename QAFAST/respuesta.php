<?php include('inc/conexion.php'); ?>
<!DOCTYPE html>
<html>
<head>
<title>Pago seguro</title>
</head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
<link rel="icon" type="image/png" sizes="32x32" href="https://qa.fastenglish.com.mx/favicon/favicon-96x96.png">
<body>
  <div class="container d-flex align-content-center justify-content-center" style="min-height: 100vh;">
    <div class="row align-content-center justify-content-center">
      <?php
        // Validar los campos que retornara la transacción.

        // Obtener los datos e insertarlos en una tabla de la base de datos
        // Enviar un correo con el mensaje del estatus de la transacción.
        $BNRG_TEXTO = '';
        if(isset($_POST['BNRG_FOLIO'])){
          include('inc/conexion.php');
          $BNRG_TEXTO         = $_POST['BNRG_TEXTO'];
          date_default_timezone_set('America/Monterrey');
          if($BNRG_TEXTO == 'Aprobado'){
            $fecha_registro = date("Y-m-d H:i:s");
            $BNRG_3DS_ECI       = $_POST['BNRG_3DS_ECI'];
            $tipo               = $_POST['tipo'];
            $BNRG_CODIGO_AUT    = $_POST['BNRG_CODIGO_AUT'];
            $BNRG_TEXTO         = $_POST['BNRG_TEXTO'];
            $BNRG_TIPO_CUENTA   = $_POST['BNRG_TIPO_CUENTA'];
            $BNRG_MARCA_TARJETA = $_POST['BNRG_MARCA_TARJETA'];
            $BNRG_REFERENCIA    = $_POST['BNRG_REFERENCIA'];
            $BNRG_ID_MEDIO      = $_POST['BNRG_ID_MEDIO'];
            $BNRG_3DS_XID       = $_POST['BNRG_3DS_XID'];
            $BNRG_CODIGO_PROC   = $_POST['BNRG_CODIGO_PROC'];
            $BNRG_HORA_LOCAL    = $_POST['BNRG_HORA_LOCAL'];
            $BNRG_BANCO_EMISOR  = $_POST['BNRG_BANCO_EMISOR'];
            $BNRG_CODIGO_EMISOR = $_POST['BNRG_CODIGO_EMISOR'];
            $BNRG_MONTO_TRANS   = $_POST['BNRG_MONTO_TRANS'];
            $BNRG_FECHA_LOCAL   = $_POST['BNRG_FECHA_LOCAL'];
            $BNRG_FOLIO         = $_POST['BNRG_FOLIO'];
            $BNRG_REF_CLIENTE2  = $_POST['BNRG_REF_CLIENTE2'];
            $BNRG_REF_CLIENTE1  = $_POST['BNRG_REF_CLIENTE1'];
            $BNRG_3DS_UCAF      = $_POST['BNRG_3DS_UCAF'];
          }
          // echo json_encode($_POST);
        }else{
            // echo "Proeblemas con los servicios , no se pudo resolver su transacción.<br/>";
        }

      ?>

      <?php
        if($BNRG_CODIGO_PROC == 'A'){
      ?>
        <div id="ventana">
          <div class="wrapperAlert">
            <div class="contentAlert">
              <div class="topHalf">
                <p><svg viewBox="0 0 512 512" width="100" title="check-circle">
                  <path d="M504 256c0 136.967-111.033 248-248 248S8 392.967 8 256 119.033 8 256 8s248 111.033 248 248zM227.314 387.314l184-184c6.248-6.248 6.248-16.379 0-22.627l-22.627-22.627c-6.248-6.249-16.379-6.249-22.628 0L216 308.118l-70.059-70.059c-6.248-6.248-16.379-6.248-22.628 0l-22.627 22.627c-6.248 6.248-6.248 16.379 0 22.627l104 104c6.249 6.249 16.379 6.249 22.628.001z" />
                  </svg></p>
                <h1>Pago exitoso</h1>
                <ul class="bg-bubbles">
                  <li></li>
                  <li></li>
                  <li></li>
                  <li></li>
                  <li></li>
                  <li></li>
                  <li></li>
                  <li></li>
                  <li></li>
                  <li></li>
                </ul>
              </div>
              <div class="bottomHalf">
                <div class="detalles">Folio:      <?php echo $BNRG_FOLIO ?></div>
                <div class="detalles">Referencia: <?php echo $BNRG_REF_CLIENTE2 ?></div>
                <div class="detalles">Matricula:  <?php echo $BNRG_REF_CLIENTE1 ?></div>
                <div class="detalles">Monto:      <?php echo $BNRG_MONTO_TRANS ?></div>
                <br/>
                <br/>
                <!-- Registrar en la base de datos -->
                <?php 

                  $sql = "INSERT INTO transacciones_online (
                    bnrg_folio,
                    bnrg_texto,
                    bnrg_3ds_eci,
                    bnrg_codigo_aut,
                    bnrg_tipo_cuenta,
                    bnrg_marca_tarjeta,
                    bnrg_referencia,
                    bnrg_3ds_xid,
                    bnrg_codigo_proc,
                    fecha_registro,
                    bnrg_banco_emisor,
                    bnrg_codigo_emisor,
                    bnrg_monto_trans,
                    bnrg_ref_cliente2,
                    bnrg_ref_cliente1,
                    bnrg_3ds_ucaf
                    ) VALUES (
                    '".$BNRG_FOLIO."',
                    '".$BNRG_TEXTO."',
                    '".$BNRG_3DS_ECI."',
                    '".$BNRG_CODIGO_AUT."',
                    '".$BNRG_TIPO_CUENTA."',
                    '".$BNRG_MARCA_TARJETA."',
                    '".$BNRG_REFERENCIA."',
                    '".$BNRG_3DS_XID."',
                    '".$BNRG_CODIGO_PROC."',
                    '".$fecha_registro."',
                    '".$BNRG_BANCO_EMISOR."',
                    '".$BNRG_CODIGO_EMISOR."',
                    '".$BNRG_MONTO_TRANS."',
                    '".$BNRG_REF_CLIENTE2."',
                    '".$BNRG_REF_CLIENTE1."',
                    '".$BNRG_3DS_UCAF."')";
          $sql2 = "SELECT bnrg_folio FROM transacciones_online WHERE bnrg_folio = '".$BNRG_FOLIO."' LIMIT 1";
              $resultado2 = $connCOLECTA->query($sql2);
          $dataAlumnoGrupos = $resultado2->fetch_assoc();
          if(!$dataAlumnoGrupos){
            $resultado = $connCOLECTA->query($sql);
          }
                ?>
                <p style="font-size: 26px;">¡Tu pago se a completado con éxito!</p>
                <button id="alertMO" onclick="imprimir()">DESCARGAR</button>
              </div>
            </div>        
          </div>
        </div>

      <?php
        }else{
      ?>
        <div id="ventana">
          <div class="wrapperAlert">
            <div class="contentAlert">
              <div class="topHalf" style="background-image: -webkit-linear-gradient(45deg, rgb(255, 102, 102), rgb(255, 39, 39)) !important; ">
                <p><svg aria-hidden="true" width="100" focusable="false" data-prefix="far" data-icon="times-circle" class="svg-inline--fa fa-times-circle fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm0 448c-110.5 0-200-89.5-200-200S145.5 56 256 56s200 89.5 200 200-89.5 200-200 200zm101.8-262.2L295.6 256l62.2 62.2c4.7 4.7 4.7 12.3 0 17l-22.6 22.6c-4.7 4.7-12.3 4.7-17 0L256 295.6l-62.2 62.2c-4.7 4.7-12.3 4.7-17 0l-22.6-22.6c-4.7-4.7-4.7-12.3 0-17l62.2-62.2-62.2-62.2c-4.7-4.7-4.7-12.3 0-17l22.6-22.6c4.7-4.7 12.3-4.7 17 0l62.2 62.2 62.2-62.2c4.7-4.7 12.3-4.7 17 0l22.6 22.6c4.7 4.7 4.7 12.3 0 17z"></path></svg></p>
                <h1>Pago denegado</h1>
                <ul class="bg-bubbles">
                  <li></li>
                  <li></li>
                  <li></li>
                  <li></li>
                  <li></li>
                  <li></li>
                  <li></li>
                  <li></li>
                  <li></li>
                  <li></li>
                </ul>
              </div>
              <div class="bottomHalf">
                <div class="detalles">Nota:       <?php echo $BNRG_TEXTO ?></div>
                <br/>
                <br/>
                <p style="font-size: 26px;">¡Tu pago ha sido denegado!</p>
                <button id="alertMO" onclick="imprimir()">DESCARGAR</button>
              </div>
            </div>        
          </div>
        </div>
      <?php
        }
      ?>
    </div>
  </div>
</body>
<script>
  imprimir = function(){
    window.print()
  }

  function handleBackButton(){
    isBack = (x != document._mine._a1.value);
    document._mine._a1.value=2;
    document._mine._a1.defaultValue=2;
  }
</script>
<style>
@import url('https://fonts.googleapis.com/css2?family=Khand:wght@500&display=swap');
*{
  margin:0;
  padding: 0;
  box-sizing: border-box;
}
body  { 
  display: flex;
  font-size: 14px;
  text-align: center;
  justify-content: center;
  align-items: center;
  font-family: 'Khand', sans-serif;   
}        

.wrapperAlert {
  max-width: 675px;
  height: 600px;
  overflow: hidden;
  border-radius: 12px;
  border: thin solid #ddd;           
}

.topHalf {
  width: 100%;
  color: white;
  overflow: hidden;
  min-height: 250px;
  position: relative;
  padding: 40px 0;
  background: rgb(0,0,0);
  background: -webkit-linear-gradient(45deg, #000C81, #14CAFF);
}

.topHalf p {
  margin-bottom: 30px;
}

.detalles{
  font-size: 20px;
}

svg {
  fill: white;
}
.topHalf h1 {
  font-size: 2.25rem;
  display: block;
  font-weight: 500;
  letter-spacing: 0.15rem;
  text-shadow: 0 2px rgba(128, 128, 128, 0.6);
}
        
/* Original Author of Bubbles Animation -- https://codepen.io/Lewitje/pen/BNNJjo */
.bg-bubbles{
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;            
  z-index: 1;
}

li{
  position: absolute;
  list-style: none;
  display: block;
  width: 40px;
  height: 40px;
  background-color: rgba(255, 255, 255, 0.15);/* fade(green, 75%);*/
  bottom: -160px;

  -webkit-animation: square 20s infinite;
  animation:         square 20s infinite;

  -webkit-transition-timing-function: linear;
  transition-timing-function: linear;
}
li:nth-child(1){
  left: 10%;
}   
li:nth-child(2){
  left: 20%;

  width: 80px;
  height: 80px;

  animation-delay: 2s;
  animation-duration: 17s;
}   
li:nth-child(3){
  left: 25%;
  animation-delay: 4s;
}   
li:nth-child(4){
  left: 40%;
  width: 60px;
  height: 60px;

  animation-duration: 22s;

  background-color: rgba(white, 0.3); /* fade(white, 25%); */
}   
li:nth-child(5){
  left: 70%;
}   
li:nth-child(6){
  left: 80%;
  width: 120px;
  height: 120px;

  animation-delay: 3s;
  background-color: rgba(white, 0.2); /* fade(white, 20%); */
}   
li:nth-child(7){
  left: 32%;
  width: 160px;
  height: 160px;

  animation-delay: 7s;
}   
li:nth-child(8){
  left: 55%;
  width: 20px;
  height: 20px;

  animation-delay: 15s;
  animation-duration: 40s;
}   
li:nth-child(9){
  left: 25%;
  width: 10px;
  height: 10px;

  animation-delay: 2s;
  animation-duration: 40s;
  background-color: rgba(white, 0.3); /*fade(white, 30%);*/
}   
li:nth-child(10){
  left: 90%;
  width: 160px;
  height: 160px;

  animation-delay: 11s;
}

@-webkit-keyframes square {
  0%   { transform: translateY(0); }
  100% { transform: translateY(-500px) rotate(600deg); }
}
@keyframes square {
  0%   { transform: translateY(0); }
  100% { transform: translateY(-500px) rotate(600deg); }
}

.bottomHalf {
  align-items: center;
  padding: 35px;
}
.bottomHalf p {
  font-weight: 500;
  font-size: 1.05rem;
  margin-bottom: 20px;
}

button {
  border: none;
  color: white;
  cursor: pointer;
  border-radius: 12px;            
  padding: 10px 18px;            
  background-color: #10143E;
  text-shadow: 0 1px rgba(128, 128, 128, 0.75);
}
button:hover {
  background-color: #85ddbf;
}
</style>