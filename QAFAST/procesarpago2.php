<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Página de Ejemplo Banregio 3D Secure</title>
	</head>
	<body>
		<div style="height: 110px; background-color: #ff6b00;
			background-image: url(./imagenes/logo-banregio.png);
			background-repeat: no-repeat;
	 		background-size: auto 110px; width: 100%;
			border-style: solid; border-width: 1px; border-radius: 5px;
			border-color: #c3c3c3">
		</div>
		<div style="position: absolute; margin: auto; top:
			0; right: 0; bottom: 0; left: 0; width: 300px; height:
			250px;"
		>
			<form action="https://colecto.banregio.com/tds/vistas/recepcion3ds.zul" method="post">
				<table style="width: 300px">
					<tr>
						<td align="right">
							<label>Modo transacción</label>
						</td>
						<td align="left">
							<input name="BNRG_MODO_TRANS" value="PRD"/>
						</td>
					</tr>

					<tr>
						<td align="right">
							<label>Medio de Pago</label>
						</td>
						<td>
							<input name="BNRG_ID_MEDIO" value="EEASDVNV"/>
						</td>
					</tr>

					<tr>
						<td align="right">
							<label>ID AFILIACION</label>
						</td>
						<td>
							<input name="BNRG_ID_AFILIACION" value="8588065"/>
						</td>
					</tr>

					<tr>
						<td align="right">
							<label>Tarjeta</label>
						</td>
						<td>
							<input name="BNRG_NUMERO_TARJETA"/>
						</td>
					</tr>

					<tr>
						<td align="right">
							<label>Fecha Expiracion</label>
						</td>
						<td>
							<input name="BNRG_FECHA_EXP"/>
						</td>
					</tr>

					<tr>
						<td align="right">
							<label>Código Seguridad</label>
						</td>
						<td>
							<input name="BNRG_CODIGO_SEGURIDAD"/>
						</td>
					</tr>

					<tr>
						<td align="right">
							<label>Token</label>
						</td>
						<td>
							<!-- <input name="BNRG_TOKEN"/> -->
						</td>
					</tr>

					<tr>
						<td align="right">
							<label>Monto</label>
						</td>
						<td>
							<input name="BNRG_MONTO_TRANS"/>
						</td>
					</tr>

					<tr>
						<td align="right">
							<label>Ref 1</label>
						</td>
						<td>
							<input name="BNRG_REF_CLIENTE1"/>
						</td>
					</tr>

					<tr>
						<td align="right">
							<label>URL Respuesta</label>
						</td>
						<td>
							<input name="BNRG_URL_RESPUESTA" value="https://testcolecto.banregio.com/simcom/imprimeresultado.jsp"/>
						</td>
					</tr>

					<tr>
						<td colspan="2" align="center">
							<input type="submit" value="Enviar" width="70px"/>
						</td>
					</tr>
				</table>
			</form>
		</div>
		<div style="height: 50px; width: 100%; background-color:
		#d1d3d3; position: absolute; bottom: 0px; left: 0px; borderstyle: solid; border-width: 1px; border-radius: 5px; bordercolor: #c3c3c3">
		</div>
	</body>
</html>