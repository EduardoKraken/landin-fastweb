<?php
// Validar los campos que retornara la transacción.

// Obtener los datos e insertarlos en una tabla de la base de datos
// Enviar un correo con el mensaje del estatus de la transacción.

if(isset($_POST['folio'])){
  include('inc/conexion.php');
  date_default_timezone_set('America/Monterrey');
  $fecha_registro = date("Y-m-d H:i:s");
    function limpiar($parametro){
      $parametro = str_replace('‘','',$parametro);
      $parametro = str_replace('"','',$parametro);
      $parametro = str_replace('–','',$parametro);
      $parametro = str_replace('/*','',$parametro);
      $parametro = str_replace('*/','',$parametro);
      $parametro = str_replace('xp_','',$parametro);
      $parametro = str_replace('<','',$parametro);
      $parametro = str_replace('>','',$parametro);
      $parametro = str_replace('insert','',$parametro);
      $parametro = str_replace('delete','',$parametro);
      $parametro = str_replace('update','',$parametro);
      $parametro = str_replace('select','',$parametro);
      $parametro = str_replace('drop','',$parametro);
      $parametro = str_replace('table','',$parametro);
      $parametro = str_replace("'","\'",$parametro);
      return $parametro;
    }


    $fecha = $_POST['bnrgFechaLocal'];
    $separador = explode("/", $fecha);
    $fechaArmada = $separador[2]."-".$separador[1]."-".$separador[0];
    
    $folio             = $_POST['folio'];
    $bnrgTexto         = $_POST['bnrgTexto'];
    $bnrgIdMedio       = $_POST['bnrgIdMedio'];
    $bnrgCodigoProc    = $_POST['bnrgCodigoProc'];
    $bnrgHoraLocal     = $_POST['bnrgHoraLocal'];
    //$bnrgFechaLocal    = $_POST['bnrgFechaLocal'];
    $bnrgCodigoRechazo = $_POST['bnrgCodigoRechazo'];
    $bnrgCodigoAut     = $_POST['bnrgCodigoAut'];
    $bnrgTipoCuenta    = $_POST['bnrgTipoCuenta']; // Tipo de cuenta o producto bancario. Ejemplo: Crédito
    $bnrgMarcaTarjeta  = $_POST['bnrgMarcaTarjeta']; //  Marca de la tarjeta. Ejemplo: VISA
    $bnrgBancoEmisor   = $_POST['bnrgBancoEmisor']; // Banco emisor de la tarjeta. Ejemplo: BanRegio.
    $bnrgCodigoEmisor  = $_POST['bnrgCodigoEmisor']; // Código de respuesta proporcionado por el emisor.Ejemplo: 00
    $bnrgReferencia    = $_POST['bnrgReferencia']; // eferencia única para identificar la transacción proporcionada por Colecto.
    $lote              = $_POST['lote']; // Lote asignado por el comercio para la transacción.
    $refCliente1       = $_POST['refCliente1']; // Referencia proporcionada por el cliente para la transacción.
    $refCliente2       = $_POST['refCliente2']; // Referencia proporcionada por el cliente para la transacción.
    $numeroTarjeta     = substr($_POST['numeroTarjeta'], -4, 4); // Últimos 4 dígitos de la tarjeta con la que se realizó la transacción.
    
    $hoy               = date("Y-m-d");

    $sql = "INSERT INTO transacciones_online (
        folio,
        bnrgTexto,
        bnrgIdMedio,
        bnrgCodigoProc,
        bnrgHoraLocal,
        bnrgFechaLocal,
        bnrgCodigoRechazo,
        bnrgCodigoAut,
        bnrgTipoCuenta,
        bnrgMarcaTarjeta,
        bnrgBancoEmisor,
        bnrgCodigoEmisor,
        bnrgReferencia,
        lote,
        refCliente1,
        refCliente2,
        numeroTarjeta,
        fecha_registro,
        fecha_ultimo_cambio,
        deleted) VALUES (
        '".$folio."',
        '".$bnrgTexto."',
        '".$bnrgIdMedio."',
        '".$bnrgCodigoProc."',
        '".$bnrgHoraLocal."',
        '".$fechaArmada."',
        '".$bnrgCodigoRechazo."',
        '".$bnrgCodigoAut."',
        '".$bnrgTipoCuenta."',
        '".$bnrgMarcaTarjeta."',
        '".$bnrgBancoEmisor."',
        '".$bnrgCodigoEmisor."',
        '".$bnrgReferencia."',
        '".$lote."',
        '".$refCliente1."',
        '".$refCliente2."',
        '".$numeroTarjeta."',
        '".$hoy."',
        '".$hoy."',
        0)";

      
        if($bnrgCodigoProc=="A"){
          echo "Pagina de retorno con valores de la trasancción <br/>";
          echo "Transacción exitosa";
          $subject      = "Notificación INBI transacción de Banregio.";
          $subjectCli   = "Notificación transacción exitosa INBI.";
          $tituloAdmin  = "¡Notificación de transacción exitosa por Banregio!";
          $tituloAlumno = "¡Notificación de transacción exitosa por Banregio!";

          $bodyAdmin    = "
          <h2>".$tituloAdmin."</h2>
          <p>El usuario  ".$refCliente1.", realizo una transaccion, revise el portal de Transacciones Online para validar y aplicar los pagos 
          Texto para el administrador de las transacciones en linea.
          </p>
          <br/>
          <span class=\"dato_tile\">DATOS GENERALES.</span>
          <br/>
          <table>
          <tr><td>Correo:</td><td>".$refCliente1."</td></tr>
          <tr><td>Referencia</td><td>".$refCliente2."</td></tr>
          </table>";

          $bodyAlumno   = "
          <h2>".$tituloAlumno."</h2>
          <p>Se he realizado su pago de forma exitosa. 
          Texto para el usuario que realizo el pago.
          </p>
          <table>
          <tr><td>Referencia</td><td>".$refCliente2."</td></tr>
          </table>";

        }else{
          echo "Pagina de retorno con valores de la trasancción <br/>";
          echo "Transacción declinada";
          $subject      = "Notificación INBI transacción declinada.";
          $subjectCli   = "Notificación transacción declinada INBI.";
          $tituloAdmin  = "¡Notificación de transacción declinada!";
          $tituloAlumno = "¡Notificación de transacción declinada!";
          
          $bodyAdmin    = "
          <h2>".$tituloAdmin."</h2>
          <p>Se intento hacer una transacción sin exito. 
          Texto para el administrador de las transacciones declinadas en linea.
          </p>
          <br/>
          <span class=\"dato_tile\">DATOS GENERALES.</span>
          <br/>";
          $bodyAlumno   = "<h2>".$tituloAlumno."</h2>
          <p>No se pudo realizar la transacción de su pago. Consulte con su banco
          </p>";

        }

    if ($conn->query($sql) === TRUE){
        $correo = "gerardo.flores@inbi.mx";
        $headers = "From: pago@inbi.mx \r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
      
        $message = "
        <!doctype html>
        <html lang=\"en\">
            <head>
            <title>".$subject."</title>
        <style>
            body {
            margin: 0;
            padding: 0;
            }
            h1 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 110%;
            font-weight: bold;
            color: #2E86C1;
            }
        
            h2 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 100%;
            font-weight: bold;
            color: #2E86C1 ;
            }
            p{
                color: #666;
                font-family: Arial, Helvetica, sans-serif;
            }
            .center {
            position: relative;
            margin: auto;
            padding: auto;
            text-align: center;
            }
            .contenedor {
            width: 80%;
            height: auto;
            padding: 0;
            margin: auto;
            background-color: #fff;
            }
            .centro {
            width: 200px;
            height: 30px;
            vertical-align: middle;
            }
            .asado:active {
            border: none;
            }
            .linea {
            width: 100%;
            height: 1px;
            border: 1px solid #cdcdcd;
            }
            .go {
            background-color:#FF5733;
            color:#fff;
            border:none;
            padding:10px 15px 10px 15px;
            font-family: Arial, Helvetica, sans-serif;
            }
            .dato-title {
            font-size: 100%;
            font-weight: bold;
            font-family: Arial, Helvetica, sans-serif;
            }
            .dato-description {
            font-size: 100%;
            font-weight: normal;
            font-family: Arial, Helvetica, sans-serif;
            }
        </style>
      
      </head>
      <body>
        <div class=\"contenedor\">
          <h1>INBI</h1>
            ".$bodyAdmin."
        </div>
      </body>
      </html>
      ";
      
      mail($correo, $subject, $message, $headers);

      
      $correoCli = $refCliente1;
      $headersCli = "From: pagos@inbi.mx \r\n";
      $headersCli .= "MIME-Version: 1.0\r\n";
      $headersCli .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
    
      $messageCli = "
      <!doctype html>
      <html lang=\"en\">
          <head>
          <title>".$subjectCli."</title>
      <style>
          body {
          margin: 0;
          padding: 0;
          }
          h1 {
          font-family: Arial, Helvetica, sans-serif;
          font-size: 110%;
          font-weight: bold;
          color: #2E86C1;
          }
      
          h2 {
          font-family: Arial, Helvetica, sans-serif;
          font-size: 100%;
          font-weight: bold;
          color: #2E86C1 ;
          }
          p{
              color: #666;
              font-family: Arial, Helvetica, sans-serif;
          }
          .center {
          position: relative;
          margin: auto;
          padding: auto;
          text-align: center;
          }
          .contenedor {
          width: 80%;
          height: auto;
          padding: 0;
          margin: auto;
          background-color: #fff;
          }
          .centro {
          width: 200px;
          height: 30px;
          vertical-align: middle;
          }
          .asado:active {
          border: none;
          }
          .linea {
          width: 100%;
          height: 1px;
          border: 1px solid #cdcdcd;
          }
          .go {
          background-color:#FF5733;
          color:#fff;
          border:none;
          padding:10px 15px 10px 15px;
          font-family: Arial, Helvetica, sans-serif;
          }
          .dato-title {
          font-size: 100%;
          font-weight: bold;
          font-family: Arial, Helvetica, sans-serif;
          }
          .dato-description {
          font-size: 100%;
          font-weight: normal;
          font-family: Arial, Helvetica, sans-serif;
          }
      </style>
    
    </head>
    <body>
      <div class=\"contenedor\">
        <h1>INBI</h1>
        ".$bodyAlumno."
      </div>
    </body>
    </html>
    ";
    
    mail($correoCli, $subjectCli, $messageCli, $headersCli);




    }else{
        echo "Error al hacer Insert.<br/>";
        echo " Consulta con error en la sintaxis: <br/>";
        echo $sql."<br/>";
    }

}else{
    echo "Proeblemas con los servicios , no se pudo resolver su transacción.<br/>";
}
?>