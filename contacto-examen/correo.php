<?php

$nombre          = "Alejandro Flores";
$correo          = "alejandro@gmail.com";
$telefono        = "919191919191";
$motivo_aprender = "Trabajo";
$fecha           = "2020-09-11 11:31:12";

$to      = 'isc.gerardoflores@gmail.com';
$subject = 'Registro de examen de ubicación INBI.';


$headers = "From: " . $correo. "\r\n";
$headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

$message = "
<!doctype html>
<html lang=\"en\">
  <head>
    <title>Registro de examen de ubicación.</title>
<style>
  body {
    margin: 0;
    padding: 0;
  }
  h1 {
    font-family: Arial, Helvetica, sans-serif;
    font-size: 110%;
    font-weight: bold;
    color: #2E86C1;
  }

  h2 {
    font-family: Arial, Helvetica, sans-serif;
    font-size: 100%;
    font-weight: bold;
    color: #2E86C1 ;
  }
p{
  color: #666;
  font-family: Arial, Helvetica, sans-serif;
}
  .center {
    position: relative;
    margin: auto;
    padding: auto;
    text-align: center;
  }
  .contenedor {
    width: 80%;
    height: auto;
    padding: 0;
    margin: auto;
    background-color: #fff;
  }
  .centro {
    width: 200px;
    height: 30px;
    vertical-align: middle;
  }
  .asado:active {
    border: none;
  }
  .linea {
    width: 100%;
height: 1px;
border: 1px solid #cdcdcd;
  }
  .go {
    background-color:#FF5733;
    color:#fff;
    border:none;
    padding:10px 15px 10px 15px;
    font-family: Arial, Helvetica, sans-serif;
  }
  .dato-title {
    font-size: 100%;
    font-weight: bold;
    font-family: Arial, Helvetica, sans-serif;
  }
  .dato-description {
    font-size: 100%;
    font-weight: normal;
    font-family: Arial, Helvetica, sans-serif;
  }

</style>


</head>
<body>
  <div class=\"contenedor\">
    <h1>INBI</h1>
    
    <h2>¡Informacion de la persona que contesto el examen!</h2>

    <p>".$nombre." a contestao el examen de evaluación, este correo es una notificación con la información basica, 
    si quiere consultar el registro completo haga click en el boton Ir al registro. </p>
    <br/><br/><br/>

      <table cellpadding=\"8\">
      <tr style=\'background: #eee;\'><td><span class=\"dato-title\">Name:</span> </td><td><span class=\"dato-description\">" . $nombre . "</span></td></tr>
      <tr><td><span class=\"dato-title\">Correo:</span></td><td><span class=\"dato-description\">" . $correo . "</span></td></tr>
      <tr><td><span class=\"dato-title\">Teléfono:</span> </td><td><span class=\"dato-description\">" . $telefono . "</span></td></tr>
      <tr><td><span class=\"dato-title\">Motivos de aprendizaje:</span> </td><td><span class=\"dato-description\">" . $motivo_aprender . "</span></td></tr>
      <tr><td><span class=\"dato-title\">Fecha:</span> </td><td><span class=\"dato-description\">" . $fecha . "</span></td></tr>
      </table>
      <br/><br/>
      <div class=\"center\">
        <span class=\"go\">Ir al registro</span>
      </div>

  </div>
</body>
</html>
";




mail($to, $subject, $message, $headers);
?>
