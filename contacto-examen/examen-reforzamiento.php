<!-- Black List: Buscar ip en una tabla de Black List -->
<!-- Generación de token y ID de session -->
<!-- Insertar la id y la session en la base de datos-->

<!-- Checar como esta el examen de ubicación de Grop Up -->
<!--Poner Captcha para evitar Span -->
<!--Ingresar estos datos en la base de datos -->

<!--Poder calcular el tiempo de el examen -->
<?php
require_once 'inc/conexion.php';
date_default_timezone_set('America/Monterrey');
?>
<!doctype html>
<html lang="en">
  <head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-77502498-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-77502498-1');
</script>

    <!-- Required meta tags -->
    <meta http-equiv=”Content-Type” content=”text/html; charset=UTF-8″ />
    
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="google" content="notranslate">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script type="text/script" src="js/bootstrap.min.js"></script>
    <title>Plataforma de evaluaciones FASTENGLISH</title>
<style>
body {
  border-top:4px solid #cdcdcd;
  padding-top:20px;
}
  .asado {
    width: 200px;
    height: 30px;
    vertical-align: middle;
  }
  .asado:active {
    border: none;
  }
  .pregunta {
    color:#000;
    font-style:arial;
    font-size:100%;
    font-weight:bold;

  }
  .form-group {
    margin:30px 0 30px 0;
  }

  .opcion {
    color:#6E6E6E;
    font-style:arial;
    font-size:90%;
    font-weight:bold;
    margin-right:20px;
    margin-left:5px;
  }
</style>
  </head>
  <script type="text/javascript">
  function startTime(){
    today=new Date();
    h=today.getHours();
    m=today.getMinutes();
    s=today.getSeconds();
    m=checkTime(m);
    s=checkTime(s);
    document.getElementById('reloj').innerHTML=h+":"+m+":"+s;
    t=setTimeout('startTime()',500);
    }

  function checkTime(i){
    if (i<10) {i="0" + i;}return i;
    }

  window.onload=function(){
    startTime();
    }
</script>


  <body>
    <?php
    if(!isset($_POST['entrar'])){
    ?>
      <div class="container"> <!--Inicia Container -->
        <div class="row">    
          <div class="col-md-3"><img src="img/cropped-logo-fast-png-1.png" width="155" height="131"></div>
          <div class="col-md-6"></div>
          <div class="col-md-3"><div id="reloj" style="font-size:20px;"></div> </div>
        </div>


        <div class="row">    
          <div class="col-md-2"></div>
          <div class="col-md-8">
          <h1>Examen de reforzamiento FastEnglish. </h1>
          </div>
          <div class="col-md-2"></div>
        </div>

        <div class="row">    
          <div class="col-md-2"></div>
          <div class="col-md-8">
              <div class="form-group">
              <form action="" method="POST" name="forma" id="forma">
                <label for="nombre">Nombre</label>
                  <input type="text" class="form-control" name="nombre" id="nombre" required>
              </div>

              <div class="form-group">
                  <label for="correo">Correo</label>
                  <input type="email" class="form-control" name="correo" id="correo" required>
              </div>

              <div class="form-group">
                  <label for="telefono">Código</label>
                  <input type="text" class="form-control" name="codigo" id="codigo">
              </div>

              
              <div class="form-group">
                        <label for="exampleFormControlSelect1">¿A que facultad desea ingresar?</label>
                        <select class="form-control" name="facultad" id="facultad" required>
                          <option>Seleccione una opción</option>
                          <option value="1">Agronomía</option>
                          <option value="2">Arquitectura</option>
                          <option value="3">Artes Escénicas</option>
                          <option value="4">Artes Visuales</option>
                          <option value="5">Ciencias Biológicas</option>

                          <option value="6">Ciencias de la Tierra</option>
                          <option value="7">Ciencias Físico - Matematicas</option>
                          <option value="8">Ciencias Forestales</option>
                          <option value="9">Ciencias Políticas y Relaciones Internacionales</option>
                          <option value="10">Ciencias Químicas</option>
                          <option value="11">Contaduría Pública y Administración</option>
                          <option value="12">Derecho y Criminología</option>
                          <option value="13">Economía</option>
                          <option value="14">Enfermería</option>

                          <option value="15">Filosofía y Letras</option>
                          <option value="16">Ingenieria Civil</option>
                          <option value="17">Ingeniería Mecánica y Eléctrica</option>
                          <option value="18">Medicina</option>
                          <option value="19">Medicina Veterinaria y Zootecnia</option>
                          <option value="20">Música</option>
                          <option value="21">Odontología</option>                          
                          <option value="22">Organización Deportiva</option>
                          <option value="23">Psicología</option>
                          <option value="24">Salud Pública y Nutrición</option>
                          <option value="25">Trabajo Social y Desarrollo Humano</option>
                        </select>
              </div>

              <div class="form-group">
                  <label for="carrera">Carrera</label>
                  <input type="text" class="form-control" name="carrera" id="carrera" required>
              </div>

          </div>
          <div class="col-md-2"></div>
        </div>

        <div class="row">    
         
          <div class="col-md-2"></div>       
            <div class="col-md-8">
                
                    <div class="form-group">
                    <p class="pregunta">1.- Watch Out! She _________ to hit you?</p>
                    <input type="radio" name="uno" id="uno_a" value="A" required> <span class="opcion">A) Is going to </span>
                    <input type="radio" name="uno" id="uno_b" value="B"> <span class="opcion">B) Going to </span>
                    <input type="radio" name="uno" id="uno_c" value="C"> <span class="opcion">C) Are going to </span>
                    <input type="radio" name="uno" id="uno_d" value="D"> <span class="opcion">D) Would  </span>
                    </div>
                    <br/>

                    <div class="form-group">
                    <p class="pregunta">2.- ___________ very nice cars. I love them.</p>
                    <input type="radio" name="dos" id="dos_a" value="A" required><span class="opcion">A) It`s </span>
                    <input type="radio" name="dos" id="dos_b" value="B"><span class="opcion">B) They are </span>
                    <input type="radio" name="dos" id="dos_c" value="C"><span class="opcion">C) It  </span>
                    <input type="radio" name="dos" id="dos_d" value="D"><span class="opcion">D) There are </span>
                    </div>
                    <br/>
                    


                    <div class="form-group">
                    <p class="pregunta">3.- My mom ________ me go to the party</p>
                    <input type="radio" name="tres" id="tres_a" value="A" required><span class="opcion">A)  offered </span>
                    <input type="radio" name="tres" id="tres_b" value="B"><span class="opcion">B)  let </span>
                    <input type="radio" name="tres" id="tres_c" value="C"><span class="opcion">C)  said </span>
                    <input type="radio" name="tres" id="tres_d" value="D"><span class="opcion">D)  asked </span>
                    </div>
                    <br/>
                    

                    <div class="form-group">
                    <p class="pregunta">4.- Carl is my best friend of the world. _____ will always be with me.</p>
                    <input type="radio" name="cuatro" id="cuatro_a" value="A" required><span class="opcion">A)  his </span>
                    <input type="radio" name="cuatro" id="cuatro_b" value="B"><span class="opcion">B)  him </span>
                    <input type="radio" name="cuatro" id="cuatro_c" value="C"><span class="opcion">C)  he </span>
                    <input type="radio" name="cuatro" id="cuatro_d" value="D"><span class="opcion">D)  she </span>
                    </div>
                    <br/>



                    <div class="form-group">
                    <p class="pregunta">5.- I remember when I went to New York. It was an amazing trip _____ 2016. </p>
                    <input type="radio" name="cinco" id="cinco_a" value="A" required><span class="opcion">A)  of </span>
                    <input type="radio" name="cinco" id="cinco_b" value="B"><span class="opcion">B)  on </span>
                    <input type="radio" name="cinco" id="cinco_c" value="C"><span class="opcion">C)  in </span>
                    <input type="radio" name="cinco" id="cinco_d" value="D"><span class="opcion">D)  for </span>
                    </div>
                    <br/>


                    <div class="form-group">
                    <p class="pregunta">6.- How old are you?</p>
                    <input type="radio" name="seis" id="seis_a" value="A" required><span class="opcion">A)  I am so sad </span>
                    <input type="radio" name="seis" id="seis_b" value="B"><span class="opcion">B)  I have 21 years old </span>
                    <input type="radio" name="seis" id="seis_c" value="C"><span class="opcion">C)  I am 21 years old </span>
                    <input type="radio" name="seis" id="seis_d" value="D"><span class="opcion">D)  I have 21 years </span>
                    </div>
                    <br/>


                    <div class="form-group">
                    <p class="pregunta">7.- Your cellphone is amazing, can you call ______?</p>
                    <input type="radio" name="siete" id="siete_a" value="A" required><span class="opcion">A)  she </span>
                    <input type="radio" name="siete" id="siete_b" value="B"><span class="opcion">B)  he </span>
                    <input type="radio" name="siete" id="siete_c" value="C"><span class="opcion">C)  I </span>
                    <input type="radio" name="siete" id="siete_d" value="D"><span class="opcion">D)  her </span>
                    </div>
                    <br/>


                    <div class="form-group">
                    <p class="pregunta">8.- Cristiano Ronaldo is _________ Messi. </p>
                    <input type="radio" name="ocho" id="ocho_a" value="A"  required><span class="opcion">A)  better </span>
                    <input type="radio" name="ocho" id="ocho_b" value="B"><span class="opcion">B)  worse than </span>
                    <input type="radio" name="ocho" id="ocho_c" value="C"><span class="opcion">C)  Better that </span>
                    <input type="radio" name="ocho" id="ocho_d" value="D"><span class="opcion">D)  worse </span>
                    </div>
                    <br/>



                    <div class="form-group">
                    <p class="pregunta">9.- If only she ________ to the party, I would be so happy.</p>
                    <input type="radio" name="nueve" id="nueve_a" value="A" required><span class="opcion">A)  go </span>
                    <input type="radio" name="nueve" id="nueve_b" value="B"><span class="opcion">B)  going </span>
                    <input type="radio" name="nueve" id="nueve_c" value="C"><span class="opcion">C)  went </span>
                    <input type="radio" name="nueve" id="nueve_d" value="D"><span class="opcion">D)  gone </span>
                    </div>
                    <br/>


                    
                    <div class="form-group">
                    <p class="pregunta">10.- My mom __________ to prepare that dinner. She is really tired because she worked all night long. </p>
                    <input type="radio" name="diez" id="diez_a" value="A" required><span class="opcion">A)  Isn`t going </span>
                    <input type="radio" name="diez" id="diez_b" value="B"><span class="opcion">B)  going </span>
                    <input type="radio" name="diez" id="diez_c" value="C"><span class="opcion">C)  can`t preparing </span>
                    <input type="radio" name="diez" id="diez_d" value="D"><span class="opcion">D)  don`t </span>
                    </div>
                    <br/>


                    <div class="form-group">
                    <p class="pregunta">11.- Rudolph will study French, ______?</p>
                    <input type="radio" name="once" id="once_a" value="A" required><span class="opcion">A)  Will he </span>
                    <input type="radio" name="once" id="once_b" value="B"><span class="opcion">B)  Willn`t he </span>
                    <input type="radio" name="once" id="once_c" value="C"><span class="opcion">C)  Won`t he </span>
                    <input type="radio" name="once" id="once_d" value="D"><span class="opcion">D)  Will Rudolph </span>
                    </div>
                    <br/>

            
                    <div class="form-group">
                    <p class="pregunta">12.- I am fed up _________ exercising everyday </p>
                    <input type="radio" name="doce" id="doce_a" value="A" required><span class="opcion">A)  For </span>
                    <input type="radio" name="doce" id="doce_b" value="B"><span class="opcion">B)  To </span>
                    <input type="radio" name="doce" id="doce_c" value="C"><span class="opcion">C)  About </span>
                    <input type="radio" name="doce" id="doce_d" value="D"><span class="opcion">D)  With </span>
                    </div>
                    <br/>


                    <div class="form-group">
                    <p class="pregunta">13.- Your boyfriend is really handsome, ________ is not. </p>
                    <input type="radio" name="trece" id="trece_a" value="A" required><span class="opcion">A)  my </span>
                    <input type="radio" name="trece" id="trece_b" value="B"><span class="opcion">B)  him </span>
                    <input type="radio" name="trece" id="trece_c" value="C"><span class="opcion">C)  mine </span>
                    <input type="radio" name="trece" id="trece_d" value="D"><span class="opcion">D)  your </span>
                    </div>
                    <br/>


                    <div class="form-group">
                    <p class="pregunta">14.- _____ are you going to? </p>
                    <input type="radio" name="catorce" id="catorce_a" value="A" required><span class="opcion">A)  Where </span>
                    <input type="radio" name="catorce" id="catorce_b" value="B"><span class="opcion">B)  Whose </span>
                    <input type="radio" name="catorce" id="catorce_c" value="C"><span class="opcion">C)  How </span>
                    <input type="radio" name="catorce" id="catorce_d" value="D"><span class="opcion">D)  Who </span>
                    </div>
                    <br/>


                    <div class="form-group">
                    <p class="pregunta">15.- I _____________ the U.S in my whole life. </p>
                    <input type="radio" name="quince" id="quince_a" value="A" required><span class="opcion">A)  Have never visited </span>
                    <input type="radio" name="quince" id="quince_b" value="B"><span class="opcion">B)  Has never visited </span>
                    <input type="radio" name="quince" id="quince_c" value="C"><span class="opcion">C)  am never visited </span>
                    <input type="radio" name="quince" id="quince_d" value="D"><span class="opcion">D)  Am going to visited </span>
                    </div>
                    <br/>


                    <div class="form-group">
                    <p class="pregunta">16.- My dad is so crazy. He  __________ right now and he is actually working. </p>
                    <input type="radio" name="dieciseis" id="dieciseis_a" value="A" required><span class="opcion">A)  sleeps </span>
                    <input type="radio" name="dieciseis" id="dieciseis_b" value="B"><span class="opcion">B)  slept </span>
                    <input type="radio" name="dieciseis" id="dieciseis_c" value="C"><span class="opcion">C)  Is sleeping </span>
                    <input type="radio" name="dieciseis" id="dieciseis_d" value="D"><span class="opcion">D)  Have slept </span>
                    </div>
                    <br/>

  
                    <div class="form-group">
                    <p class="pregunta">17.- Everest is ______________ mountain of the world.</p>
                    <input type="radio" name="diecisiete" id="diecisiete_a" value="A" required><span class="opcion">A)  The taller  </span>
                    <input type="radio" name="diecisiete" id="diecisiete_b" value="B"><span class="opcion">B)  The most tallest </span>
                    <input type="radio" name="diecisiete" id="diecisiete_c" value="C"><span class="opcion">C)  Tallest than </span>
                    <input type="radio" name="diecisiete" id="diecisiete_d" value="D"><span class="opcion">D)  The tallest </span>
                    </div>
                    <br/>



                    <div class="form-group">
                    <p class="pregunta">18.- Do you know if a Ferrari is ___________ an airplane? </p>
                    <input type="radio" name="dieciocho" id="dieciocho_a" value="A" required><span class="opcion">A)  Faster that </span>
                    <input type="radio" name="dieciocho" id="dieciocho_b" value="B"><span class="opcion">B)  More fast </span>
                    <input type="radio" name="dieciocho" id="dieciocho_c" value="C"><span class="opcion">C)  faster </span>
                    <input type="radio" name="dieciocho" id="dieciocho_d" value="D"><span class="opcion">D)  Faster than </span>
                    </div>
                    <br/>


                    <div class="form-group">
                    <p class="pregunta">19.- If you had 1 million dollars, what ______ you buy? </p>
                    <input type="radio" name="diecinueve" id="diecinueve_a" value="A" required><span class="opcion">A)  would </span>
                    <input type="radio" name="diecinueve" id="diecinueve_b" value="B"><span class="opcion">B)  had </span>
                    <input type="radio" name="diecinueve" id="diecinueve_c" value="C"><span class="opcion">C)  should </span>
                    <input type="radio" name="diecinueve" id="diecinueve_d" value="D"><span class="opcion">D)  did  </span>
                    </div>
                    <br/>



                    <div class="form-group">
                    <p class="pregunta">20.- What did you eat last week?</p>
                    <input type="radio" name="veinte" id="veinte_a" value="A" required><span class="opcion">A)  I eat some pizza </span>
                    <input type="radio" name="veinte" id="veinte_b" value="B"><span class="opcion">B)  I eating some pizza </span>
                    <input type="radio" name="veinte" id="veinte_c" value="C"><span class="opcion">C)  I eaten some pizza </span>
                    <input type="radio" name="veinte" id="veinte_d" value="D"><span class="opcion">D)  I ate some pizza </span>
                    </div>
                    <br/>

                    <br/><br/>
                  <div class="text-center">
                    <button type="submit" name="entrar" class="btn btn-primary">Finalizar Evaluación</button>
                    
                  </div>
                  </form>
                  <br/><br/><br/><br/>

            </div>
          <div class="col-md-2"></div>
        </div>
        <!-- Termina el formulario-->
        </div><!--Termima container-->
  <?php  
  }
  else
  {
          function limpiar($parametro){

          $parametro = str_replace('‘','',$parametro);
          $parametro = str_replace('"','',$parametro);
          $parametro = str_replace('–','',$parametro);
          $parametro = str_replace('/*','',$parametro);
          $parametro = str_replace('*/','',$parametro);
          $parametro = str_replace('xp_','',$parametro);
          $parametro = str_replace('<','',$parametro);
          $parametro = str_replace('>','',$parametro);
          $parametro = str_replace('insert','',$parametro);
          $parametro = str_replace('delete','',$parametro);
          $parametro = str_replace('update','',$parametro);
          $parametro = str_replace('select','',$parametro);
          $parametro = str_replace('drop','',$parametro);
          $parametro = str_replace('table','',$parametro);

          return $parametro;
          }

        $nombre           = limpiar($_POST['nombre']);
        $correo           = limpiar($_POST['correo']); // limpiar espacios en blanco
        if(empty($_POST['codigo'])){
          $codigo = "SEGUNDAEVAL";
        }else{
          $codigo           = limpiar($_POST['codigo']); // limpiar espacios en blanco
        }
        
        $facultad         = limpiar($_POST['facultad']);
        $carrera          = limpiar($_POST['carrera']);

        $fecha_registro   = date("Y-m-d H:i:s");

        $puntuacion       = 0;
        $puntuacionGlobal = 0;
        // 1.	A
        $respuesta1      = limpiar($_POST['uno']);
        if($respuesta1=="A"){
          $puntuacionUno = 5;
          $respuestaCorrecta1 = "si";
          $puntuacionGlobal = $puntuacionGlobal + 5; 
        }else{
          $puntuacionUno = 0;
          $respuestaCorrecta1 = "no";
        }


        // 2.	B
        $respuesta2      = limpiar($_POST['dos']);
        if($respuesta2=="B"){
          $puntuacionDos = 5;
          $respuestaCorrecta2 = "si";
          $puntuacionGlobal = $puntuacionGlobal + 5;
        }else{
          $puntuacionDos = 0;
          $respuestaCorrecta2 = "no";
        }


        // 3.	B
        $respuesta3      = limpiar($_POST['tres']);
        if($respuesta3=="B"){
          $puntuacionTres = 5;
          $respuestaCorrecta3 = "si";
          $puntuacionGlobal = $puntuacionGlobal + 5;
        }else{
          $puntuacionTres = 0;
          $respuestaCorrecta3 = "no";
        }


        //4.	C
        $respuesta4      = limpiar($_POST['cuatro']);
        if($respuesta4=="C"){
          $puntuacionCuatro = 5;
          $respuestaCorrecta4 = "si";
          $puntuacionGlobal = $puntuacionGlobal + 5;
        }else{
          $puntuacionCuatro = 0;
          $respuestaCorrecta4 = "no";
        }


        //5.	C
        $respuesta5      = limpiar($_POST['cinco']);
        if($respuesta5=="C"){
          $puntuacionCinco =  5;
          $respuestaCorrecta5 = "si";
          $puntuacionGlobal = $puntuacionGlobal + 5;
        }else{
          $puntuacionCinco = 0;
          $respuestaCorrecta5 = "no";
        }


        //6.	C
        $respuesta6      = limpiar($_POST['seis']);
        if($respuesta6=="C"){
          $puntuacionSeis = 5;
          $respuestaCorrecta6 = "si";
          $puntuacionGlobal = $puntuacionGlobal + 5;
        }else{
          $puntuacionSeis = 0;
          $respuestaCorrecta6 = "no";
        }


        //7.	D
        $respuesta7      = limpiar($_POST['siete']);
        if($respuesta7=="D"){
          $puntuacionSiete = 5;
          $respuestaCorrecta7 = "si";
          $puntuacionGlobal = $puntuacionGlobal + 5;
        }else{
          $puntuacionSiete = 0;
          $respuestaCorrecta7 = "no";
        }


        //8.	B
        $respuesta8      = limpiar($_POST['ocho']);
        if($respuesta8=="B"){
          $puntuacionOcho = 5;
          $respuestaCorrecta8 = "si";
          $puntuacionGlobal = $puntuacionGlobal + 5;
        }else{
          $puntuacionOcho = 0;
          $respuestaCorrecta8 = "no";
        }



        //9.	C
        $respuesta9      = limpiar($_POST['nueve']);
        if($respuesta9=="C"){
          $puntuacionNueve = 5;
          $respuestaCorrecta9 = "si";
          $puntuacionGlobal = $puntuacionGlobal + 5;
        }else{
          $puntuacionNueve = 0;
          $respuestaCorrecta9 = "no";
        }


        //10.	A
        $respuesta10      = limpiar($_POST['diez']);
        if($respuesta10=="A"){
          $puntuacionDiez = 5;
          $respuestaCorrecta10 = "si";
          $puntuacionGlobal = $puntuacionGlobal + 5;
        }else{
          $puntuacionDiez = 0;
          $respuestaCorrecta10 = "no";
        }


        //11.	C
        $respuesta11      = limpiar($_POST['once']);
        if($respuesta11=="C"){
          $puntuacionOnce = 5;
          $respuestaCorrecta11 = "si";
          $puntuacionGlobal = $puntuacionGlobal + 5;
        }else{
          $puntuacionOnce = 0;
          $respuestaCorrecta11 = "no";
        }


        //12.	D
        $respuesta12      = limpiar($_POST['doce']);
        if($respuesta12=="D"){
          $puntuacionDoce = 5;
          $respuestaCorrecta12 = "si";
          $puntuacionGlobal = $puntuacionGlobal + 5;
        }else{
          $puntuacionDoce = 0;
          $respuestaCorrecta12 = "no";
        }




        //13.	C
        $respuesta13      = limpiar($_POST['trece']);
        if($respuesta13=="C"){
          $puntuacionTrece = 5;
          $respuestaCorrecta13 = "si";
          $puntuacionGlobal = $puntuacionGlobal + 5;
        }else{
          $puntuacionTrece = 0;
          $respuestaCorrecta13 = "no";
        }


        //14.	A
        $respuesta14      = limpiar($_POST['catorce']);
        if($respuesta14=="A"){
          $puntuacionCatorce = 5;
          $respuestaCorrecta14 = "si";
          $puntuacionGlobal = $puntuacionGlobal + 5;
        }else{
          $puntuacionCatorce = 0;
          $respuestaCorrecta14 = "no";
        }



        //15.	A
        $respuesta15      = limpiar($_POST['quince']);
        if($respuesta15=="A"){
          $puntuacionQuince = 5;
          $respuestaCorrecta15 = "si";
          $puntuacionGlobal = $puntuacionGlobal + 5;
        }else{
          $puntuacionQuince = 0;
          $respuestaCorrecta15 = "no";
        }



        //16.	C
        $respuesta16      = limpiar($_POST['dieciseis']);
        if($respuesta16=="C"){
          $puntuacionDieciseis = 5;
          $respuestaCorrecta16 = "si";
          $puntuacionGlobal = $puntuacionGlobal + 5;
        }else{
          $puntuacionDieciseis = 0;
          $respuestaCorrecta16 = "no";
        }


        //17.	D
        $respuesta17      = limpiar($_POST['diecisiete']);
        if($respuesta17=="D"){
          $puntuacionDiecisiete = 5;
          $respuestaCorrecta17 = "si";
          $puntuacionGlobal = $puntuacionGlobal + 5;
        }else{
          $puntuacionDiecisiete = 0;
          $respuestaCorrecta17 = "no";
        }



        //18.	D
        $respuesta18      = limpiar($_POST['dieciocho']);
        if($respuesta18=="D"){
          $puntuacionDieciocho = 5;
          $respuestaCorrecta18 = "si";
          $puntuacionGlobal = $puntuacionGlobal + 5;
        }else{
          $puntuacionDieciocho = 0;
          $respuestaCorrecta18 = "no";
        }

        // dieciseis
        // diecisiete
        // dieciocho
        // diecinueve

        //19.	A
        $respuesta19      = limpiar($_POST['diecinueve']);
        if($respuesta19=="A"){
          $puntuacionDiecinueve = 5;
          $respuestaCorrecta19 = "si";
          $puntuacionGlobal = $puntuacionGlobal + 5;
        }else{
          $puntuacionDiecinueve = 0;
          $respuestaCorrecta19 = "no";
        }



        //20.	D
        $respuesta20     = limpiar($_POST['veinte']);
        if($respuesta20=="D"){
          $puntuacionVeinte = 5;
          $respuestaCorrecta20 = "si";
          $puntuacionGlobal = $puntuacionGlobal + 5;
        }else{
          $puntuacionVeinte = 0;
          $respuestaCorrecta20 = "no";
        }
    

//$conn = new mysqli($hogar, $userHogar, $pasqui,$bd);
//if (mysqli_connect_errno()) {
    //echo "error al conectar la base de datos";
    //exit();
//}
// Primero buscar que no exista el correo con el codigo, si es asi, poner un mensaje de que el examen ya fue contestado
$sqlExiste = "select correo,codigo from respuestas_examen where correo='".$correo."' and codigo ='".$codigo."';";
$ejecQuery = $conn->query($sqlExiste);
$row_cnt = $ejecQuery->num_rows;


    if($row_cnt>0){
      $existeRegistro=1;
    }else{
        $sqlInsertRespuestas = "INSERT INTO respuestas_examen (
        nombre,
        correo,
        codigo,
        facultad,
        carrera,
        fecha_registro,
        puntuacion_global,
        respuesta_1,
        puntuacion_1,
        respuesta_correcta_1,
        respuesta_2,
        puntuacion_2,
        respuesta_correcta_2,
        respuesta_3,
        puntuacion_3,
        respuesta_correcta_3,
        respuesta_4,
        puntuacion_4,
        respuesta_correcta_4,
        respuesta_5,
        puntuacion_5,
        respuesta_correcta_5,
        respuesta_6,
        puntuacion_6,
        respuesta_correcta_6,
        respuesta_7,
        puntuacion_7,
        respuesta_correcta_7,
        respuesta_8,
        puntuacion_8,
        respuesta_correcta_8,
        respuesta_9,
        puntuacion_9,
        respuesta_correcta_9,
        respuesta_10,
        puntuacion_10,
        respuesta_correcta_10,
        respuesta_11,
        puntuacion_11,
        respuesta_correcta_11,
        respuesta_12,
        puntuacion_12,
        respuesta_correcta_12,
        respuesta_13,
        puntuacion_13,
        respuesta_correcta_13,
        respuesta_14,
        puntuacion_14,
        respuesta_correcta_14,
        respuesta_15,
        puntuacion_15,
        respuesta_correcta_15,
        respuesta_16,
        puntuacion_16,
        respuesta_correcta_16,
        respuesta_17,
        puntuacion_17,
        respuesta_correcta_17,
        respuesta_18,
        puntuacion_18,
        respuesta_correcta_18,
        respuesta_19,
        puntuacion_19,
        respuesta_correcta_19,
        respuesta_20,
        puntuacion_20,
        respuesta_correcta_20)
        VALUES(
          '".$nombre."',
          '".$correo."',
          '".$codigo."',
          $facultad,
          '".$carrera."',
          '".$fecha_registro."',
          '".$puntuacionGlobal."',
          '".$respuesta1."',
          $puntuacionUno,
          '".$respuestaCorrecta1."',
          '".$respuesta2."',
          $puntuacionDos,
          '".$respuestaCorrecta2."',
          '".$respuesta3."',
          $puntuacionTres,
          '".$respuestaCorrecta3."',
          '".$respuesta4."',
          $puntuacionCuatro,
          '".$respuestaCorrecta4."',
          '".$respuesta5."',
          $puntuacionCinco,
          '".$respuestaCorrecta5."',
          '".$respuesta6."',
          $puntuacionSeis,
          '".$respuestaCorrecta6."',
          '".$respuesta7."',
          $puntuacionSiete,
          '".$respuestaCorrecta7."',
          '".$respuesta8."',
          $puntuacionOcho,
          '".$respuestaCorrecta8."',
          '".$respuesta9."',
          $puntuacionNueve,
          '".$respuestaCorrecta9."',
          '".$respuesta10."',
          $puntuacionDiez,
          '".$respuestaCorrecta10."',
          '".$respuesta11."',
          $puntuacionOnce,
          '".$respuestaCorrecta11."',
          '".$respuesta12."',
          $puntuacionDoce,
          '".$respuestaCorrecta12."',
          '".$respuesta13."',
          $puntuacionTrece,
          '".$respuestaCorrecta13."',
          '".$respuesta14."',
          $puntuacionCatorce,
          '".$respuestaCorrecta14."',
          '".$respuesta15."',
          $puntuacionQuince,
          '".$respuestaCorrecta15."',
          '".$respuesta16."',
          $puntuacionDieciseis,
          '".$respuestaCorrecta16."',
          '".$respuesta17."',
          $puntuacionDiecisiete,
          '".$respuestaCorrecta17."',
          '".$respuesta18."',
          $puntuacionDieciocho,
          '".$respuestaCorrecta18."',
          '".$respuesta19."',
          $puntuacionDiecinueve,
          '".$respuestaCorrecta19."',
          '".$respuesta20."',
          $puntuacionVeinte,
          '".$respuestaCorrecta20."')";

          // dieciseis
          // diecisiete
          // dieciocho
          // diecinueve


            if ($conn->query($sqlInsertRespuestas) === TRUE) {
              //Obtener el nombre de la facultad
              $sqlFacultad = "select nombre from facultades where id=$facultad";
              $qry = $conn->query($sqlFacultad);
              $data = $qry->fetch_assoc();
              $nombreFacultad = $data['nombre'];
              
              //if (!$resultado = $conn->query($sql)) {
              //}
              $conn->close(); 

              


              if($puntuacionGlobal <= 25){ // 0 a 25
                $mensaje  = "Hola <span class='negrita'> $nombre </span> te llamamos de parte de Fast English para brindarte tu puntaje del examen de 
                reforzamiento de inglés. La puntuación obtenida fue de <span class='negrita'> $puntuacionGlobal </span> puntos, la cual es muy baja para el apartado de inglés. 
                La proyección de puntaje basada en este examen nos indica que usted dejaría de obtener 80 puntos o más en el examen de admisión 
                lo cual nos pone en una situación comprometida ya que no tendríamos el colchón en dado caso que cometamos errores en otras 
                materias. Es evidente que se tienen algunos problemas con la estructura del idioma, pero hasta cierto punto es normal ya que 
                la mayoría de los alumnos que intentaran ingresar a la facultad no dominan el idioma al 100%. Aquí lo importante es saber 
                cómo lo vamos a mejorar para poder ayudarte a que no se queden tantos puntos sin obtener en el examen de admisión y 
                puedas tener esa ventaja a tu favor. Recordemos que los más importantes en el examen son los apartados de español y 
                matemáticas, sin embargo, siempre es bueno tener puntos a nuestro favor de todas las materias. 
                Aquí lo más recomendable es repasar las estructuras que viste en tu clase de inglés en las asesorías y 
                también ver el video que se subirá a la cuenta de Instagram de Fast English, donde el maestro les explicara 
                cómo se debió contestar el examen y por qué. Ahorita tenemos 2 situaciones que debemos resolver, 
                la más urgente es obtener el conocimiento necesario para obtener la mayor cantidad de puntos en el examen de admisión, 
                para eso será muy importante que no te pierdas el video explicativo y hagas los demás exámenes que se subirán a la misma 
                plataforma, solo debes estar pendiente de tu correo ya que ahí se enviaran las fechas y enlaces para practicar. 
                El otro problema que es aún más importante es que ya vas a ingresar a la facultad que 
                elegiste que es <span class='negrita'> $nombreFacultad </span> donde los maestros te van a pedir que investigues y 
                algunas veces los libros están en inglés, afortunadamente Fast English tiene una beca para ti por estudiar en 
                CEAA asesorías la cual consta de: (Aquí explicamos la beca) Al tomar esta beca estaremos resolviendo el problema de 
                raíz ya que además de familiarizarte con el inglés para el examen de admisión también comenzaras a trabajar en tu futuro y 
                lograras que tu paso por la facultad sea más exitoso ya que podrás investigar y hacer todos los trabajos que te pidan en 
                inglés sin ningún problema. ¿Te gustaría tomar la beca?";
              }

              if($puntuacionGlobal >= 26 and $puntuacionGlobal<=50){ // 26 a 50
                $mensaje  = "Hola <span class='negrita'> $nombre </span> te llamamos de parte de Fast English para brindarte tu puntaje del examen de 
                reforzamiento de inglés. La puntuación obtenida fue de <span class='negrita'> $puntuacionGlobal </span> puntos, la cual demuestra que tienes algunos conocimientos 
                del idioma sin embargo son pocos. La proyección de puntaje basada en este examen nos indica que usted dejaría de obtener 
                50 puntos o más en el examen de admisión lo cual nos pone en una situación comprometida ya que no tendríamos el colchón en 
                dado caso que cometamos errores en otras materias. Es evidente que se tienen algunos problemas con la estructura del idioma, 
                pero hasta cierto punto es normal ya que la mayoría de los alumnos que intentaran ingresar a la facultad no dominan el idioma 
                al 100%. Aquí lo importante es saber cómo lo vamos a mejorar para poder ayudarte a que no se queden tantos puntos sin obtener 
                en el examen de admisión y puedas tener esa ventaja a tu favor. Recordemos que los más importantes en el examen son los 
                apartados de español y matemáticas, sin embargo, siempre es bueno tener puntos a nuestro favor de todas las materias. 
                Aquí lo más recomendable es repasar las estructuras que viste en tu clase de inglés en las asesorías y también ver el 
                video que se subirá a la cuenta de Instagram de Fast English, donde el maestro les explicara cómo se debió contestar 
                el examen y por qué. Ahorita tenemos 2 situaciones que debemos resolver, la más urgente es obtener el conocimiento 
                necesario para obtener la mayor cantidad de puntos en el examen de admisión, para eso será muy importante que no te 
                pierdas el video explicativo y hagas los demás exámenes que se subirán a la misma plataforma, solo debes estar pendiente 
                de tu correo ya que ahí se enviaran las fechas y enlaces para practicar. El otro problema que es aún más importante es 
                que ya vas a ingresar a la facultad que elegiste que es <span class='negrita'> $nombreFacultad </span> donde los maestros te van a pedir 
                que investigues y algunas veces los libros están en inglés, afortunadamente Fast English tiene una beca para ti por estudiar 
                en CEAA asesorías la cual consta de: (Aquí explicamos la beca) Al tomar esta beca estaremos resolviendo el problema 
                de raíz ya que además de familiarizarte con el inglés para el examen de admisión también comenzaras a trabajar en tu 
                futuro y lograras que tu paso por la facultad sea más exitoso ya que podrás investigar y hacer todos los trabajos que 
                te pidan en inglés sin ningún problema. ¿Te gustaría tomar la beca?";
              }

              if($puntuacionGlobal >=51 and $puntuacionGlobal <=75){ // 51 a 75
                $mensaje  = "Hola <span class='negrita'> $nombre </span> te llamamos de parte de Fast English para brindarte tu puntaje del examen de 
                reforzamiento de inglés. La puntuación obtenida fue de <span class='negrita'> $puntuacionGlobal </span> puntos, la cual demuestra que tienes algunos conocimientos 
                del idioma sin embargo podemos mejorar el puntaje y asegurar que se obtengan todos los puntos en el apartado de inglés 
                ya que de hecho estas cerca de lograrlo solo necesitas un poco más de ayuda. La proyección de puntaje basada en este 
                examen nos indica que usted dejaría de obtener entre 30 a 50 en el examen de admisión lo cual no es tan grave sin 
                embargo como coincidirás conmigo, es mejor tener todos los puntos y no dar ninguna ventaja a nuestra competencia y 
                lograr la mayor parte del puntaje disponible para disminuir al máximo el riesgo de no alcanzar algún lugar en la facultad. 
                Debemos reforzar las estructuras ya que si perdemos esos puntos no tendríamos el colchón completo de puntos en dado caso que 
                cometamos errores en otras materias. Es evidente que se tienen algunos problemas con la estructura del idioma, pero hasta 
                cierto punto es normal ya que la mayoría de los alumnos que intentaran ingresar a la facultad no dominan el idioma al 100%. 
                Aquí lo importante es saber cómo lo vamos a mejorar para poder ayudarte a que no se queden tantos puntos sin obtener en el 
                examen de admisión y puedas tener esa ventaja a tu favor. Recordemos que los más importantes en el examen son los apartados 
                de español y matemáticas, sin embargo, siempre es bueno tener puntos a nuestro favor de todas las materias. Aquí lo más 
                recomendable es repasar las estructuras que viste en tu clase de inglés en las asesorías y también ver el video que se 
                subirá a la cuenta de Instagram de Fast English, donde el maestro les explicara cómo se debió contestar el examen y por qué. 
                Ahorita tenemos 2 situaciones que debemos resolver, la más urgente es obtener el conocimiento necesario para obtener la mayor 
                cantidad de puntos en el examen de admisión, para eso será muy importante que no te pierdas el video explicativo y hagas los 
                demás exámenes que se subirán a la misma plataforma, solo debes estar pendiente de tu correo ya que ahí se enviaran las fechas 
                y enlaces para practicar. El otro problema que es aún más importante es que ya vas a ingresar a la facultad que elegiste 
                que es <span class='negrita'> $nombreFacultad </span> donde los maestros te van a pedir que investigues y algunas veces los libros están 
                en inglés, afortunadamente Fast English tiene una beca para ti por estudiar en CEAA asesorías la cual consta de: 
                  (Aquí explicamos la beca) Al tomar esta beca estaremos resolviendo el problema de raíz ya que además de familiarizarte con el 
                  inglés para el examen de admisión también comenzaras a trabajar en tu futuro y lograras que tu paso por la facultad sea 
                  más exitoso ya que podrás investigar y hacer todos los trabajos que te pidan en inglés sin ningún problema. ¿Te gustaría 
                  tomar la beca?";
              }

              if($puntuacionGlobal > 76){ // 76 a 100
                $mensaje  = "Hola <span class='negrita'> $nombre </span> te llamamos de parte de Fast English para brindarte tu puntaje del 
                examen de reforzamiento de inglés. La puntuación obtenida fue de <span class='negrita'> $puntuacionGlobal </span> puntos, primero que nada, felicidades ya que esta 
                puntuación demuestra que tienes algunos conocimientos del idioma sin embargo sabemos que tu grado de exigencia hacia ti mismo(a) 
                es mucho mayor y no te vas a permitir perder ningún punto en el examen en el apartado de inglés. La proyección de puntaje basada 
                en este examen nos indica que usted dejaría de obtener entre 5 a 25 puntos en el examen de admisión lo cual no es tan grave sin 
                embargo como coincidirás conmigo, es mejor tener todos los puntos y no dar ninguna ventaja a nuestra competencia y lograr la 
                mayor parte del puntaje disponible para disminuir al máximo el riesgo de no alcanzar algún lugar en la facultad. Debemos reforzar 
                las estructuras ya que si perdemos esos puntos no tendríamos el colchón completo de puntos en dado caso que cometamos errores 
                en otras materias. Tus problemas con las estructuras que vendrán en el examen son relativamente pocos, sin embargo, no hay 
                que confiarnos y no dar ninguna oportunidad a que el examen nos sorprenda. Aquí lo importante es saber cómo lo vamos a mejorar 
                para poder ayudarte a que no se quede ningún punto sin obtener en el examen de admisión y puedas tener esa ventaja a tu favor. 
                Recordemos que los más importantes en el examen son los apartados de español y matemáticas, sin embargo, siempre es bueno tener
                 puntos a nuestro favor de todas las materias. Aquí lo más recomendable es repasar las estructuras que viste en tu clase de 
                 inglés en las asesorías y también ver el video que se subirá a la cuenta de Instagram de Fast English, donde el maestro les 
                 explicara cómo se debió contestar el examen y por qué. Ahorita tenemos 2 situaciones que debemos resolver, la más urgente es 
                 obtener el conocimiento necesario para obtener la mayor cantidad de puntos en el examen de admisión, para eso será muy 
                 importante que no te pierdas el video explicativo y hagas los demás exámenes que se subirán a la misma plataforma, solo 
                 debes estar pendiente de tu correo ya que ahí se enviaran las fechas y enlaces para practicar. El otro problema que es aún 
                 más importante es que ya vas a ingresar a la facultad que elegiste que es <span class='negrita'> $nombreFacultad </span> donde los maestros 
                 te van a pedir que investigues y algunas veces los libros están en inglés, afortunadamente Fast English tiene una beca para 
                 ti por estudiar en CEAA asesorías la cual consta de: (Aquí explicamos la beca) Al tomar esta beca estaremos resolviendo el 
                 problema de raíz ya que además de familiarizarte con el inglés para el examen de admisión también comenzaras a trabajar en 
                 tu futuro y lograras que tu paso por la facultad sea más exitoso ya que podrás investigar y hacer todos los trabajos que te 
                 pidan en inglés sin ningún problema. ¿Te gustaría tomar la beca?";
              }
              
              
              
              
              
              
              

            //$to      = 'ana.molina@inbi.mx,Andrea.centeno@inbi.mx';
	          $to = 'fastenglishmty@gmail.com';

            $subject = 'Respuesta de examen FASTENGLISH';


            $headers = "From: " . $correo. "\r\n";
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
            $headers .= 'Bcc: backup@fastenglish.com.mx' . "\r\n";

            $message = "
            <!doctype html>
            <html lang=\"en\">
              <head>
                <title>Respuestas de examen.</title>
            <style>
              body {
                margin: 0;
                padding: 0;
              }
              h1 {
                font-family: Arial, Helvetica, sans-serif;
                font-size: 110%;
                font-weight: bold;
                color: #2E86C1;
              }

              h2 {
                font-family: Arial, Helvetica, sans-serif;
                font-size: 100%;
                font-weight: bold;
                color: #2E86C1 ;
              }
            p{
              color: #666;
              font-family: Arial, Helvetica, sans-serif;
            }
              .center {
                position: relative;
                margin: auto;
                padding: auto;
                text-align: center;
              }
              .contenedor {
                width: 80%;
                height: auto;
                padding: 0;
                margin: auto;
                background-color: #fff;
              }
              .centro {
                width: 200px;
                height: 30px;
                vertical-align: middle;
              }
              .asado:active {
                border: none;
              }
              .linea {
                width: 100%;
                height: 1px;
                border: 1px solid #cdcdcd;
              }
              .go {
                background-color:#FF5733;
                color:#fff;
                border:none;
                padding:10px 15px 10px 15px;
                font-family: Arial, Helvetica, sans-serif;
              }
              .dato-title {
                font-size: 100%;
                font-weight: bold;
                font-family: Arial, Helvetica, sans-serif;
              }
              .dato-description {
                font-size: 100%;
                font-weight: normal;
                font-family: Arial, Helvetica, sans-serif;
              }
              .negrita {
                font-size: 100%;
                font-weight: bold;
                font-family: Arial, Helvetica, sans-serif;
              }
            </style>


            </head>
            <body>
              <div class=\"contenedor\">
                <h1>FASTENGLISH</h1>
                
                <h2>¡Respuesta de examen de FastEnglish.</h2>

                <p>".$mensaje." </p>
                <br/><br/><br/>



              </div>
            </body>
            </html>
            ";


              mail($to, $subject, $message, $headers);
              // Enviar correo tambien al usuario.
              
              ?>

              <br/><br/>

              <div class="container">
                <div class="row">
                  <div class="col-md-3">
                  </div>
                  <div class="col-md-6">
                  <div class="text-center">
                    <img src="img/cropped-logo-fast-png-1.png">
                    <br/><br/>
                  </div>
                    <p class="text-center">
                    Has contestado el examen con éxito, un representante de nuestra escuela se comunicará contigo a la brevedad al número que nos proporcionaste. 
                    La llamada puede tardar un poco debido a la gran demanda de solicitudes, sin embargo, queremos darte una atención personalizada. 
                    Agradecemos su comprensión. De igual manera debes estar al pendiente de tu correo para los avisos referentes a futuros exámenes. 
                    Gracias por tu participación y mucho éxito.</p>


                  </div>
                  <div class="col-md-3">
                  </div>
                </div>
              </div>
              <script>
                setTimeout(function(){ window.location.href = "/index.php"; }, 10000);
              </script>

              <?php
              } else {
                echo "error al intentar hacer el insert";
                echo "Consulta SQL:" . $sqlInsertRespuestas ."<br/>"; 
              
              }

    }




  if(isset($existeRegistro) and $existeRegistro==1){
    ?>
 <br/><br/>

<div class="container">
  <div class="row">
    <div class="col-md-3">
    </div>
    <div class="col-md-6">
    <div class="text-center">
      <img src="img/cropped-logo-fast-png-1.png">
      <br/><br/>
    </div>

    <div class="alert alert-danger text-center" role="alert">
    El examen ya fue contestado.
    </div>
      


    </div>
    <div class="col-md-3">
    </div>
  </div>
</div>
              <script>
                setTimeout(function(){ window.location.href = "/index.php"; }, 3000);
              </script>
    <?php
  }

  $conn->close();
}
?>






    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>